package com.sophos.network.library.viewactions

import android.view.View
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.*
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.google.android.material.tabs.TabLayout
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.TypeSafeMatcher

class ViewActs {
    companion object{
        fun selectTabAtPosition(tabIndex: Int): ViewAction {
            return object : ViewAction {
                override fun getDescription() = "with tab at index $tabIndex"

                override fun getConstraints() = Matchers.allOf(
                    ViewMatchers.isDisplayed(),
                    ViewMatchers.isAssignableFrom(TabLayout::class.java)
                )

                override fun perform(uiController: UiController, view: View) {
                    val tabLayout = view as TabLayout
                    val tabAtIndex: TabLayout.Tab = tabLayout.getTabAt(tabIndex)
                        ?: throw PerformException.Builder()
                            .withCause(Throwable("No tab at index $tabIndex"))
                            .build()

                    tabAtIndex.select()
                }
            }
        }

        fun getCountFromRecyclerView(@IdRes RecyclerViewId: Int): Int {
            val COUNT = intArrayOf(0)
            val matcher = object : TypeSafeMatcher<View>() {
                override fun matchesSafely(item: View): Boolean {
                    COUNT[0] = (item as RecyclerView).adapter!!.itemCount
                    return true
                }

                override  fun describeTo(description: Description?) {}
            }
            // onView(allOf(withId(RecyclerViewId), isDisplayed())).check(matches(matcher))
            Espresso.onView(Matchers.allOf(ViewMatchers.withId(RecyclerViewId)))
                .check(ViewAssertions.matches(matcher))
            return COUNT[0]
        }

        fun getText(matcher: ViewInteraction): String {
            var text = String()
            matcher.perform(object : ViewAction {
                override fun getConstraints(): Matcher<View> {
                    return ViewMatchers.isAssignableFrom(TextView::class.java)
                }

                override fun getDescription(): String {
                    return "Text of the view"
                }

                override fun perform(uiController: UiController, view: View) {
                    val tv = view as TextView
                    text = tv.text.toString()
                }
            })

            return text
        }

        fun clickChildViewWithId(id: Int): ViewAction {
            return object : ViewAction {
                override fun getConstraints(): Matcher<View>? {
                    return null
                }
                override fun getDescription(): String {
                    return "Click on a child view with specific id."
                }
                override fun perform(
                    uiController: UiController,
                    view: View
                ) {
                    val v = view.findViewById<View>(id)
                    v.performClick()
                }
            }
        }
    }
}