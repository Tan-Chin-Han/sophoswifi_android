package com.sophos.network.library.utils

import android.util.SparseArray

class Cico(val intervalTime: Long = CLICK_INTERVAL_TIME) {

    private companion object Constants {

        const val CLICK_INTERVAL_TIME = 500L
    }

    val ids = SparseArray<Long>()

    inline fun clicked(
        id: Int,
        blocked: () -> Unit
    ) {
        if ((ids[id] ?: 0) + intervalTime < System.currentTimeMillis()) {
            ids.put(id, System.currentTimeMillis())
            blocked()
        }
    }


}