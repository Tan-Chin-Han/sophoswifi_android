package com.sophos.network.wireless

import android.content.Context
import android.os.Build.VERSION_CODES.Q
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4


import com.sophos.network.wireless.pagetool.frags_page
import org.junit.*
import org.junit.Assert.assertNotNull
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.robolectric.annotation.Config
import java.util.*


//@RunWith(JUnit4::class)
@RunWith(AndroidJUnit4::class)
@Config(sdk = [Q])
class MainViewModelTest {
    @get:Rule
   public var instantExecutorRule = InstantTaskExecutorRule()


   // @Mock
   // var apiEndPoint: ApiEndPoint? = null

  //  @Mock
  //  var apiClient: NewsApiClient? = null
    private var viewModel: MainViewModel? = null

    private val context: Context = ApplicationProvider.getApplicationContext()

    @Mock
    var observer: Observer<frags_page>? = null
    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.openMocks(this)
       // viewModel = MainViewModel(ApplicationProvider.getApplicationContext())
        System.out.println("setUp ${this.javaClass.simpleName}")

        viewModel =Mockito.mock(MainViewModel::class.java)
        MainViewModel.currentPage.observeForever(observer!!)
    }

    @Test
    fun testNull() {
        System.out.println("testNull ${this.javaClass.simpleName}")

        assertNotNull(viewModel)
        assertNotNull(context)
        Mockito.`when`(viewModel!!.discoveryDevices(context,{})).then {
            XendDevice()
        }
        var d=viewModel!!.discoveryDevices(context,{})
       // assertNotNull(viewModel!!.xendDevice)
        //assertTrue(viewModel.getXendDevice().hasObservers());
       // assertTrue(viewModel!!.xendDevice != null)
    }


    @Test
    fun testSample() {
        System.out.println("testSample ${this.javaClass.simpleName}")
        Mockito.`when`(viewModel!!.discoveryDevices(context,{})).then {
            XendDevice().apply {

               // viewModel!!.xendDevice = this
            }
        }
        var d=viewModel!!.discoveryDevices(ApplicationProvider.getApplicationContext(),{})
       // assertNotNull(viewModel!!.xendDevice)
        //assertTrue(viewModel.getXendDevice().hasObservers());
       // assertTrue(viewModel!!.xendDevice != null)


    }
    @Test
    fun initDB(){
        viewModel!!.apply {
            initDB()

            Assert.assertNotNull( sophosDevices)
            Assert.assertEquals(0,sophosDevices.size)
        }
    }

    @Test
    fun testLinkedList(){
        //You can mock concrete classes, not just interfaces
        //You can mock concrete classes, not just interfaces
        val mockedList = mock(LinkedList::class.java)

        //stubbing

        //stubbing
        `when`(mockedList.get(0)).thenReturn("first")
       // `when`(mockedList.get(1)).thenThrow(RuntimeException())

        //following prints "first"

        //following prints "first"
        System.out.println(mockedList.get(0))

        //following throws runtime exception

        //following throws runtime exception
        System.out.println(mockedList.get(1))

        //following prints "null" because get(999) was not stubbed

        //following prints "null" because get(999) was not stubbed
        System.out.println(mockedList.get(999))

        //Although it is possible to verify a stubbed invocation, usually it's just redundant
        //If your code cares what get(0) returns, then something else breaks (often even before verify() gets executed).
        //If your code doesn't care what get(0) returns, then it should not be stubbed. Not convinced? See here.

        //Although it is possible to verify a stubbed invocation, usually it's just redundant
        //If your code cares what get(0) returns, then something else breaks (often even before verify() gets executed).
        //If your code doesn't care what get(0) returns, then it should not be stubbed. Not convinced? See here.
        verify(mockedList).get(0)
    }

    @Test
    fun reset_mock() {
        val list = mock(MutableList::class.java)
        `when`(list.size).thenReturn(10)
        //list.add(1 as Nothing)
        Assert.assertEquals(10, list.size)
        //重置mock，清除所有的互動和預設
        reset<List<*>>(list)
        Assert.assertEquals(0, list.size)
    }

    @Test
    fun testApiFetchDataSuccess() {
        // Mock API response
        // when(apiClient.fetchNews()).thenReturn(Single.just(new NewsList()));
        //viewModel.fetchNews();
        // verify(observer).onChanged(NewsListViewState.LOADING_STATE);
        //verify(observer).onChanged(NewsListViewState.SUCCESS_STATE);
    }

    @Test
    fun testApiFetchDataError() {
        // when(apiClient.fetchNews()).thenReturn(Single.error(new Throwable("Api error")));
        //viewModel.fetchNews();
        //  verify(observer).onChanged(NewsListViewState.LOADING_STATE);
        // verify(observer).onChanged(NewsListViewState.ERROR_STATE);
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
       // apiClient = null
       // viewModel = null
    }
}