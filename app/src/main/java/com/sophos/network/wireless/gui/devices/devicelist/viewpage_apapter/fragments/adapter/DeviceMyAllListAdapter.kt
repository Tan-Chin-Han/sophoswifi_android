package com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter.fragments.adapter

import android.annotation.SuppressLint
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.acelink.library.search.UPnPSophosDiscovery
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.widget.dialog.adapter.SophosLoginDialog
import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.psclass.connection.SophosNetwork
import com.sophos.network.R
import com.sophos.network.wireless.gui.widget.dialog.SophosMessageDialog
import com.sophos.network.wireless.psclass.devcies.BaseSophosDevice
import com.sophos.network.wireless.psclass.sharepreference.device.SophosAccount

open class DeviceMyAllListAdapter (private val frag: Fragment, private var devices: MutableList<BaseSophosDevice>?=null) :
    DeviceAllListAdapter(frag,devices)
{


    override fun onBindViewHolder(viewHolder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        var value=filterResult!![position]

        value.run {
            ConstantClass.printForDebug("DeviceMyAllListAdapter onBindViewHolder position=${position} timeNotFound=${timeNotFound}")
            viewHolder.apply {
                bindingHolder.deviceListItemName.isSelected=true

                bindingHolder.deviceListItemName.setText(deviceName)


                if(isUnknown())
                {

                    bindingHolder.deviceListItemMac.setText(R.string.signal_health)
                    bindingHolder.deviceListItemMac2.apply {
                        setText(R.string.unknown)
                        visibility=View.VISIBLE
                    }
                    bindingHolder.deviceListItemHealthTxt.visibility=View.GONE
                    bindingHolder.deviceListItemHealth.setText(R.string.no_detected)
                    bindingHolder.deviceListItemSignal.setBackgroundColor(ContextCompat.getColor(frag.requireContext(),android.R.color.transparent))
                    bindingHolder.deviceListItemIconImg.setImageResource(R.mipmap.ap_disable_small_alert)
                    bindingHolder.deviceListItemClintTxt.setText(0.toString())
                }else{
                    bindingHolder.deviceListItemMac.setText(mac)
                    bindingHolder.deviceListItemMac2.apply {
                        setText("")
                        visibility=View.GONE
                    }
                    bindingHolder.deviceListItemHealthTxt.visibility=View.VISIBLE
                    bindingHolder.deviceListItemHealth.setText(R.string.signal_health)
                    if(WiFiRadio==0){
                        bindingHolder.deviceListItemIconImg.setImageResource(R.mipmap.ap_disable_small_alert)
                    }else{
                        bindingHolder.deviceListItemIconImg.setImageResource(R.mipmap.wifi)
                    }

                    updateHealth(bindingHolder)
                }

                bindingHolder.deviceListCardParent.setBackgroundResource(if (operatorMode==1)R.drawable.card_edge_central else R.drawable.card_edge)

                updateOeratorMode(bindingHolder)
                bindingHolder.deviceListItemClintTxt.setText(ConnectedUsers.toString())


                var system:(device:BaseSophosDevice,keep:Boolean,user:String,pass:String,passwordError:()->Unit)->Unit={device,keep,user,pass,passwordError->
                    (frag.requireActivity() as MainActivity).apply {
                        mainViewModel.currentEditDevice=device
                        device.currentPassword=pass
                        device.currentUser=user
                       // ConstantClass.printForDebug("getSystem currentUser=${device.currentUser} currentPassword=${device.currentPassword}")
                        getSystem(getCurrentEditDevice()!! ,{info,responseCode->
                            lanuchMain {
                                info?.let {
                                    if(keep){
                                        SophosAccount.saveAccount(mac!!,user,pass!! )
                                      //  SophosAccount.saveAccountUser(mac!!,user)
                                        //SophosAccount.savePassword(mac!!,pass)
                                    }
                                    if(operatorMode==1)
                                   // if(operatorMode!=1)
                                    {
                                        changeMainPage(frags_page.ap_central)
                                    }else{
                                       changeMainPage(frags_page.ap_edit)
                                    }
                                }?: kotlin.run {
                                    if(responseCode==401){
                                        passwordError.invoke()
                                    }
                                }
                            }
                        })
                    }

                }

                var lisener=View.OnClickListener {
                    ConstantClass.printForDebug("DeviceMyAllListAdapter click timeNotFound=${timeNotFound}")
                    if(isUnknown())
                    {
                        (frag.requireActivity() as MainActivity).apply {
                            mainViewModel.currentEditDevice=this@run
                            changeMainPage(frags_page.unknown)
                            return@OnClickListener
                        }
                    }
                   /* var user=""
                    var pass=""
                    mac?.let {
                        user=SophosAccount.getAccountUser(it)
                        pass=SophosAccount.getPassword(it)
                    }
                   */

                    var account=mac?.let { it1 -> SophosAccount.getAccount(it1) }
                    ConstantClass.printForDebug("getSystemInfo currentUser=${account?.user} currentPassword=${account?.password}")
                    if(!passwordError&&account!=null&&!account.user.isEmpty()&&!account.password.isEmpty()){
                        system.invoke(this@run,false,account.user,account.password,{
                            (frag.requireActivity() as MainActivity).apply {
                                SophosMessageDialog(this,"",
                                    getString(R.string.invalid_password),{ dialog ->
                                        blurMainFrame(false)

                                    }).apply {
                                    setOnDismissListener {
                                        blurMainFrame(false)
                                        passwordError=true
                                    }
                                    show()
                                    blurMainFrame(true)
                                }
                            }
                        })

                    }else{
                       /* var blockShowLogin:()->Unit={
                            SophosLoginDialog(frag.requireContext(),passwordError,{dialog,user,pass,keep,yes ->
                                if(yes){
                                    ConstantClass.printForDebug("SophosLoginDialog keep=${keep} ")
                                    (frag.requireActivity() as MainActivity).apply {
                                        (frag.requireActivity() as MainActivity)?.blurMainFrame(false)
                                        system.invoke(this@run,keep,user!!,pass!!,{
                                            blockShowLogin.invoke()
                                        })
                                        passwordError=false

                                    }
                                }
                            }).apply {
                                setOnDismissListener {
                                    (frag.requireActivity() as MainActivity)?.blurMainFrame(false)
                                }
                                show()
                                (frag.requireActivity() as MainActivity)?.blurMainFrame(true)
                            }
                        }
                        blockShowLogin.invoke()*/
                        showLoginDialog(this@run,this,system)

                    }



                }
                bindingHolder.root.setOnClickListener (lisener)
                bindingHolder.deviceListNext.setOnClickListener (lisener)
            }

        }

    }

    fun showLoginDialog(value:BaseSophosDevice,vh:ViewHolder,system:(device:BaseSophosDevice,keep:Boolean,user:String,pass:String,passwordError:()->Unit)->Unit){
        (frag.requireActivity() as MainActivity).apply {
            SophosLoginDialog(this,vh.passwordError,{dialog,user,pass,keep,yes ->
                if(yes){
                    ConstantClass.printForDebug("SophosLoginDialog keep=${keep} ")
                        blurMainFrame(false)
                        system.invoke(value,keep,user!!,pass!!,{//password error handle
                            vh.passwordError=true
                            showLoginDialog(value,vh,system)
                        })
                        vh.passwordError=false
                }
            }).apply {
                setOnDismissListener {
                    blurMainFrame(false)
                }
                show()
                blurMainFrame(true)
            }
        }

    }

    fun getDataSets()=devices

  //  fun getItemSize()=devices?.size?:0
}
