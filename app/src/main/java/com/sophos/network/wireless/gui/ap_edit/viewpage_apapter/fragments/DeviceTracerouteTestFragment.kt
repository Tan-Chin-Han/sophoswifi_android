package com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments

import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.abstractfrag.DeviceAbstractTestFragment
import com.sophos.network.wireless.psclass.connection.SophosNetwork

import com.sophos.network.R

class DeviceTracerouteTestFragment : DeviceAbstractTestFragment() {
    companion object{
        fun getInstance(): DeviceTracerouteTestFragment {
            return  DeviceTracerouteTestFragment()
        }
       val mTag = "DeviceTracerouteTestFragment"
    }

    override val titleRes: Int
        get() =R.string.traceroute_test


    fun onSelect(){

    }

    override suspend fun exeTest(address:String): String? {
       return viewModel()!!.TraceRoute(address)
    }

    override fun supportIPv6()=false

}