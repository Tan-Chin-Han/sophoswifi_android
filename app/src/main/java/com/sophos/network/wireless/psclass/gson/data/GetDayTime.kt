package com.sophos.network.wireless.psclass.gson.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class GetDayTime {
    @Expose
    @SerializedName("error_code")
    val errorCode = -1

    @Expose
    @SerializedName("error_msg")
    val errorMsg:String?=null

    @Expose
    @SerializedName("data")
    val data:DayTimeData ?= null



    class DayTimeData{

        @Expose
        @SerializedName("local_time_year")
        var local_time_year:Int ?= null

        @Expose
        @SerializedName("local_time_month")
        var local_time_month:Int?= null

        @Expose
        @SerializedName("local_time_day")
        var local_time_day:Int?= null

        @Expose
        @SerializedName("local_time_hour")
        var local_time_hour:Int?= null

        @Expose
        @SerializedName("local_time_minute")
        var local_time_minute:Int?= null

        @Expose
        @SerializedName("local_time_seconde")
        var local_time_seconde:Int?= null

        @Expose
        @SerializedName("time_zone")
        var time_zone:Int?= null

        @Expose
        @SerializedName("ntp_enabled")
        var ntp_enabled:Int?= null

        @Expose
        @SerializedName("ntp_server_name")
        var ntp_server_name:String?= null

        @Expose
        @SerializedName("ntp_update_interval")
        var ntp_update_interval:Int?= null

        @Expose
        @SerializedName("auto_daylight_save")
        var auto_daylight_save:Int?= null

        fun equalData(data:DayTimeData?):Boolean{
            if(data==null)return false
          return time_zone==data.time_zone&& ntp_enabled?.run {
              this==data.ntp_enabled&&if(ntp_enabled==1){
                  ntp_update_interval==data.ntp_update_interval&& auto_daylight_save==data.auto_daylight_save&&ntp_server_name==data.ntp_server_name
              }else{
                  local_time_year==data.local_time_year&&
                  local_time_month==data.local_time_month&&
                   local_time_day==data.local_time_day&&
                    local_time_hour==data.local_time_hour&&
                          local_time_minute==data.local_time_minute&&
                          local_time_seconde==data.local_time_seconde
              }
          }?:false
        }

        override fun toString(): String {
            return "DayTimeData(local_time_year=$local_time_year, local_time_month=$local_time_month, local_time_day=$local_time_day, local_time_hour=$local_time_hour, local_time_minute=$local_time_minute, local_time_seconde=$local_time_seconde, time_zone=$time_zone, ntp_enabled=$ntp_enabled, ntp_server_name=$ntp_server_name, ntp_update_interval=$ntp_update_interval, auto_daylight_save=$auto_daylight_save)"
        }

    }

    override fun toString(): String {
        return "GetDayTime(errorCode=$errorCode, errorMsg=$errorMsg, data=$data)"
    }
}