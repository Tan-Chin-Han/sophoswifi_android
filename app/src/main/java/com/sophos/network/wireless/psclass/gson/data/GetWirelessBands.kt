package com.sophos.network.wireless.psclass.gson.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.sophos.network.wireless.psclass.enum.Authentication_Type
import java.util.*

class GetWirelessBands {
    @Expose
    @SerializedName("error_code")
    val errorCode = -1

    @Expose
    @SerializedName("error_msg")
    val errorMsg:String?=null

    @Expose
    @SerializedName("data")
    val data:WirelessBandData ?= null


    override fun toString(): String {
        return super.toString()
    }



    class WirelessBandData{
        @Expose
        @SerializedName("radio_enable")
        var radio_enable:Int ?= null
        @Expose
        @SerializedName("radio_band")
        var radio_band:Int ?= null
        @Expose
        @SerializedName("enabled_ssidNum")
        var ssid_num_enable:Int ?= null
        @Expose
        @SerializedName("autochannel_enabled")
        var autochannel_enable:Int ?= null
        @Expose
        @SerializedName("autochannel_range")
        var autochannel_range:Int ?= null
        @Expose
        @SerializedName("autochannel_interval")
        var autochannel_interval:Int ?= null
        @Expose
        @SerializedName("autochannel_force_enabled")
        var autochannel_force_enabled:Int ?= null
        @Expose
        @SerializedName("channel")
        var channel:Int ?= null
        @Expose
        @SerializedName("channel_bandwidth")
        var channel_bandwidth:Int ?= null
        @Expose
        @SerializedName("bss_basicrateset")
        var bss_basicrateset:Int ?= null

        @Expose
        @SerializedName("ssidNames")
        var clientList:Array<WirelessName> ?= null

        fun copy():WirelessBandData{
            return WirelessBandData().also {data->
                data.radio_enable=radio_enable
                data.radio_band=radio_band
                data.ssid_num_enable=ssid_num_enable
                data.autochannel_enable=autochannel_enable
                data.autochannel_range=autochannel_range
                data.autochannel_interval=autochannel_interval
                data.autochannel_force_enabled=autochannel_force_enabled
                data.channel=channel
                data.channel_bandwidth=channel_bandwidth
                data.bss_basicrateset=bss_basicrateset
            }
        }

        override fun toString(): String {
            return "WirelessBandData(radio_enable=$radio_enable, radio_band=$radio_band, ssid_num_enable=$ssid_num_enable, autochannel_enable=$autochannel_enable, autochannel_range=$autochannel_range, autochannel_interval=$autochannel_interval, autochannel_force_enabled=$autochannel_force_enabled, channel=$channel, channel_bandwidth=$channel_bandwidth, bss_basicrateset=$bss_basicrateset)"
        }


    }

    class WirelessName{
        @Expose
        @SerializedName("ssidname")
        var ssidname:String ?= null
        @Expose
        @SerializedName("index")
        var index:Int ?= null
        @Expose
        @SerializedName("vlan_id")
        var vlan_id:Int ?= 0

        var auth= Authentication_Type.none

        var pass:String ?= null

        fun equalName(name:WirelessName):Boolean{
            return this.ssidname==name.ssidname&&this.vlan_id==name.vlan_id
        }

        fun equalAuth(newName:WirelessName):Boolean{
            return when(newName.auth){
                Authentication_Type.wep->true//no support
                Authentication_Type.none,Authentication_Type.owe ->this.auth==newName.auth
                else->{
                    var passCheck=newName.pass.let { namePass->
                        (( namePass==null||namePass.isEmpty() )&&( this.pass==null||this.pass!!.isEmpty() ))||this.pass==newName.pass
                    }
                    this.auth==newName.auth&&passCheck
                }
            }

        }

      /*  fun copy():WirelessName{
            return WirelessName().apply {
                this.ssidname=ssidname
                this.index=index
                this.vlan_id=vlan_id
                this.auth=auth
                this.pass=pass
            }
        }*/

        override fun toString(): String {
            return  "WirelessName(ssidname=$ssidname, index=$index,vlan_id=$vlan_id,auth=$auth,pass=$pass)"
        }
    }
}