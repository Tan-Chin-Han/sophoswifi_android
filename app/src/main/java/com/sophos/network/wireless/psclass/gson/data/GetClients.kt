package com.sophos.network.wireless.psclass.gson.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class GetClients {
    @Expose
    @SerializedName("error_code")
    val errorCode = -1

   // @Expose
   // @SerializedName("error_msg")
   // val errorMsg:String?=null

    @Expose
    @SerializedName("data")
    val data:ClientsData ?= null

    class ClientsData{
        @Expose
        @SerializedName("total_size")
        val totalSize:Int ?= null
        @Expose
        @SerializedName("client_list")
        val clientList:Array<ClientBand> ?= null
    }
}