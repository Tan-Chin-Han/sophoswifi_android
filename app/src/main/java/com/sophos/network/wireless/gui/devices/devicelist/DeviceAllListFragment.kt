package com.sophos.network.wireless.gui.devices.devicelist

import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.android.viewer.utils.TimerUtil
import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.gui.BaseFragment


import com.acelink.library.utils.data.ConstantClass
import com.google.android.material.tabs.TabLayoutMediator

import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.*
import com.acelink.library.pagetool.CoroutineIO.contextIO
import com.acelink.library.pagetool.CoroutineIO.contextMain
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.google.android.material.tabs.TabLayout
import com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter.AdapterDeviceAllListFragments
import com.sophos.network.wireless.pagetool.ViewModelTool.viewModels
import com.sophos.network.R
import com.sophos.network.databinding.FragDeviceAllListBinding
import com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter.MyPageMode
import com.sophos.network.wireless.psclass.devcies.BaseSophosDevice
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class DeviceAllListFragment : BaseFragment() {
    companion object{
        fun getInstance(): DeviceAllListFragment {
            return  DeviceAllListFragment()
        }
       val mTag = "DeviceAllListFragment"
    }

    val binding by viewBinding(FragDeviceAllListBinding::inflate)

    private var allListtimer =TimerUtil()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug("onCreateView ${javaClass.simpleName} ")
        binding.deviceAllListViewpager.adapter= AdapterDeviceAllListFragments(this)
        binding.deviceAllListViewpager.isUserInputEnabled=false
        TabLayoutMediator(binding.deviceAllListTabs, binding.deviceAllListViewpager,true,false) { tab, position ->
            when(position){
                0->  {
                      tab.text = "${getString(R.string.my_access)}(0)"
                    changeTabFont(0)
                     }
                1->{
                        tab.text = "${getString(R.string.new_access)}(0)"
                    }
            }

            ConstantClass.printForDebug("TabLayoutMediator pos=${ position}")

        }.attach()
        binding.deviceAllListTabs.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                ConstantClass.printForDebug("onTabSelected pos=${ tab?.position?:-1}")
                changeTabFont(tab.position)
                checkMyPageShouldGoNoDevice(tab.position )
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {
                ConstantClass.printForDebug("onTabReselected pos=${ tab?.position?:-1}")
                changeTabFont(tab.position)
                checkMyPageShouldGoNoDevice(tab.position )
            }
        })

        getMainActivity()!!.updateTitleRightVisable(View.GONE)
        lifecycle.addObserver( LifecycleEventObserver {source,event->
         when(event){
             Lifecycle.Event.ON_START->{ }
             Lifecycle.Event.ON_RESUME->{
                 ConstantClass.printForDebug("${javaClass.simpleName} ON_RESUME mode=${getViewPager2Adapter().deviceMyAllListFragment.myPageMode}")
                 getMainActivity()!!.startSSDPScanTimer()
             }
             Lifecycle.Event.ON_PAUSE->{
                 ConstantClass.printForDebug("${javaClass.simpleName} ON_PAUSE")

             }
             Lifecycle.Event.ON_STOP->{ }
         }

        })
        return binding.root
    }

    fun checkMyPageShouldGoNoDevice(pos: Int){
        var device=getSophosDevice()
        if(pos==0&&device!!.size==0){
            if(getViewPager2Adapter().deviceMyAllListFragment.myPageMode!=MyPageMode.NoDevice){
                getViewPager2Adapter().deviceMyAllListFragment.apply {
                    updateUINoDeviceMode()
                    startScan(true)
                }
            }
        }
    }

    @Synchronized
     fun changeTabFont( posSelect:Int){
        ConstantClass.printForDebug("changeTabFont pos=${posSelect}")
        val vg =  binding.deviceAllListTabs.getChildAt(0) as ViewGroup
        val tabsCount = vg.childCount
        for (j in 0 until tabsCount) {
           // val vgTab = vg.getChildAt(j) as ViewGroup
            //val tabChildsCount = vgTab.childCount
            var tabview=binding.deviceAllListTabs.getTabAt(j)?.view
            tabview?.apply {
                for (i in 0 until childCount) {
                    val tabViewChild = tabview.getChildAt(i)
                    if (tabViewChild is TextView)
                    {
                        tabViewChild.setTextSize(TypedValue.COMPLEX_UNIT_SP,14f )
                        tabViewChild.setTextColor(ContextCompat.getColor(requireContext(),R.color.sophos_title_black))
                        if(posSelect==j)
                            tabViewChild.setTypeface(ResourcesCompat.getFont(requireContext(), R.font.sophossans_semibold))
                        else
                            tabViewChild.setTypeface(ResourcesCompat.getFont(requireContext(), R.font.sophossans_regular))
                    }
                }
            }

        }
    }

    fun updateTabNum(pos:Int,num:Int){
        ConstantClass.printForDebug("updateTabNum ${pos} ${num}")
        when(pos){
            0->binding.deviceAllListTabs.getTabAt(pos)!!.setText("${getString(R.string.my_access)}($num)")
            1-> binding.deviceAllListTabs.getTabAt(pos)!!.setText("${getString(R.string.new_access)}($num)")
        }
        binding.deviceAllListTabs.selectedTabPosition?.apply {
            if(pos!=this) changeTabFont(this)
        }

    }

    fun getViewPager2Adapter():AdapterDeviceAllListFragments{
        return  binding.deviceAllListViewpager.adapter as AdapterDeviceAllListFragments
    }

    fun toMyListTab()
    {
        ConstantClass.printForDebug("toMyListTab")
        binding.deviceAllListTabs.apply {
            setScrollPosition(0,0f,true);
            binding.deviceAllListViewpager.setCurrentItem(0);
        }
        changeTabFont(0)
    }

    fun toNewTab()
    {
        ConstantClass.printForDebug("toNewTab")
        binding.deviceAllListTabs.apply {
            setScrollPosition(1,0f,true);
            binding.deviceAllListViewpager.setCurrentItem(1);

        }
        changeTabFont(1)
    }


    fun notificationAllSlaveUpdate(){
        (frags_page.allList.pageFragment as DeviceAllListFragment).apply {
            var slaves=getSophosDevice()

            var my= (getViewPager2Adapter().deviceMyAllListFragment).calculSophosDevices()

        //    var news= slaves?.filter { it.new==true } as ArrayList<BaseSophosDevice>

            (getViewPager2Adapter().deviceNewAllListFragment) .apply {


                       lanuchIO{
                       ConstantClass.printForDebug("deviceNewAllListFragment isAdded=${isAdded}")
                       if(!isAdded){
                          // delay(500)
                       }
                        lanuchMain{
                          // updateTabNum(1,news.size)
                           calculSophosDevices()

                       }
                   }



            }
           // ConstantClass.printForDebug("notificationAllSlaveUpdate slaves size=${slaves.size} my size=${my?.size?:0} news=${news.size}")
        }

    }

    fun notificationCurrentEditUpdate()=(getViewPager2Adapter().deviceMyAllListFragment).notificationCurrentEditUpdate()

    override fun onDestroyView() {
        super.onDestroyView()

    }

    override fun onBackPress() {
        super.onBackPress()
        //getMainActivity()?.changeMainPage(frags_page.allList)
        getMainActivity()?.finish()
    }

    override fun onSelect() {
        ConstantClass.printForDebug("${mTag} onSelect DeviceListFragment")

        getMainActivity()!!.startSSDPScanTimer()
        super.onSelect()
    }

    override fun onDeSelect() {
        allListtimer.removeMyTimer()

        super.onDeSelect()
    }


}