package com.sophos.network.wireless.gui.ap_edit

import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.android.viewer.utils.TimerUtil
import com.sophos.network.wireless.pagetool.frags_page


import com.acelink.library.utils.data.ConstantClass

import com.sophos.network.wireless.pagetool.ViewModelTool.viewModels
import com.sophos.network.R
import com.sophos.network.databinding.FragApEditBinding
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments
import com.sophos.network.wireless.gui.widget.dialog.SophosProgressMsgDialog
import com.sophos.network.wireless.gui.widget.dialog.SophosYesNoDialog

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import androidx.core.content.ContextCompat
import com.acelink.library.pagetool.CoroutineIO.contextIO
import com.acelink.library.pagetool.CoroutineIO.contextMain
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.acelink.library.search.UPnPSophosDiscovery.Companion.ssdpDiscoveryInterval

import com.acelink.library.utils.WiFiUtils
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.APConfigFragment
import com.sophos.network.wireless.gui.BaseFragment
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_clients_page
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_day_time_page
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_lan_port_page
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_ping_test_page
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_system_page
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_traceroute_test_page
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_wireless_band_page
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.DayTimePageMode
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.LanPortPageMode
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.SystemPageMode
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.WirelessBandsPageMode
import com.sophos.network.wireless.gui.devices.devicelist.DeviceAllListFragment
import com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter.*
import com.sophos.network.wireless.psclass.SophosEditableStype
import com.sophos.network.wireless.psclass.connection.SophosNetwork
import com.sophos.network.wireless.psclass.enum.Radio
import com.sophos.network.wireless.psclass.sharepreference.device.SophosAccount
import java.time.Duration

enum class APEditMode{
    enable,radio_disable,rebooting,
}

class APEditFragment : APConfigFragment() {
    companion object{
        fun getInstance(): APEditFragment {
            return  APEditFragment()
        }
       val mTag = "APEditFragment"
        val updateValueResultSuccess=0
        val updateValueResultReturn=1
        val updateValueResultFullFail=2
        val updateValueResulPartialtFail=3
        val updateValueResultNoChange=4

        val updateValueResultSuccess_andIPChange=11
    }

    //val viewModel: APEditViewModel by viewModels()

    val binding by viewBinding(FragApEditBinding::inflate)

    private lateinit var tabLayoutMediator:TabLayoutMediator

    private var apEditMode=APEditMode.enable

    private var discoveryUpdateModeDelay=-1L


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug("onCreateView ${javaClass.simpleName} ")
        viewModel.editFragment=this

        getMainActivity()!!.updateTitleRightVisable(View.GONE)
        binding.apEditViewpager.adapter= AdapterDeviceEditFragments(this)
        binding.apEditViewpager.isUserInputEnabled=false
        tabLayoutMediator=TabLayoutMediator(binding.apEditTabs, binding.apEditViewpager,true,false) { tab, position ->
            when(position){
                edit_system_page->  {
                    tab.text = getString(R.string.system)
                    changeTabFont(0)
                }
                edit_lan_port_page->{
                    tab.text = getString(R.string.lan_port)
                }
                edit_wireless_band_page->{
                    tab.text = getString(R.string.wireless_bands)
                }
                edit_clients_page->{
                    tab.text = getString(R.string.wireless_clients)
                }
                edit_day_time_page->{
                    tab.text = getString(R.string.day_and_time)
                }
                edit_ping_test_page->{
                    tab.text = getString(R.string.ping_test)
                }
                edit_traceroute_test_page->{
                    tab.text = getString(R.string.traceroute_test)
                }
            }

            ConstantClass.printForDebug("TabLayoutMediator pos=${ position}")

        }.apply {attach()  }
        binding.apEditTabs.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                ConstantClass.printForDebug("onTabSelected Edit pos=${ tab?.position?:-1}")

                changeTabFont(tab.position)
                lanuchIO{
                    delay(100)

                        when(tab.position){
                            edit_system_page->getViewPager2EditAdapter().deviceSystemFragment.onSelect()
                            edit_lan_port_page->getViewPager2EditAdapter().deviceLanPortFragment.onSelect()
                            edit_wireless_band_page->getViewPager2EditAdapter().deviceWirelessBandsFragment.onSelect()
                            edit_clients_page->getViewPager2EditAdapter().deviceClientsFragment.onSelect()

                            edit_day_time_page->getViewPager2EditAdapter().dayTimeFragment.onSelect()
                            edit_traceroute_test_page->getViewPager2EditAdapter().deviceTracerouteTestFragment.onSelect()
                        }
                    lanuchMain {
                        when(tab.position){
                            edit_system_page,edit_lan_port_page,edit_wireless_band_page,edit_day_time_page->{
                                updateEditableLayoutOnly(true)
                            }
                            else ->{
                                updateEditableLayoutOnly(false)
                            }
                        }
                    }
                    isAPinEditingMode()//for test
                }

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                ConstantClass.printForDebug("onTabUnselected pos=${ tab?.position?:-1}")
                when(tab.position) {
                    edit_system_page -> getViewPager2EditAdapter().deviceSystemFragment.deSelect()
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                ConstantClass.printForDebug("onTabReselected pos=${ tab?.position?:-1}")
                getViewPager2EditAdapter()?.apply {
                    when(tab.position) {
                        edit_system_page->  {
                            deviceSystemFragment.apply { if(isAdded)onSelect() }
                        }
                        edit_lan_port_page->{
                            deviceLanPortFragment.apply { if(isAdded)onSelect() }
                        }
                        edit_wireless_band_page->{
                            deviceWirelessBandsFragment.apply { if(isAdded)onSelect() }
                        }
                        edit_clients_page->{
                            deviceClientsFragment.apply { if(isAdded)onSelect() }
                        }
                        edit_day_time_page->{
                            dayTimeFragment.apply { if(isAdded)onSelect() }
                        }
                    }
                }

            }
        })
      /*  binding.apEditViewpager.registerOnPageChangeCallback(object :ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if(binding.apEditTabs.selectedTabPosition!=position){
                    val tab: TabLayout.Tab = binding.apEditTabs.getTabAt(position)!!
                    tab.select()
                }
            }
        })*/

        changeTabFont(0)


        binding.apEditDisableLayout.setOnClickListener {
            if(isAPinEditingMode())return@setOnClickListener
            getMainActivity()?.apply {
                when(apEditMode){
                    APEditMode.enable->{
                        SophosYesNoDialog(requireContext(),"",getString(R.string.ap_disable_msg),{dialog, yes ->
                            if(yes){

                                getCurrentEditDevice()?.let {device->
                                    device.mac?.let {

                                        SophosAccount.getMyDevice(it).let { sophosItem->
                                            lanuchIO {
                                                onShowProgress(null)
                                                var result= SophosNetwork.disableRadios(device,sophosItem,support6G)
                                                ConstantClass.printForDebug("setDisable result ${result}")
                                                if(result){
                                                    var reload= SophosNetwork.setReload(device)
                                                    if(reload!=null&&reload>0)   {
                                                        ConstantClass.printForDebug("setDisable Reload =${reload} xxxxxxxxxxxxxxxxxxxx")
                                                        viewModel.countReload(reload)
                                                        delay((reload*1000).toLong())
                                                        viewModel.removeCountReload()
                                                    }
                                                    result=reload!=null
                                                }

                                                lanuchMain {
                                                    if(result){
                                                        device.WiFiRadio=0
                                                        discoveryUpdateModeDelay=System.currentTimeMillis()+5000
                                                        binding.apEditDisableTxt.setText(getString(R.string.enable))
                                                        apEditMode=APEditMode.radio_disable
                                                        updateApEditModeUI()
                                                        (frags_page.allList.pageFragment as? DeviceAllListFragment)?.notificationCurrentEditUpdate()
                                                        lanuchIO {
                                                            if(binding.apEditViewpager.currentItem==edit_wireless_band_page)
                                                            {
                                                                getViewPager2EditAdapter().deviceWirelessBandsFragment.getBands()
                                                            }else{
                                                              onHideProgress()
                                                            }
                                                            lanuchMain{
                                                                showChangeResult(true)
                                                            }

                                                        }

                                                    }else{
                                                        showChangeResult(false)
                                                        onHideProgress()
                                                    }
                                                }

                                            }

                                        }
                                    }
                                }



                            }else{
                                (requireActivity() as MainActivity).blurMainFrame(false)
                            }

                        }).apply {
                            setOnDismissListener{
                                (requireActivity() as MainActivity).blurMainFrame(false)
                            }
                        }.show()
                        (requireActivity() as MainActivity).blurMainFrame(true)
                    }
                    APEditMode.radio_disable->{
                        // SophosYesNoDialog(requireContext(),"",getString(R.string.ap_eable_msg),{dialog, yes ->
                        // if(yes){
                        lanuchIO {
                            getCurrentEditDevice()?.let {device->
                                device.mac?.let {
                                    SophosAccount.getMyDevice(it).let { sophosItem->
                                        lanuchIO {
                                            onShowProgress(null)
                                            var result= SophosNetwork.recoveryRadios(device,sophosItem,support6G)
                                            if(result){
                                                var reload= SophosNetwork.setReload(device)
                                                ConstantClass.printForDebug("recoveryRadios Reload =${reload} xxxxxxxxxxxxxxxxxxxx")
                                                if(reload!=null&&reload>0)   {

                                                    viewModel.countReload(reload)
                                                    delay((reload*1000).toLong())
                                                    viewModel.removeCountReload()
                                                }

                                                result=reload!=null
                                            }
                                            lanuchMain {
                                                if(result){
                                                    device.WiFiRadio=sophosItem?.run {
                                                        var res=(if(enable24g==1)1 else 0) +(if(enable5g==1)2 else 0)+(if(enable6g==1)4 else 0)
                                                        res
                                                    }?:7
                                                    discoveryUpdateModeDelay=System.currentTimeMillis()+5000
                                                    binding.apEditDisableTxt.setText(getString(R.string.disable))
                                                    apEditMode=APEditMode.enable
                                                    updateApEditModeUI()
                                                    (frags_page.allList.pageFragment as? DeviceAllListFragment)?.notificationCurrentEditUpdate()
                                                    lanuchIO {
                                                        if(binding.apEditViewpager.currentItem==edit_wireless_band_page){
                                                            getViewPager2EditAdapter().deviceWirelessBandsFragment.getBands()
                                                        }else{
                                                            onHideProgress()
                                                        }
                                                        lanuchMain{
                                                            showChangeResult(true)
                                                        }
                                                    }

                                                }else{
                                                    showChangeResult(false)
                                                   onHideProgress()
                                                }
                                            }

                                        }

                                    }
                                }
                            }
                        }


                        // }

//                    }).apply {
//                        setOnDismissListener{
//                            (requireActivity() as MainActivity).blurMainFrame(false)
//                        }
//                    }.show()
//                    (requireActivity() as MainActivity).blurMainFrame(true)
                    }
                }
            }



        }
        updateTitleUI()

        support6G=getCurrentEditDevice()?.isSupport6G()?:false
        updateDiscoveryEnd()
        updateApEditModeUI()
        return binding.root
    }

    private fun updateTitleUI(){
        getMainActivity()?.mainViewModel?.currentEditDevice?.apply {
            binding.apEditTitleName.setText(deviceName)
            binding.apEditTitleName.isSelected=true
            binding.apEditTitleMac.setText(mac)

            binding.apEditTitleDelete.setOnClickListener {
                ConstantClass.printForDebug("apEditTitleDelete")
                mac?.let { it1 -> SophosAccount.deleteDevice(it1) }
                getSophosDevice()?.find { it.mac==mac }?.new=true

                (frags_page.allList.pageFragment as DeviceAllListFragment).apply {
                    notificationAllSlaveUpdate()
                    getViewPager2Adapter().deviceMyAllListFragment.startScan(false)
                }
                onBackPress()

            }

            binding.apEditTitleFrequency.setText(getWiFiRadioFrequency())
            binding.apEditTitleClintCount.setText("${ConnectedUsers} ${getString(R.string.connected_clients)}")
        }

    }

    private fun isAPinEditingMode():Boolean{
        return when(binding.apEditViewpager.currentItem){
            edit_system_page->{
               return getViewPager2EditAdapter().deviceSystemFragment.run {
                   when(systemPageMode){
                       SystemPageMode.Edit->true
                       else ->false
                    }
                }
            }
            edit_wireless_band_page-> {
                return getViewPager2EditAdapter().deviceWirelessBandsFragment.run {
                    when (wirelessBandsPageMode) {
                        WirelessBandsPageMode.Edit -> true
                         else ->false
                    }
                }
            }
            edit_lan_port_page->{
                return getViewPager2EditAdapter().deviceLanPortFragment.run {
                    when (lanPortPageMode) {
                        LanPortPageMode.Edit_Static,LanPortPageMode.Edit_DHCP -> true
                        else ->false
                    }
                }
            }
            edit_day_time_page->{
                return getViewPager2EditAdapter().dayTimeFragment.run {
                    when (dayTimePageMode) {
                        DayTimePageMode.Edit_NTP_Enable,DayTimePageMode.Edit_NTP_Disable -> true
                        else ->false
                    }
                }
            }
            else ->false
        }
    }

    fun updateApEditModeUI(){
        when(apEditMode){
            APEditMode.enable->{
                binding.apEditRadioSignalImg.setImageResource(R.mipmap.ap_edit_radio_enable)
                getCurrentEditDevice()?.apply{
                    binding.apEditTitleMac.setText(mac)
                }

                binding.apEditDisableSmallAlert.visibility=View.GONE
                binding.apEditTitleFrequency.visibility=View.VISIBLE

                binding.apEditEditableTxt.alpha=1f
                binding.apEditRebootTxt.alpha=1f
                binding.apEditEditableImg.setImageResource(R.mipmap.ap_edit)
                binding.apEditEditableImg.visibility=View.VISIBLE
                binding.apEditSaveImg.visibility=View.GONE
                binding.apEditRebootImg.setImageResource(R.mipmap.reboot)
                binding.apEditDisableImg.setImageResource(R.mipmap.ap_disable)
                binding.apEditDisableTxt.setText(R.string.disable)
                binding.apEditEditableLayout.setOnClickListener {
                        editableChange(true)
                }
                binding.apEditRebootLayout.setOnClickListener {
                    if(isAPinEditingMode())return@setOnClickListener
                    SophosYesNoDialog(requireContext(),"",getString(R.string.reboot_msg),{dialog, yes ->
                        if(yes){
                            viewModel!!.getClients {
                                var clients= Radio.getAllClients()
                                var phoneMac=WiFiUtils.getPhoneWifiMac(requireContext())
                                var phoneIP=WiFiUtils.getPhoneWifiIP(requireContext())
                                ConstantClass.printForDebug("phoneMac=${phoneMac} phoneIP=${phoneIP}")
                                var find=clients.find {
                                    it.mac==phoneMac ||
                                            (phoneIP!=null&& it.ip!! in  phoneIP!!    )


                                }
                                lifecycleScope.launch(contextMain){
                                    if(find!=null){
                                            SophosYesNoDialog(requireContext(),"",getString(R.string.reboot_turn_off_network),
                                                { dialog, yes ->
                                                    if(yes){
                                                        rebooting(true)
                                                    }else{
                                                        (requireActivity() as MainActivity).blurMainFrame(false)
                                                    }

                                                }).apply {
                                                show()
                                                setCancelable(false)
                                            }


                                    }else{
                                        rebooting(false)
                                    }
                                }

                            }



                        }else{
                            (requireActivity() as MainActivity).blurMainFrame(false)
                        }
                    }).apply {
                        show()
                        setCancelable(false)
                    }
                    (requireActivity() as MainActivity).blurMainFrame(true)
                }
            }
            APEditMode.radio_disable->{
                binding.apEditDisableImg.setImageResource(R.mipmap.ap_enable)
                binding.apEditDisableTxt.setText(R.string.enable)
                binding.apEditRebootLayout.setOnClickListener(null)
                binding.apEditEditableLayout.setOnClickListener(null)
                binding.apEditRadioSignalImg.setImageResource(R.mipmap.ap_edit_radio_disable)
                binding.apEditTitleMac.setText(R.string.ap_radio_disable)
                binding.apEditDisableSmallAlert.visibility=View.VISIBLE
                binding.apEditTitleFrequency.visibility=View.INVISIBLE
                binding.apEditEditableTxt.alpha=0.3f
                binding.apEditRebootTxt.alpha=0.3f
                binding.apEditEditableImg.setImageResource(R.mipmap.ap_edit_light)
                binding.apEditEditableImg.visibility=View.VISIBLE
                binding.apEditSaveImg.visibility=View.GONE
                binding.apEditRebootImg.setImageResource(R.mipmap.reboot_light)

            }
        }
    }

    private fun editableChange(saveValue:Boolean){
        when(binding.apEditViewpager.currentItem){
            edit_system_page->{
                getViewPager2EditAdapter().deviceSystemFragment.apply {
                    when(systemPageMode){
                        SystemPageMode.Edit->{

                            if(saveValue){
                                lanuchIO {

                                    var result=updateValue()
                                    lanuchMain {
                                        if(result!=2)showChangeResult(result==1)
                                        if(result!=0)
                                        {
                                            updateEditableLayoutContent(false)
                                            systemPageMode= SystemPageMode.Normal
                                            updateSystemPageMode()

                                            enableEditTab()
                                        }
                                    }
                                }
                            }else{
                                updateEditableLayoutContent(false)
                                systemPageMode= SystemPageMode.Normal
                                updateSystemPageMode()

                                enableEditTab()
                                onSelect()
                            }


                        }
                        SystemPageMode.Normal->{
                            updateEditableLayoutContent(true)
                            systemPageMode= SystemPageMode.Edit
                            updateSystemPageMode()
                            disableEditTab(edit_system_page)
                        }
                    }

                }
            }
            edit_lan_port_page->{
                getViewPager2EditAdapter().deviceLanPortFragment.apply {
                    when(lanPortPageMode){
                        LanPortPageMode.Edit_Static, LanPortPageMode.Edit_DHCP->
                        {
                            if(saveValue){
                                lanuchIO {
                                    getMainActivity()?.onShowProgress(null)
                                    var res=updateLanPort()
                                    if(res==updateValueResultSuccess||res==updateValueResultSuccess_andIPChange)
                                    {
                                        var startTime=System.currentTimeMillis()
                                        var reload= SophosNetwork.setReload( getCurrentEditDevice()!!)
                                        if(reload!=null&&reload>0)   {
                                            ConstantClass.printForDebug("setLanPort Reload =${reload} xxxxxxxxxxxxxxxxxxxx")
                                            viewModel.countReload(reload)
                                            delay((reload*1000).toLong())
                                            viewModel.removeCountReload()
                                        }
                                        var block:()->Unit={
                                            onSelect()
                                            lanuchMain {
                                                updateEditableLayoutContent(false)
                                                if(lanPortPageMode== LanPortPageMode.Edit_Static){
                                                    lanPortPageMode= LanPortPageMode.Normal_Static
                                                }else{
                                                    lanPortPageMode= LanPortPageMode.Normal_DHCP
                                                }
                                                updateLanPortPageMode()

                                                enableEditTab()
                                                showChangeResult(true)
                                                getMainActivity()?.onHideProgress()
                                            }
                                        }
                                        if(res==updateValueResultSuccess_andIPChange){
                                          var currentDevice=getCurrentEditDevice()!!
                                          viewModel.timerRefoundCurrentDevice(currentDevice,startTime ,{result->
                                              block.invoke()
                                          })
                                        }else{
                                            block.invoke()
                                        }

                                    }else if(res==updateValueResultFullFail||res==updateValueResulPartialtFail){
                                        lanuchMain {
                                            showChangeResult(false)
                                            getMainActivity()?.onHideProgress()
                                        }
                                    }else if(res==updateValueResultReturn){
                                        getMainActivity()?.onHideProgress()
                                    }
                                    else if(res==updateValueResultNoChange){
                                        lanuchMain {
                                            updateEditableLayoutContent(false)
                                            if(lanPortPageMode== LanPortPageMode.Edit_Static){
                                                lanPortPageMode= LanPortPageMode.Normal_Static
                                            }else{
                                                lanPortPageMode= LanPortPageMode.Normal_DHCP
                                            }
                                            updateLanPortPageMode()
                                            enableEditTab()
                                            getMainActivity()?.onHideProgress()
                                        }
                                    }

                                }

                            }else{
                                lanuchMain {

                                    if(lanPortPageMode== LanPortPageMode.Edit_Static){
                                        lanPortPageMode= LanPortPageMode.Normal_Static
                                    }else{
                                        lanPortPageMode= LanPortPageMode.Normal_DHCP
                                    }
                                    updateEditableLayoutContent(false)
                                    updateLanPortPageMode()
                                    enableEditTab()
                                    onSelect()
                                }
                            }
                        }
                        LanPortPageMode.Normal_Static,LanPortPageMode.Normal_DHCP->{
                                updateEditableLayoutContent(true)
                                if(lanPortPageMode== LanPortPageMode.Normal_Static){
                                    lanPortPageMode= LanPortPageMode.Edit_Static
                                }else{
                                    lanPortPageMode= LanPortPageMode.Edit_DHCP
                                }
                                updateLanPortPageMode()
                                disableEditTab(edit_lan_port_page)
                        }
                    }

                }
            }
            edit_wireless_band_page->{
                getViewPager2EditAdapter().deviceWirelessBandsFragment.apply {
                    when(wirelessBandsPageMode){
                        WirelessBandsPageMode.Edit->{

                            if(saveValue){
                                lanuchIO{
                                    getMainActivity()?.apply {
                                        onShowProgress(null)
                                        var success=updateValue()
                                        ConstantClass.printForDebug("Wireless Band updateValue success=${success}-------------->")
                                        var update=success!=updateValueResultFullFail&&success!=updateValueResultReturn&&success!=updateValueResultNoChange
                                        if(!update)onHideProgress()
                                        lanuchMain{
                                            if (success==updateValueResultSuccess|| success==updateValueResultNoChange){
                                                updateEditableLayoutContent(false)

                                                wirelessBandsPageMode= WirelessBandsPageMode.Normal
                                                updateWirelessBandsPageMode()
                                                enableEditTab()
                                                if (success==updateValueResultSuccess)
                                                    showChangeResult(true)
                                            }else if (success== updateValueResultReturn)
                                            {
                                                //nothing
                                            }
                                            else{
                                                showChangeResult(false)
                                            }
                                            if(update){
                                                onSelect()
                                            }
                                        }
                                    }


                                }
                            }else{
                                wirelessBandsPageMode= WirelessBandsPageMode.Normal
                                updateWirelessBandsPageMode()
                                updateEditableLayoutContent(false)

                                enableEditTab()
                                onSelect()
                            }

                        }
                        WirelessBandsPageMode.Normal->{
                            updateEditableLayoutContent(true)
                            wirelessBandsPageMode= WirelessBandsPageMode.Edit
                            updateWirelessBandsPageMode()
                            disableEditTab(edit_wireless_band_page)
                        }
                    }

                }
            }
            edit_day_time_page->{
                getViewPager2EditAdapter().dayTimeFragment.apply {
                    when(dayTimePageMode){
                        DayTimePageMode.Edit_NTP_Enable,DayTimePageMode.Edit_NTP_Disable->{
                            if(saveValue){
                                var current=System.currentTimeMillis()
                                lanuchIO {
                                    var result=updateValue()
                                    lanuchMain {
                                        if(result!=2)showChangeResult(result==1)
                                        if(result!=0){
                                            updateEditableLayoutContent(false)

                                            dayTimePageMode= DayTimePageMode.Normal
                                            updateDayTimePageMode()

                                            enableEditTab()
                                        }
                                    }
                                }

                            }else{
                                updateEditableLayoutContent(false)

                                dayTimePageMode= DayTimePageMode.Normal
                                updateDayTimePageMode()

                                enableEditTab()
                                onSelect()
                            }



                        }
                        DayTimePageMode.Normal->{
                            updateEditableLayoutContent(true)
                            dayTimePageMode= if(ntpServerEnable)DayTimePageMode.Edit_NTP_Enable else DayTimePageMode.Edit_NTP_Disable
                            updateDayTimePageMode()
                            disableEditTab(edit_day_time_page)
                        }
                    }
                }
            }
        }
    }

    private fun rebooting(self:Boolean){
        SophosProgressMsgDialog(requireContext(),"","${getString(R.string.rebooting_msg)}\n\n${getString(R.string.please_wait)}",{

        }).apply {
            show()
            setCancelable(false)
            lifecycleScope.launch(contextIO){
                //delay(3000)
                var result=false
                for (i in 0 ..2){
                    result=SophosNetwork.setReboot(getCurrentEditDevice()!!)
                    if(result )
                        break
                }
                if(!result){
                    lifecycleScope.launch(contextMain) {
                        dismiss()
                        (requireActivity() as MainActivity).blurMainFrame(false)
                        showRebootResult(false)
                    }
                    return@launch
                }


                var startRebootTime=System.currentTimeMillis()
                viewModel.countReboot(120,{
                    updateMsg("${getString(R.string.rebooting_msg)}\n\n${it} ${getString(R.string.seconds_left)}")
                })
                delay(10000)

                var currentDevice=getCurrentEditDevice()!!
                viewModel.timerRefoundCurrentDevice(currentDevice,startRebootTime ,{result->
                    dismiss()
                    (requireActivity() as MainActivity).blurMainFrame(false)
                    showRebootResult(result)
                })

            }
        }
    }

    fun showRebootResult(success:Boolean){
        if(success){
            SophosEditableStype.displayResultMsg(binding.apEditSuccessTxt,getString(R.string.reboot_successfully),true)
        }else{
            SophosEditableStype.displayResultMsg(binding.apEditSuccessTxt,getString(R.string.edit_save_fail),false)
        }
        binding.apEditSuccessCard.visibility=View.VISIBLE

        lifecycleScope.launch(contextIO){
            delay(3000)
            withContext(Dispatchers.Main){
                binding.apEditSuccessCard.visibility=View.GONE
            }
        }
    }

    fun showChangeResult(success:Boolean){
        if(success){
            SophosEditableStype.displayResultMsg(binding.apEditSuccessTxt,getString(R.string.edit_save_successfully),true)
        }else{
            SophosEditableStype.displayResultMsg(binding.apEditSuccessTxt,getString(R.string.edit_save_fail),false)
        }
        binding.apEditSuccessCard.visibility=View.VISIBLE

        lifecycleScope.launch(contextIO){
            delay(3000)
            withContext(Dispatchers.Main){
                binding.apEditSuccessCard.visibility=View.GONE
            }
        }
    }

    private fun updateEditableLayoutOnly(editable:Boolean){
        if(editable){
            binding.apEditEditableLayout.alpha=1f
        }else{
            binding.apEditEditableLayout.alpha=0.3f
        }
    }

    private fun updateEditableLayoutContent(editable:Boolean){
        ConstantClass.printForDebug("updateEditableLayoutContent editable=${editable}")
        if(editable){
            binding.apEditEditableTxt.setText(R.string.save)
            binding.apEditEditableTxt.setTextColor( ContextCompat.getColor(requireContext(),R.color.white))
            binding.apEditEditableImg.visibility=View.GONE
            binding.apEditSaveImg.visibility=View.VISIBLE
            binding.apEditEditableLayout.setBackgroundColor( ContextCompat.getColor(requireContext(),R.color.sophos_deep_blue))
            binding.apEditRebootImg.setImageResource(R.mipmap.reboot_light)
            when(apEditMode){
                APEditMode.radio_disable->  binding.apEditDisableImg.setImageResource(R.mipmap.ap_enable_light)
                else->binding.apEditDisableImg.setImageResource(R.mipmap.radio_disable_off)
            }

            binding.apEditRebootTxt.alpha=0.3f
            binding.apEditDisableTxt.alpha=0.3f
        }else{
             binding.apEditEditableTxt.setText(R.string.edit)
            binding.apEditEditableImg.setImageResource(R.mipmap.ap_edit)
            binding.apEditEditableImg.visibility=View.VISIBLE
            binding.apEditSaveImg.visibility=View.GONE
            binding.apEditEditableTxt.setTextColor( ContextCompat.getColor(requireContext(),R.color.sophos_background))
            binding.apEditEditableLayout.setBackgroundColor( ContextCompat.getColor(requireContext(),R.color.white))
            binding.apEditRebootImg.setImageResource(R.mipmap.reboot)
            when(apEditMode) {
                APEditMode.radio_disable -> binding.apEditDisableImg.setImageResource(R.mipmap.ap_enable)
                else -> binding.apEditDisableImg.setImageResource(R.mipmap.ap_disable)
            }

            binding.apEditRebootTxt.alpha=1f
            binding.apEditDisableTxt.alpha=1f
        }
    }

    fun enableEditTab(){
        val tabStrip = binding.apEditTabs.getChildAt(0) as LinearLayout
        for (i in 0 until tabStrip.childCount) {
            tabStrip.getChildAt(i).setOnTouchListener(null)
        }
        val vg =  binding.apEditTabs.getChildAt(0) as ViewGroup
        val tabsCount = vg.childCount
        for (j in 0 until tabsCount) {
            val vgTab = vg.getChildAt(j) as ViewGroup
            val tabChildsCount = vgTab.childCount
            for (i in 0 until tabChildsCount) {
                val tabViewChild = vgTab.getChildAt(i)
                if (tabViewChild is TextView)
                {
                   tabViewChild.alpha=1f
                }
            }
        }
    }

    private fun disableEditTab(pos:Int){
        val tabStrip = binding.apEditTabs.getChildAt(0) as LinearLayout
        for (i in 0 until tabStrip.childCount) {
            tabStrip.getChildAt(i).setOnTouchListener { v, event -> true }
        }
        val vg =  binding.apEditTabs.getChildAt(0) as ViewGroup
        val tabsCount = vg.childCount
        for (j in 0 until tabsCount) {
            val vgTab = vg.getChildAt(j) as ViewGroup
            val tabChildsCount = vgTab.childCount
            for (i in 0 until tabChildsCount) {
                val tabViewChild = vgTab.getChildAt(i)
                if (tabViewChild is TextView)
                {

                    if(pos==j)
                        tabViewChild.alpha=1f
                    else
                        tabViewChild.alpha=0.3f
                }
            }
        }
    }

    fun changeTabFont( posSelect:Int){
        ConstantClass.printForDebug("changeTabFont pos=${posSelect}")
        val vg =  binding.apEditTabs.getChildAt(0) as ViewGroup
        val tabsCount = vg.childCount
        for (j in 0 until tabsCount) {
            val vgTab = vg.getChildAt(j) as ViewGroup
            val tabChildsCount = vgTab.childCount
            for (i in 0 until tabChildsCount) {
                val tabViewChild = vgTab.getChildAt(i)
                if (tabViewChild is TextView)
                {
                    tabViewChild.setTextSize(TypedValue.COMPLEX_UNIT_SP,14f )
                    if(posSelect==j){
                        tabViewChild.setTypeface(ResourcesCompat.getFont(requireContext(), R.font.sophossans_semibold))
                        tabViewChild.invalidate()
                    }

                    else
                        tabViewChild.setTypeface(ResourcesCompat.getFont(requireContext(), R.font.sophossans_regular))
                }
            }
        }
    }

    fun getViewPager2EditAdapter(): AdapterDeviceEditFragments {
        return  binding.apEditViewpager.adapter as AdapterDeviceEditFragments
    }

  /*  fun toSystemTab(){
        binding.apEditTabs.apply {
            setScrollPosition(0,0f,true);
            binding.apEditViewpager.setCurrentItem(0);
        }
    }

    fun toLanPortTab(){
        binding.apEditTabs.apply {
            setScrollPosition(1,0f,true);
            binding.apEditViewpager.setCurrentItem(1);
        }
    }

    fun toWirelessBandsTab(){
        binding.apEditTabs.apply {
            setScrollPosition(2,0f,true);
            binding.apEditViewpager.setCurrentItem(2);
        }
    }*/

    override fun onDestroyView() {

        super.onDestroyView()
    }

    override fun onBackPress() {
        super.onBackPress()
        if(isAPinEditingMode()) {
            editableChange(false)
        }else{
            getMainActivity()?.changeMainPage(frags_page.allList)
        }

    }

    override fun onSelect() {

        ConstantClass.printForDebug("${mTag} onSelect APEditFragment")
       /* lanuchIO {
            viewModel.getWirelessBandData {
                var radioEnabe=true
                Radio.values().forEach{
                    if(it.valueBand?.data?.radio_enable?:1==0){
                        radioEnabe=false
                    }
                }
                ConstantClass.printForDebug("${mTag} radioEnabe ${radioEnabe}")
                if(!radioEnabe){
                    lanuchMain {
                        apEditMode=APEditMode.radio_disable
                        updateApEditModeUI()
                    }
                }


            }
        }*/
        getMainActivity()!!.startSSDPScanTimer()
        viewModel.onSelect()
        super.onSelect()
    }

    override fun onDeSelect()
    {
        viewModel.onDeSelect()
        super.onDeSelect()
    }

   override fun updateDiscoveryEnd(){
        var current=getCurrentEditDevice()
        getSophosDevice()?.forEachIndexed { index, baseSophosDevice ->
            if(baseSophosDevice.mac==current!!.mac){
                lifecycleScope.launch(contextMain) {
                    binding.apEditTitleClintCount.setText("${baseSophosDevice.ConnectedUsers} ${getString(R.string.connected_clients)}")
                    if(baseSophosDevice.deviceName!= binding.apEditTitleName.text.toString()){
                        binding.apEditTitleName.setText(baseSophosDevice.deviceName)
                    }

                    if(baseSophosDevice.timeNotFound==0){
                        var time=System.currentTimeMillis()
                        if(time-discoveryUpdateModeDelay>10000){
                            discoveryUpdateModeDelay=time
                            if(baseSophosDevice.WiFiRadio==0&&apEditMode!=APEditMode.radio_disable){
                                apEditMode=APEditMode.radio_disable
                                updateApEditModeUI()
                            }else if(baseSophosDevice.WiFiRadio!=0&&apEditMode!=APEditMode.enable){
                                apEditMode=APEditMode.enable
                                updateApEditModeUI()
                            }
                        }

                    }
                    binding.apEditTitleFrequency.setText(baseSophosDevice.getWiFiRadioFrequency())

                }

            }
        }
    }
}