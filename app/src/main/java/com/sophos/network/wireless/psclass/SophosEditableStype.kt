package com.sophos.network.wireless.psclass

import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.sophos.network.R

object SophosEditableStype {
    fun enableEditText(edit:AppCompatEditText){
        edit.apply {
            isEnabled=true
            ContextCompat.getColor(context,R.color.table_color).apply {
                setTextColor(this)
            }
            context.getDrawable(R.drawable.background_default_textbox_5dp).apply {
                background=this
            }
            setOnFocusChangeListener { v, hasFocus ->
                if(hasFocus){
                    context.getDrawable(R.drawable.background_focus_textbox_5dp).apply {
                       background=this
                    }
                    /*ContextCompat.getColor(context,R.color.table_color).apply {
                        setTextColor(this)
                    }*/
                }else{
                    context.getDrawable(R.drawable.background_default_textbox_5dp).apply {
                        background=this
                    }
                   /* ContextCompat.getColor(context,R.color.sophos_editable_color).apply {
                        setTextColor(this)
                    }*/
                }
            }
           /* if(isFocused){
                ContextCompat.getColor(context,R.color.table_color).apply {
                    setTextColor(this)
                }
            }else{
                ContextCompat.getColor(context,R.color.sophos_editable_color).apply {
                    setTextColor(this)
                }
            }*/
        }


    }

    fun disableEditText(edit:AppCompatEditText){
        edit.apply {
            isEnabled=false
            ContextCompat.getColor(context,R.color.table_color).apply {
                setTextColor(this)
            }
            setOnFocusChangeListener (null)
            context.getDrawable(android.R.color.transparent).apply {
                background=this
            }
        }
    }

    fun addSearchStyle(label:AppCompatEditText,afterTextChanged:(s: Editable?)->Unit){

        var density = label.context.getResources().getDisplayMetrics().density;
        var width = Math.round(20 * density).toInt();
        var height = Math.round(20 * density).toInt();
         var search =   ContextCompat.getDrawable(label.context,R.mipmap.search)!!
        search.setBounds(0, 0, width, height);
        var widthx = Math.round(13 * density).toInt();
        var heightx = Math.round(13 * density).toInt();
        var searchx =   ContextCompat.getDrawable(label.context,R.mipmap.searchx)!!
          searchx.setBounds(0, 0, widthx, heightx);
        search.apply {
            label.setCompoundDrawables(null, null, search, null);
        }
        label.addTextChangedListener( object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable) {
                if(p0.toString().length>0){
                    label.setCompoundDrawables(null, null, searchx, null);
                }else{
                    label.setCompoundDrawables(null, null, search, null);
                }
                     afterTextChanged.invoke(p0)
            }
        })
        label.setOnTouchListener(object :View.OnTouchListener{
                //var inRegion=false
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                var DRAWABLE_LEFT = 0;
                var  DRAWABLE_TOP = 1;
                var  DRAWABLE_RIGHT = 2;
                var  DRAWABLE_BOTTOM = 3;
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    if(event.getRawX() >= (label.getRight() - label.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                      //  inRegion=true
                        return true;
                    }
                }
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (label.getRight() - label.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                        label.setText("")
                        return true;
                    }
                   // inRegion=false
                }
                return false;
            }

        })

    }

    fun displayResultMsg(edit: TextView, result:String, success:Boolean){
        if(success){
            edit.setBackgroundColor(ContextCompat.getColor(edit.context,R.color.sophos_signal_excellent))
        }else{
            edit.setBackgroundColor(ContextCompat.getColor(edit.context,R.color.sophos_signal_poor))
        }
        edit.setText(result)
    }
}