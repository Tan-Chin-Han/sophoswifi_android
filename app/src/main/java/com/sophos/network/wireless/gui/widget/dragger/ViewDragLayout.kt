package com.sophos.network.wireless.gui.widget.dragger

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.util.SparseArray
import android.util.SparseIntArray
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.IdRes
import androidx.annotation.IntDef
import androidx.core.util.forEach
import androidx.core.view.ViewCompat
import androidx.customview.widget.ViewDragHelper
import com.acelink.library.utils.data.ConstantClass
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.ref.WeakReference

class ViewDragLayout  
@JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null, defStyle: Int = 0) :
    FrameLayout(
        context!!, attrs, defStyle
    ) {
    @IntDef(//HOVER_FRAME_OVERLAY,
        HOVER_LINEAR_HORIZONTAL
       // , HOVER_LINEAR_VERTICAL
    )
    @Retention(
        RetentionPolicy.SOURCE
    )
    annotation class HoverMode

    @IntDef(flag = true, value = [LEFT, TOP, RIGHT, BOTTOM, DIRECTION_ALL])
    @Retention(
        RetentionPolicy.SOURCE
    )
    annotation class DragFlag

    /*--------------------------------
     * General declaration
     *-------------------------------*/
    private val childViews: MutableList<View> = ArrayList()
    private val dragFlags = SparseIntArray()
    private val dragDistanceXs = SparseArray<Distance>()
   // private val dragDistanceYs = SparseArray<Distance>()
    private val edgeViews = SparseIntArray()
    private val hookList = SparseArray<List<View>>()
    var viewDragHelper: ViewDragHelper? = null
        private set
    //private var vdhEnable = true

    @HoverMode
    private var layoutType = HOVER_LINEAR_HORIZONTAL
    private var chainEnable = false
    private var edgeFlag = 0
  //  private var pullEnable = false
    private var speedFactor = 1.0f
  //  private var dispatchingTime: Long = 0
  //  private var pressTime = PRESS_TIME

    /*--------------------------------
   * Functions
   *-------------------------------*/
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        viewDragHelper = ViewDragHelper.create(this, 1.0f, CustomViewDragHelperCallback(this))
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        // Edge tracking
        viewDragHelper!!.setEdgeTrackingEnabled(edgeFlag)

        // Save child views
        val childCount = childCount
        childViews.clear()
        for (i in 0 until childCount) {
            val view = getChildAt(i)
            childViews.add(view)
        }
        measureChildren(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimensionType(layoutType, widthMeasureSpec, heightMeasureSpec, childCount)
    }

    /**
     * Measure view group with current layout type
     *
     * @param layoutType layout type [.HOVER_FRAME_OVERLAY], [.HOVER_LINEAR_HORIZONTAL],
     * [ ][.HOVER_LINEAR_VERTICAL]
     * @param widthMeasureSpec measure spec
     * @param heightMeasureSpec measure spec
     * @param childCount group child count
     */
    private fun setMeasuredDimensionType(
        @HoverMode layoutType: Int, widthMeasureSpec: Int,
        heightMeasureSpec: Int, childCount: Int
    ) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
        val offsetLeftAndRight = paddingLeft + paddingRight
        val offsetTopAndBottom = paddingTop + paddingBottom
        if (DEBUG) {
            Log.d(
                TAG, "measure - "
                        + layoutType
                        + " offsetLeftAndRight = ["
                        + offsetLeftAndRight
                        + ", offsetTopAndBottom = ["
                        + offsetTopAndBottom
                        + "]"
            )
        }
        var width = 0
        var height = 0
        if (widthMode == MeasureSpec.EXACTLY && heightMode == MeasureSpec.EXACTLY) {
            width = widthSize
            height = heightSize
        } else {
            for (index in 0 until childCount) {
                val child = childViews[index]
                val measureWidth = child.measuredWidth
                val measureHeight = child.measuredHeight
                when (layoutType) {
                  /*  HOVER_FRAME_OVERLAY -> {
                        width = if (width < measureWidth) measureWidth else width
                        height = if (height < measureHeight) measureHeight else height
                    }*/
                    HOVER_LINEAR_HORIZONTAL -> {
                        width += measureWidth
                        height = if (height < measureHeight) measureHeight else height
                    }
                   /* HOVER_LINEAR_VERTICAL -> {
                        width = if (width < measureWidth) measureWidth else width
                        height += measureHeight
                    }*/
                    else -> {
                    }
                }
            }
            width = if (widthMode == MeasureSpec.EXACTLY) {
                widthSize
            } else {
                Math.min(width + offsetLeftAndRight, widthSize)
            }
            height = if (heightMode == MeasureSpec.EXACTLY) {
                heightSize
            } else {
                Math.min(height + offsetTopAndBottom, heightSize)
            }
        }
        if (DEBUG) {
            Log.d(TAG, "measure - $layoutType, width = [$width, height = [$height]")
        }
        setMeasuredDimension(width, height)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        if (DEBUG) {
            Log.d(
                TAG, "onLayout - "
                        + "changed = ["
                        + changed
                        + "], l = ["
                        + l
                        + "], t = ["
                        + t
                        + "], r = ["
                        + r
                        + "], b = ["
                        + b
                        + "]"
                        + ", type = ["
                        + layoutType
                        + "]"
            )
        }
        val childCount = childViews.size
        if (childCount > 0) {
            when (layoutType) {
                //HOVER_FRAME_OVERLAY -> layoutOverlap(l, t, r, b, childCount)
                HOVER_LINEAR_HORIZONTAL -> layoutHorizontal(t, b, childCount)
               // HOVER_LINEAR_VERTICAL -> layoutVertical(l, r, childCount)
                else -> {
                }
            }
        }
    }

    /**
     * Build child layout as overlap
     *
     * @param l parent left
     * @param t parent top
     * @param r parent right
     * @param b parent bottom
     * @param childCount child count
     */
   /* private fun layoutOverlap(l: Int, t: Int, r: Int, b: Int, childCount: Int) {
        for (index in 0 until childCount) {
            val child = childViews[index]
            val childWidth = child.measuredWidth
            val childHeight = child.measuredHeight
            var left = r - l - childWidth
            var top = b - t - childHeight
            if (left > 0) {
                left = (r - l) / 2 - childWidth / 2
            }
            if (top > 0) {
                top = (b - t) / 2 - childHeight / 2
            }
            child.layout(left, top, left + childWidth, top + childHeight)
        }
    }*/

    /**
     * Build child layout as horizontal
     *
     * @param t parent top
     * @param b parent bottom
     * @param childCount child count
     */
    private fun layoutHorizontal(t: Int, b: Int, childCount: Int) {
        val isInit = childViews[0].left == 0
        var offset = 0
        for (index in 0 until childCount) {
            val child = childViews[index]
            val childWidth = child.measuredWidth
            val childHeight = child.measuredHeight
            var top = b - t - childHeight
            if (top > 0) {
                top = (b - t) / 2 - childHeight / 2
            }
            if (isInit) {
                child.layout(0, top, childWidth, top + childHeight)
                if (index > 0) {
                    child.offsetLeftAndRight(offset)
                }
                offset += childWidth
            } else {
                child.layout(child.left, child.top, child.right, child.bottom)
            }
        }
    }

    /**
     * Build child layout as vertical
     *
     * @param l parent left
     * @param r parent right
     * @param childCount child count
     */
    /*private fun layoutVertical(l: Int, r: Int, childCount: Int) {
        val isInit = childViews[0].top == 0
        var offset = 0
        for (index in 0 until childCount) {
            val child = childViews[index]
            val childWidth = child.measuredWidth
            val childHeight = child.measuredHeight
            var left = r - l - childWidth
            if (left > 0) {
                left = (r - l) / 2 - childWidth / 2
            }
            if (isInit) {
                child.layout(left, 0, left + childWidth, childHeight)
                if (index > 0) {
                    child.offsetTopAndBottom(offset)
                }
                offset += childHeight
            } else {
                child.layout(child.left, child.top, child.right, child.bottom)
            }
        }
    }

    private fun requestParentDisallowInterceptTouchEvent(request: Boolean) {
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(request)
        }
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
           ConstantClass.printForDebug("ViewDragLayout onInterceptTouchEvent vdhEnable="+vdhEnable+",getAction="+ev.getAction());

        if (vdhEnable) {
            //val action = MotionEventCompat.getActionMasked(ev)
            val action = ev.getAction() and MotionEvent.ACTION_MASK
           // ConstantClass.printForDebug("ViewDragLayout onInterceptTouchEvent getActionMasked="+ev.getActionMasked());
            if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
                viewDragHelper!!.cancel()
                return false
            }
            var should= viewDragHelper!!.shouldInterceptTouchEvent(ev)
            ConstantClass.printForDebug("ViewDragLayout shouldInterceptTouchEvent should=${should}");
            if(!should){
                dragFlags.forEach { key, value ->
                    val child = getView(key)
                    child?.apply {
                      //  ConstantClass.printForDebug("ViewDragLayout shouldInterceptTouchEvent ev.x=${ev.x} ev.y=${ev.y} x=${x} y=${y}");
                        if(ev.x<x||ev.x>x+width||ev.y<y||ev.y>y+height){
                            return false
                        }
                    }

                }

            }
            return true
        }
        return super.onInterceptTouchEvent(ev)  
    }*/



  //  private var timeDown: Long = 0
    private var nameClickLisener: onNameClickLisener? = null

    interface onNameClickLisener {
        fun onClick()
    }

    fun setOnNameClickLisener(onNameClickLisener: onNameClickLisener?) {
        this.nameClickLisener = onNameClickLisener
    }


    /*override fun onTouchEvent(event: MotionEvent): Boolean {
        ConstantClass.printForDebug("ViewDragLayout onTouchEvent getAction="+event.getAction());
        super.onTouchEvent(event)
        if (vdhEnable) {
           // val action = MotionEventCompat.getActionMasked(event)
            val action = event.action and MotionEvent.ACTION_MASK
            when (action) {
                MotionEvent.ACTION_DOWN -> {
                    dispatchingTime = System.currentTimeMillis() + pressTime
                    if (event.x < width * 0.5) timeDown = System.currentTimeMillis()
                }
                MotionEvent.ACTION_MOVE -> return if (System.currentTimeMillis() >= dispatchingTime) {
                    requestParentDisallowInterceptTouchEvent(true)
                    return vdhProcessTouchEvent(event)
                } else {
                    true
                }
                MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP -> {
                    if (System.currentTimeMillis() < dispatchingTime) {
                        performClick()
                    }
                    if (action == MotionEvent.ACTION_UP||action ==MotionEvent.ACTION_CANCEL) {
                        if (System.currentTimeMillis() - timeDown < 500) {
                            ConstantClass.printForDebug("ViewDragLayout click x="+event.getX()+",viewHalfWidth="+(getWidth()*0.5));
                            if (event.x < width * 0.5) {
                                if (nameClickLisener != null) nameClickLisener!!.onClick()
                            }
                        }
                    }
                    dispatchingTime = -1
                    requestParentDisallowInterceptTouchEvent(false)
                }
                else -> {
                }
            }
            return vdhProcessTouchEvent(event)
        }
        return super.onTouchEvent(event)
    }*/

   /* private fun vdhProcessTouchEvent(event: MotionEvent): Boolean {
        try {
            viewDragHelper!!.processTouchEvent(event)
        } catch (ex: Exception) {
            ex.printStackTrace()
            return false
        }
        ConstantClass.printForDebug("vdhProcessTouchEvent return true");
        return true
    }*/

    override fun computeScroll() {
        if (viewDragHelper!!.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this)
        }
     }

    private fun getView(childId: Int): View? {
        synchronized(childViews) {
            for (view in childViews) {
                if (view.id == childId) {
                    return view
                }
            }
        }
        return null
    }

    /**
     * Sets VDH enable status
     *
     * @param dragEnable true enable, false otherwise
     */
   /* fun setVDHEnable(dragEnable: Boolean) {
        vdhEnable = dragEnable
    }*/

    /**
     * Drags specific view.
     *
     * @param childId specific view id
     * @param x drag distance in x-axis
     * @param y drag distance int y-axis
     */
    fun dragSpecificView(childId: Int, x: Int, y: Int) {
        ConstantClass.printForDebug("dragSpecificView x=${x}")
        val hoverView = getView(childId)
        if (hoverView != null) {
            if (viewDragHelper!!.smoothSlideViewTo(
                    hoverView, hoverView.left + x,
                    hoverView.top + y
                )
            ) {
                ViewCompat.postInvalidateOnAnimation(this)
            }
        }
    }


    /**
     * Resets specific view.
     *
     * @param childId specific view id
     */
    fun resetSpecificView(childId: Int) {
        val hoverView = getView(childId)

        if (hoverView != null) {
            val x = dragDistanceXs[childId]
           // val y = dragDistanceYs[childId]
            val left = x?.startPoint ?: hoverView.left
            val t = hoverView.top
            ConstantClass.printForDebug("resetSpecificView left=${left} ")
            if (viewDragHelper!!.smoothSlideViewTo(hoverView, left, t)) {
                ViewCompat.postInvalidateOnAnimation(this)
            }
        }
    }

    /**
     * Sets view group layout type since we can't extends other layout
     *
     * @param layoutType layout type [.HOVER_FRAME_OVERLAY], [.HOVER_LINEAR_HORIZONTAL],
     * [ ][.HOVER_LINEAR_VERTICAL]
     */
    private fun setLayoutType(@HoverMode layoutType: Int) {
        this.layoutType = layoutType
    }

    /**
     * Sets all child's drag direction flag
     *
     * @param dragDirectionFlag drag flag
     */
   /* private fun setDragDirectionFlag(@DragFlag dragDirectionFlag: Int) {
        addOnLayoutChangeListener(object : OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int,
                oldTop: Int, oldRight: Int, oldBottom: Int
            ) {
                dragFlags.clear()
                val size = childViews.size
                for (i in 0 until size) {
                    val childId = childViews[i].id
                    dragFlags.put(childId, dragDirectionFlag)
                }
                removeOnLayoutChangeListener(this)
            }
        })
    }*/

    /**
     * Sets specific child's drag direction flag
     *
     * @param childId specific child id
     * @param dragDirectionFlag drag flag
     */
    private fun setSpecificDragDirectionFlag(@IdRes childId: Int, @DragFlag dragDirectionFlag: Int)
    {
        addOnLayoutChangeListener(object : OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int,
                oldTop: Int, oldRight: Int, oldBottom: Int)
            {
                val child = getView(childId)
                if (child != null) {
                    dragFlags.put(childId, dragDirectionFlag)
                }
                removeOnLayoutChangeListener(this)
            }
        })
    }

    /**
     * Sets all child's drag distance x
     *
     * @param leftX left distance
     * @param rightX right distance
     */
    private fun setDragX(leftX: Int, rightX: Int)
    {
        addOnLayoutChangeListener(object : OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int,
                oldTop: Int, oldRight: Int, oldBottom: Int
            ) {
                dragDistanceXs.clear()
                val size = childViews.size
                for (i in 0 until size) {
                    val child = childViews[i]
                    // Only the child has drag flag can set drag x
                    val flag = dragFlags[child.id]
                    if (flag > 0) {
                        dragDistanceXs.put(child.id,
                           Distance(
                                child.left,
                                leftX,
                                rightX
                            )
                        )
                        ConstantClass.printForDebug("${dragDistanceXs.get(child.id).toString()}")
                    }
                }
                removeOnLayoutChangeListener(this)
            }
        })
    }

    /**
     * Sets specific child's drag distance x
     *
     * @param childId specific child id
     * @param leftX left distance
     * @param rightX right distance
     */
    /*private fun setSpecificDragX(@IdRes childId: Int, leftX: Int, rightX: Int) {
        addOnLayoutChangeListener(object : OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int,
                oldTop: Int, oldRight: Int, oldBottom: Int
            ) {
                val child = getView(childId)
                if (child != null) {
                    dragDistanceXs.put(childId,
                       Distance(
                            child.left,
                            leftX,
                            rightX
                        )
                    )
                }
                removeOnLayoutChangeListener(this)
            }
        })
    }*/

    /**
     * Sets all child's drag distance y
     *
     * @param topY top distance
     * @param bottomY bottom distance
     */
  /*  private fun setDragY(topY: Int, bottomY: Int) {
        addOnLayoutChangeListener(object : OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int,
                oldTop: Int, oldRight: Int, oldBottom: Int
            ) {
                dragDistanceYs.clear()
                val size = childViews.size
                for (i in 0 until size) {
                    val child = childViews[i]
                    dragDistanceYs.put(child.id,
                        Distance(
                            child.top,
                            topY,
                            bottomY
                        )
                    )
                }
                removeOnLayoutChangeListener(this)
            }
        })
    }*/

    /**
     * Sets specific child's drag distance y
     *
     * @param childId specific child id
     * @param topY top distance
     * @param bottomY bottom distance
     */
    /*private fun setSpecificDragY(@IdRes childId: Int, topY: Int, bottomY: Int) {
        addOnLayoutChangeListener(object : OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int,
                oldTop: Int, oldRight: Int, oldBottom: Int
            ) {
                val child = getView(childId)
                if (child != null) {
                    dragDistanceYs.put(childId,
                        Distance(
                            child.top,
                            topY,
                            bottomY
                        )
                    )
                }
                removeOnLayoutChangeListener(this)
            }
        })
    }*/

    /**
     * Sets edge tracking flag to drag specific child.
     *
     * @param dragChildId specific child id
     * @param edgeFlag tracking flag
     */
    /*private fun setSpecificDragEdgeFlag(@IdRes dragChildId: Int, edgeFlag: Int) {
        this.edgeFlag = edgeFlag
        addOnLayoutChangeListener(object : OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int,
                oldTop: Int, oldRight: Int, oldBottom: Int
            ) {
                val child = getView(dragChildId)
                if (child != null) {
                    edgeViews.put(dragChildId, edgeFlag)
                }
                removeOnLayoutChangeListener(this)
            }
        })
    }*/

    /**
     * Drag view as chain
     *
     * @param chainEnable true enable, false otherwise.
     */
    private fun asChain(chainEnable: Boolean) {
        this.chainEnable = chainEnable
    }

    /**
     * Generate hook list that Target view hooks chain id
     *
     * @param targetId target view that owns hook
     * @param hookId view which be hooked
     */
    /*private fun hookWithSpecificView(targetId: Int, hookId: IntArray) {
        addOnLayoutChangeListener(object : OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int,
                oldTop: Int, oldRight: Int, oldBottom: Int
            ) {
                val child = getView(targetId)
                if (child != null) {
                    val hooks: MutableList<View> = ArrayList()
                    for (id in hookId) {
                        val view = getView(id)
                        if (view != null) {
                            hooks.add(view)
                        }
                    }
                    hookList.put(targetId, hooks)
                }
                removeOnLayoutChangeListener(this)
            }
        })
    }*/

    /**
     * Enable the pull action
     *
     * @param pullEnable true enable, false disable
     */
   /* private fun asPull(pullEnable: Boolean) {
        this.pullEnable = pullEnable
    }*/

    /**
     * Sets the factor of speed
     *
     * @param speedFactor factor that directly proportional to speed
     */
    /*private fun setSpeedFactor(speedFactor: Float) {
        this.speedFactor = speedFactor
    }*/

    /**
     * Sets the press time that trigger drag
     *
     * @param pressTime press time
     */
   /* private fun setPressTime(pressTime: Long) {
        this.pressTime = pressTime
    }*/

    /**
     * VDH callback
     */
    private class CustomViewDragHelperCallback internal constructor(reference: ViewDragLayout) :
        ViewDragHelper.Callback() {
        private val instance: ViewDragLayout?
        override fun onEdgeDragStarted(edgeFlags: Int, pointerId: Int) {
            val size = instance!!.edgeViews.size()
            for (i in 0 until size) {
                if (instance.edgeViews.valueAt(i) == edgeFlags) {
                    val childId = instance.edgeViews.keyAt(i)
                    val edgeChild = instance.getView(childId)
                    if (edgeChild != null) {
                        instance.viewDragHelper!!.captureChildView(edgeChild, pointerId)
                    }
                }
            }
        }

        override fun tryCaptureView(child: View, pointerId: Int): Boolean {
            val flag = instance!!.dragFlags[child.id]
            return flag > 0
        }

        override fun clampViewPositionHorizontal(child: View, left: Int, dx: Int): Int {
            val flag = instance!!.dragFlags[child.id] and (LEFT or RIGHT)
            val distanceX = instance.dragDistanceXs[child.id]
            val hooks = instance.hookList[child.id]
            val adjustedDx = (dx * instance.speedFactor).toInt()
            ConstantClass.printForDebug("clampViewPositionHorizontal flag=${flag} left=${left} dx=${dx}");
            when (flag) {
                LEFT -> if (dx < 0) {
                    if (hooks != null) {
                        for (hooked in hooks) {
                            val hookX = instance.dragDistanceXs[hooked.id]
                            if (hookX != null) {
                                if (hooked.left + adjustedDx >= hookX.min) {
                                    hooked.offsetLeftAndRight(adjustedDx)
                                }
                            }
                        }
                    }
                    if (distanceX != null) {
                        if (left >= distanceX.min) {
                            return child.left + adjustedDx
                        }
                    }
                }
                RIGHT -> if (dx > 0) {
                    if (hooks != null) {
                        for (hooked in hooks) {
                            val hookX = instance.dragDistanceXs[hooked.id]
                            if (hookX != null) {
                                if (hooked.left - adjustedDx <= hookX.max) {
                                    hooked.offsetLeftAndRight(-adjustedDx)
                                }
                            }
                        }
                    }
                    if (distanceX != null) {
                        if (left <= distanceX.max) {
                            return child.left + adjustedDx
                        }
                    }
                }
                LEFT or RIGHT -> {
                    if (hooks != null) {
                        for (hooked in hooks) {
                            val hookX = instance.dragDistanceXs[hooked.id]
                            if (hookX != null) {
                                if (hooked.left + adjustedDx >= hookX.min) {
                                    hooked.offsetLeftAndRight(adjustedDx)
                                } else if (hooked.left - adjustedDx <= hookX.max) {
                                    hooked.offsetLeftAndRight(-adjustedDx)
                                }
                            }
                        }
                    }
                    if (distanceX != null) {
                        if (left >= distanceX.min && left <= distanceX.max) {
                            return child.left + adjustedDx
                        }
                    }
                }
                else -> {
                }
            }
            return child.x.toInt()
        }

        /*override fun clampViewPositionVertical(child: View, top: Int, dy: Int): Int {
            val flag = instance!!.dragFlags[child.id] and (TOP or BOTTOM)
            val distanceY = instance.dragDistanceYs[child.id]
            val adjustedDy = (dy * instance.speedFactor).toInt()
            when (flag) {
                TOP -> if (dy < 0) {
                    if (distanceY != null) {
                        if (top >= distanceY.min) {
                            return child.top + adjustedDy
                        }
                    }
                }
                BOTTOM -> if (dy > 0) {
                    if (distanceY != null) {
                        if (top <= distanceY.max) {
                            return child.top + adjustedDy
                        }
                    }
                }
                TOP or BOTTOM -> if (distanceY != null) {
                    if (top >= distanceY.min && top <= distanceY.max) {
                        return child.top + adjustedDy
                    }
                }
                else -> {
                }
            }
            return child.y.toInt()
        }*/

        override fun getViewVerticalDragRange(child: View): Int {
            return 1
        }

        override fun onViewPositionChanged(
            changedView: View,
            left: Int,
            top: Int,
            dx: Int,
            dy: Int
        ) {
            if (instance!!.chainEnable) {
                when (instance.layoutType) {
                   /* HOVER_FRAME_OVERLAY -> {
                        onHorizontalViewPositionChanged(changedView, dx)
                        onVerticalViewPositionChanged(changedView, dy)
                    }*/
                    HOVER_LINEAR_HORIZONTAL -> onHorizontalViewPositionChanged(changedView, dx)
                 //   HOVER_LINEAR_VERTICAL -> onVerticalViewPositionChanged(changedView, dy)
                    else -> { }
                }
            }
        }

        private fun onHorizontalViewPositionChanged(changedView: View, dx: Int) {
            val size = instance!!.childViews.size
            for (i in 0 until size) {
                val view = instance.childViews[i]
                if (view != changedView) {
                    view.offsetLeftAndRight(dx)
                }
            }
            ViewCompat.postInvalidateOnAnimation(instance)
        }

       /* private fun onVerticalViewPositionChanged(changedView: View, dy: Int) {
            val size = instance!!.childViews.size
            for (i in 0 until size) {
                val view = instance.childViews[i]
                if (view != changedView) {
                    view.offsetTopAndBottom(dy)
                }
            }
            ViewCompat.postInvalidateOnAnimation(instance)
        }

        override fun onViewReleased(releasedChild: View, xvel: Float, yvel: Float) {
            when (instance!!.layoutType) {
                HOVER_FRAME_OVERLAY -> if (instance.pullEnable) {
                    pulledAnimation(releasedChild)
                } else {
                    releasedAnimation(releasedChild, xvel, yvel)
                }
                HOVER_LINEAR_HORIZONTAL -> releasedHorizontalAnimation(releasedChild, xvel)
                HOVER_LINEAR_VERTICAL -> releasedVerticalAnimation(releasedChild, yvel)
                else -> {
                }
            }
        }

      private fun pulledAnimation(releasedChild: View) {
            val x = instance!!.dragDistanceXs[releasedChild.id]
            val y = instance.dragDistanceYs[releasedChild.id]
            val left = x?.min ?: releasedChild.left
            val top = y?.min ?: releasedChild.top
            if (instance.viewDragHelper!!.smoothSlideViewTo(releasedChild, left, top)) {
                ViewCompat.postInvalidateOnAnimation(instance)
            }
        }

        private fun releasedAnimation(releasedChild: View, xvel: Float, yvel: Float) {
            var left = releasedChild.left
            var top = releasedChild.top
            val x = instance!!.dragDistanceXs[releasedChild.id]
            if (x != null) {
                val distanceThreshold = x.max + x.min / 2
                if (xvel < -VELOCITY_THRESHOLD || releasedChild.left < distanceThreshold) {
                    left = x.min
                } else if (xvel > VELOCITY_THRESHOLD || releasedChild.left > distanceThreshold) {
                    left = x.max
                }
            }
            val y = instance.dragDistanceYs[releasedChild.id]
            if (y != null) {
                top = if (yvel < 0) y.min else y.max
            }
            if (instance.viewDragHelper!!.smoothSlideViewTo(releasedChild, left, top)) {
                ViewCompat.postInvalidateOnAnimation(instance)
            }
        }

        private fun releasedHorizontalAnimation(releasedChild: View, xvel: Float) {
            val x = instance!!.dragDistanceXs[releasedChild.id]
            var left = releasedChild.left
            if (x != null) {
                val distanceThreshold = (x.max + x.min) / 2
                if (xvel < -VELOCITY_THRESHOLD || releasedChild.left <= distanceThreshold) {
                    left = x.min
                } else if (xvel > VELOCITY_THRESHOLD || releasedChild.left > distanceThreshold) {
                    left = x.max
                }
                if (instance.viewDragHelper!!.smoothSlideViewTo(
                        releasedChild, left,
                        releasedChild.top
                    )
                ) {
                    ViewCompat.postInvalidateOnAnimation(instance)
                }
            }
            val hooks = instance.hookList[releasedChild.id]
            if (hooks != null) {
                for (hooked in hooks) {
                    var hookedLeft = hooked.left
                    val hookedX = instance.dragDistanceXs[hooked.id]
                    if (hookedX != null) {
                        val distanceThreshold = (hookedX.max + hookedX.min) / 2
                        if (xvel < -VELOCITY_THRESHOLD || hooked.left <= distanceThreshold) {
                            hookedLeft = hookedX.min
                        } else if (xvel > VELOCITY_THRESHOLD || hooked.left > distanceThreshold) {
                            hookedLeft = hookedX.max
                        }
                        if (instance.viewDragHelper!!.smoothSlideViewTo(
                                hooked,
                                hookedLeft,
                                hooked.top
                            )
                        ) {
                            ViewCompat.postInvalidateOnAnimation(instance)
                        }
                    }
                }
            }
        }

       private fun releasedVerticalAnimation(releasedChild: View, yvel: Float) {
            val y = instance!!.dragDistanceYs[releasedChild.id]
            if (y != null) {
                val top = if (yvel < 0) y.min else y.max
                if (instance.viewDragHelper!!.smoothSlideViewTo(
                        releasedChild, releasedChild.left,
                        top
                    )
                ) {
                    ViewCompat.postInvalidateOnAnimation(instance)
                }
            }
        }*/

        init {
            val weakReference = WeakReference(reference)
            instance = weakReference.get()
        }
    }

    class Builder(reference: ViewDragLayout) {
        private val instance: ViewDragLayout?

        /**
         * Sets view group layout type since we can't extends other layout
         *
         * @param layoutType layout type [.HOVER_FRAME_OVERLAY], [ ][.HOVER_LINEAR_HORIZONTAL],
         * [ ][.HOVER_LINEAR_VERTICAL]
         */
        fun setLayoutType(@HoverMode layoutType: Int): Builder {
            instance!!.setLayoutType(layoutType)
            return this
        }

        /**
         * Sets all child's drag direction flag
         *
         * @param dragDirectionFlag drag flag
         */
        /*fun setDragDirectionFlag(@DragFlag dragDirectionFlag: Int): Builder {
            instance!!.setDragDirectionFlag(dragDirectionFlag)
            return this
        }*/

        /**
         * Sets specific child's drag direction flag
         *
         * @param childId specific child id
         * @param dragDirectionFlag drag flag
         */
        fun setSpecificDragDirectionFlag(
            @IdRes childId: Int,
            @DragFlag dragDirectionFlag: Int
        ): Builder {
            instance!!.setSpecificDragDirectionFlag(childId, dragDirectionFlag)
            return this
        }

        /**
         * Sets edge tracking flag to drag specific child.
         *
         * @param dragChildId specific child id
         * @param edgeFlag tracking flag
         */
       /* fun setSpecificDragEdgeFlag(@IdRes dragChildId: Int, edgeFlag: Int): Builder {
            instance!!.setSpecificDragEdgeFlag(dragChildId, edgeFlag)
            return this
        }*/

        /**
         * Sets all child's drag distance x
         *
         * @param leftX left distance
         * @param rightX left distance
         */
        fun setDragX(leftX: Int, rightX: Int): Builder {
            instance!!.setDragX(leftX, rightX)
            return this
        }

        /**
         * Sets specific child's drag distance x
         *
         * @param childId specific child id
         * @param leftX left distance
         * @param rightX right distance
         */
        /*fun setSpecificDragX(@IdRes childId: Int, leftX: Int, rightX: Int): Builder {
            instance!!.setSpecificDragX(childId, leftX, rightX)
            return this
        }*/

        /**
         * Sets all child's drag distance y
         *
         * @param topY top distance
         * @param bottomY bottom distance
         */
      /*  fun setDragY(topY: Int, bottomY: Int): Builder {
            instance!!.setDragY(topY, bottomY)
            return this
        }*/

        /**
         * Sets specific child's drag distance y
         *
         * @param childId specific child id
         * @param topY top distance
         * @param bottomY bottom distance
         */
      /*  fun setSpecificDragY(@IdRes childId: Int, topY: Int, bottomY: Int): Builder {
            instance!!.setSpecificDragY(childId, topY, bottomY)
            return this
        }*/

        /**
         * Drags the layout as chain
         * [NOTICED] this only work at linear mode
         */
        fun asChain(): Builder {
            instance!!.asChain(true)
            return this
        }

        /**
         * Chained view together while drag
         *
         * @param targetId target root view
         * @param chainId the view you want to asChain together
         */
       /* fun hookWith(@IdRes targetId: Int, vararg chainId: Int): Builder {
            instance!!.hookWithSpecificView(targetId, chainId)
            return this
        }*/

        /**
         * No drags, just pull
         */
      /*  fun asPull(): Builder {
            instance!!.asPull(true)
            return this
        }*/

        /**
         * Sets the factor of speed
         *
         * @param speedFactor factor that directly proportional to speed
         */
      /*  fun speedFactor(speedFactor: Float): Builder {
            instance!!.setSpeedFactor(speedFactor)
            return this
        }*/

        /**
         * Sets the press time that trigger drag
         *
         * @param pressTime press time
         */
        /*fun pressTime(pressTime: Long): Builder {
            instance!!.setPressTime(pressTime)
            return this
        }*/

        /**
         * Must be called when you create a layout options
         */
        fun create() {
            instance!!.requestLayout()
            instance.invalidate()
        }

        init {
            val weakReference = WeakReference(reference)
            instance = weakReference.get()
        }
    }

    companion object {
        private const val TAG = "ViewDragLayout"
        var DEBUG = false

        /*--------------------------------
   * Type definitions
   *-------------------------------*/
      //  const val HOVER_FRAME_OVERLAY = 0
        const val HOVER_LINEAR_HORIZONTAL = 1
       // const val HOVER_LINEAR_VERTICAL = 2

        /*--------------------------------
   * Direction definitions
   *-------------------------------*/
        const val LEFT = 1
        const val TOP = 1 shl 1
        const val RIGHT = 1 shl 2
        const val BOTTOM = 1 shl 3
        const val DIRECTION_ALL = LEFT or TOP or RIGHT or BOTTOM

        /*--------------------------------
   * Constant declaration
   *-------------------------------*/
        private const val VELOCITY_THRESHOLD = 50
        //private const val PRESS_TIME: Long = 200
        private const val PRESS_TIME: Long = 50
    }
}