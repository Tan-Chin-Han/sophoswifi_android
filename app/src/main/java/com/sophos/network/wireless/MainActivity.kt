package com.sophos.network.wireless

import android.Manifest
import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
import android.view.WindowManager
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.acelink.library.biometric.BiometricPromptUtils
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.acelink.library.pagetool.CoroutineIO.contextMain
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.acelink.library.search.UPnPSophosDiscovery
import com.acelink.library.sharepreference.SPUtils
import com.acelink.library.utils.blur.GaussianBlur
import com.acelink.library.utils.data.ConstantClass
import com.android.viewer.utils.TimerUtil
import com.android.viewer.utils.ViewUtils
import com.google.android.material.navigation.NavigationView
import com.google.firebase.FirebaseApp
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.sophos.network.R
import com.sophos.network.databinding.ActivityMainDrawerBinding
import com.sophos.network.wireless.gui.ap_edit.APEditFragment

import com.sophos.network.wireless.gui.general.ProgressFragment
import com.sophos.network.wireless.gui.widget.dialog.SophosMessageDialog
import com.sophos.network.wireless.pagetool.ViewModelTool.viewModels
import com.sophos.network.wireless.pagetool.ViewPage2Pages.Companion.mainPages
import com.sophos.network.wireless.pagetool.changeMainPage
import com.sophos.network.wireless.pagetool.changeWelecomsPage
import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.psclass.devcies.BaseSophosDevice
import com.sophos.network.wireless.psclass.gson.data.GetSystem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext



class MainActivity : AppCompatActivity() ,View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private val bindingMain by viewBinding(ActivityMainDrawerBinding::inflate)

    val mainViewModel: MainViewModel by viewModels()

    private var keyguardManager: KeyguardManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val firebaseApp = FirebaseApp.initializeApp(this)
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
        UPnPSophosDiscovery.lastTimeDiscovery=-1
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
           // var flags = window.decorView.systemUiVisibility
          //  flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
           // window.decorView.systemUiVisibility = flags
        }else{
            var controller = window.decorView.windowInsetsController
            controller?.setSystemBarsAppearance(APPEARANCE_LIGHT_STATUS_BARS, APPEARANCE_LIGHT_STATUS_BARS)
        }

        setContentView(bindingMain.root)
        actionBar?.hide()
        initLiveData()

        enableBackFunction()

        var display=resources.displayMetrics
        ConstantClass.printForDebug("MainActivity height=${display.heightPixels} width=${display.widthPixels}")
        changeWelecomsPage(bindingMain.appBarMain.mainBaseFrame,frags_page.wlecome )
        MainViewModel.main=this
        mainViewModel.timerWelecomeEnd(this)

       /* mainViewModel.discoveryDevices(this,{
            if(it.size==0)
            {
                 if(!isFinishing){

                 }

            }else{
                mainViewModel.updateXendDevice(it)

            }
        })*/
        bindingMain.titleInclude.appTitleLeft.setOnClickListener{
            if(bindingMain.drawerLayout.isDrawerOpen(GravityCompat.START)){
                closeDrawer()
            }else{
                openDrawer()
            }
        }


        keyguardManager =  getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager;
        bindingMain.mainAbout.setOnClickListener(this)
        bindingMain.mainSettings.setOnClickListener(this)
        bindingMain.mainLogout.setOnClickListener(this)
        bindingMain.navViewLeft.setNavigationItemSelectedListener(this)
        mainViewModel.initDB()
    }

    var mainMsgDialog:SophosMessageDialog?=null
    fun showNoLockFoundAlready()
    {
        mainMsgDialog=SophosMessageDialog(this,"",
                getString(R.string.suthentication_set_msg),{ dialog ->
                    blurMainFrame(false)
                    finish()
                }).apply {
                setOnDismissListener {
                    blurMainFrame(false)
                }
                show()
                blurMainFrame(true)
            }
    }

    fun showLockTooManyAlready(errorCode:Int)
    {
       var ss=if (errorCode== BiometricPromptUtils.BiometricDisable) getString(R.string.authentication_too_many_disable)
           else getString(R.string.authentication_too_many)

        mainMsgDialog=SophosMessageDialog(this,"",
            ss,{ dialog ->
                blurMainFrame(false)
                finish()
            }).apply {
            setOnDismissListener {
                blurMainFrame(false)
            }
            show()
            blurMainFrame(true)
        }
    }


    fun checkingPersonalLock(block: () -> Unit){
        var b=SPUtils.getBiometric(this)
        var p=SPUtils.getPincode(this)
        ConstantClass.printForDebug( "checkingPersonalLock b=${b} p= $p ")
        if(b==1){
            var strong=bioBiometricsStrongCheck ()
            if (strong == BiometricManager.BIOMETRIC_SUCCESS) {
                /*if (ciphertextWrapper == null){
                    showBiometricPromptForEncryption()
                }*/
                lanuchMain {
                    showBiometricPromptForEncryption { result ->
                        ConstantClass.printForDebug( "showBiometricPromptForEncryption result is $result ")
                        block.invoke()

                    }
                }

            } else if( strong == BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED ){
                ConstantClass.printForDebug("checkingPersonalLock BIOMETRIC_ERROR_NONE_ENROLLED")
                if(p==1){
                    checkDeviceLock(block)
                }else{
                    showNoLockFoundAlready()
                }
            }
            else {

                lockUnavaliable(strong,true)
                showNoLockFoundAlready()
            }
        }else if(p==1){

            checkDeviceLock(block)

        }else{
            block.invoke()
        }
    }


    private fun checkDeviceLock(block: () -> Unit){
        var device=bioBiometricsDeviceCheck ()
        ConstantClass.printForDebug( "checkDeviceLock device $device ")
        if (device == BiometricManager.BIOMETRIC_SUCCESS) {
            val callback = object : BiometricPrompt.AuthenticationCallback() {

                override fun onAuthenticationError(errCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errCode, errString)
                    ConstantClass.printForDebug("bioBiometricsDeviceCheck errCode is $errCode and errString is: $errString")
                    if("Cancel" in errString || "cancel" in errString){
                        finish()
                        return
                    }
                    else if (errCode == android.hardware.biometrics.BiometricPrompt.BIOMETRIC_ERROR_CANCELED ||
                        errCode == android.hardware.biometrics.BiometricPrompt.BIOMETRIC_ERROR_USER_CANCELED
                    )// cancel by code
                    {
                        ConstantClass.printForDebug("BIOMETRIC_ERROR_CANCELED or BIOMETRIC_ERROR_USER_CANCELED finish")
                        finish()
                        return
                    }else if (errCode== android.hardware.biometrics.BiometricPrompt.BIOMETRIC_ERROR_LOCKOUT)//too many times
                    {
                        showLockTooManyAlready( BiometricPromptUtils.BiometricTooMany)
                    }else if (errCode== android.hardware.biometrics.BiometricPrompt.BIOMETRIC_ERROR_LOCKOUT_PERMANENT)//. Biometric authentication is disabled until the user unlocks with strong authenticatio
                    {
                        showLockTooManyAlready( BiometricPromptUtils.BiometricDisable)
                    }
                    if(errCode==0){
                        finish()//background
                    }

                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    ConstantClass.printForDebug("checkDeviceLock onAuthenticationFailed.")
                }

                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    ConstantClass.printForDebug("checkDeviceLock Authentication was successful")
                    block.invoke()
                }
            }
            showPincodePromptForEncryption(callback)
        } else {

            lockUnavaliable(device,false)
            showNoLockFoundAlready()
        }
    }


    fun lockUnavaliable(res: Int,useBiometric:Boolean){
        Log.e("123", "lockUnavaliable res=${res}. SDK_INT=${Build.VERSION.SDK_INT}")
        when (res) {
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
                Log.e("123", "No biometric features available on this device.")
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE
            ->{
                Log.e("123", "Biometric features are currently unavailable.")

            }
            BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED->{
                Log.e("123", "BIOMETRIC_ERROR_UNSUPPORTED")
                if(oldKeyguardSecure()&&!useBiometric){
                    showAuthenticationScreen()
                }
            }
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                Log.e("123", "The user hasn't associated any biometric credentials with their account.")
                // Prompts the user to create credentials that your app accepts.
               /* val enrollIntent = Intent(Settings.ACTION_BIOMETRIC_ENROLL).apply {
                    putExtra(
                        Settings.EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED,
                        if(useBiometric)BiometricManager.Authenticators.BIOMETRIC_STRONG else DEVICE_CREDENTIAL
                    )
                }
                if (enrollIntent.resolveActivity(getPackageManager()) != null){
                    startActivityForResult(enrollIntent, 99999)
                }*/

            }
        }
    }


    fun bioBiometricsStrongCheck():Int{
        val canAuthenticate = BiometricManager.from(applicationContext)
            .canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG)
        return canAuthenticate
    }

    fun bioBiometricsDeviceCheck():Int{
        val canAuthenticate = BiometricManager.from(applicationContext)
            .canAuthenticate(BiometricManager.Authenticators.DEVICE_CREDENTIAL)
        return canAuthenticate
    }


    fun oldKeyguardSecure():Boolean{
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
          //  return keyguardManager!!.isKeyguardSecure&&keyguardManager!!.isDeviceSecure
            return keyguardManager!!.isDeviceSecure
        }
        return false
    }


    fun showAuthenticationScreen() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            Log.e("123","Secure lock isKeyguardSecure=${keyguardManager!!.isKeyguardSecure} isDeviceSecure=${keyguardManager!!.isDeviceSecure} isDeviceLocked=${keyguardManager!!.isDeviceLocked}")
            if ( oldKeyguardSecure())// is secured by a PIN, pattern or password or a SIM card is currently locked.
            {
                // Custom title description
                val intent = keyguardManager!!.createConfirmDeviceCredentialIntent(
                    "This is a custom title",
                    "This is a custom description"
                )
                if (intent != null) {
                    startActivityForResult(intent, 101)//no need
                } else {
                    Log.e("123","Secure lock screen hasn't set up")
                }
            } else {
                Log.e("123","Secure lock screen hasn't set up")
            }
        }

    }

    fun updateTitleRightVisable(  visibility:Int){
        bindingMain.titleInclude.appTitleRight.visibility=visibility
    }


    private fun enableBackFunction(){
        onBackPressedDispatcher.addCallback(this,object :
            OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                ConstantClass.printForDebug("handleOnBackPressed ${MainViewModel.currentPage.value}")
                MainViewModel.currentPage.value?.pageFragment?.onBackPress()

            }
        })
    }

    private fun initLiveData(){

        MainViewModel.currentPage.observe(this@MainActivity, Observer { page: frags_page ->
            when(page){
                frags_page.wlecome->{
                    ConstantClass.printForDebug("wlecome ${MainViewModel.currentPage}")
                   /* supportFragmentManager.beginTransaction().let {

                        it.add(binding.mainBaseFrame.id,frags_page.wlecome.getInstance(),frags_page.wlecome.getTag())
                        it.commit()
                    }*/
                }
                frags_page.leaveWelcome->{
                    ConstantClass.printForDebug("observer leave leaveWelcome isDisconvery=${mainViewModel.isDisconvery}")
                    if(!mainViewModel.isDisconvery){

                        leaveWelcome()
                    }else{
                        onShowProgress(getString(R.string.discovering))
                    }
                 //   binding.navView.visibility=View.GONE
                }
                frags_page.allList->{

                }


                else-> {

                }
            }
        })


    }




    fun changeMainPage(viewpage2Page: frags_page){
        lifecycleScope.launch (contextMain) {

            when(viewpage2Page){
                frags_page.ap_central,frags_page.unknown ->{
                    bindingMain.titleInclude.appTitleBack.visibility=View.VISIBLE
                    bindingMain.titleInclude.appTitleBack.setOnClickListener {
                        changeMainPage(frags_page.allList)
                    }
                }
                frags_page.ap_edit->{
                    bindingMain.titleInclude.appTitleBack.visibility=View.VISIBLE
                    bindingMain.titleInclude.appTitleBack.setOnClickListener {
                        (frags_page.ap_edit.pageFragment as APEditFragment).apply {
                                onBackPress()
                            }

                        }
                }
                frags_page.allList->{
                    bindingMain.titleInclude.appTitleBack.visibility=View.GONE
                    bindingMain.titleInclude.appTitleBack.setOnClickListener(null)
                }
                frags_page.about->{
                    bindingMain.titleInclude.appTitleBack.visibility=View.GONE
                    bindingMain.titleInclude.appTitleBack.setOnClickListener {
                       // frags_page.about.pageFragment?.onBackPress()
                    }
                }
                frags_page.settings->{
                    bindingMain.titleInclude.appTitleBack.visibility=View.GONE
                    bindingMain.titleInclude.appTitleBack.setOnClickListener {
                       // frags_page.settings.pageFragment?.onBackPress()
                    }
                }
            }
            changeMainPage(bindingMain.appBarMain.mainBaseFrame,viewpage2Page)
        }
    }

    private val  blur by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        GaussianBlur(this)
    }

    fun blurMainFrame(isBlur:Boolean)
    {
       ConstantClass.printForDebug("blurMainFrame isBlur=${isBlur}")

            if(isBlur){
                lanuchIO {
                    var bit = blur.renderView(bindingMain.appMainAllLayout,15f)
                   // var bit= FastBlur.doBlur(FastBlur.getScreenshot(bindingMain.appMainAllLayout)!!,25, false)
                    bit?.run {
                        withContext(Dispatchers.Main) {
                            bindingMain.mainFrameBlurImg.setImageBitmap(bit)
                            bindingMain.mainFrameBlurImg.visibility = View.VISIBLE
                        }
                    }


                }
            }else{
                lanuchMain{
                    withContext(Dispatchers.Main){
                        bindingMain.mainFrameBlurImg.setImageResource(android.R.color.transparent)
                        bindingMain.mainFrameBlurImg.visibility=View.GONE
                    }
                }
            }


    }

    fun setStatusDeep(){
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.statusBarColor=ContextCompat.getColor(this,R.color.title_color)
        setDarkStatusBar()
     }

    /*fun setLightStatusBar()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
        {
            window?.insetsController?.setSystemBarsAppearance(APPEARANCE_LIGHT_STATUS_BARS, APPEARANCE_LIGHT_STATUS_BARS)
        } else
        {
            getWindow().getDecorView()?.apply {
                setSystemUiVisibility(getSystemUiVisibility() or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

            }
        }
    }*/


    fun setDarkStatusBar()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
        {
            window?.insetsController?.setSystemBarsAppearance(0, APPEARANCE_LIGHT_STATUS_BARS)
        }else
        {
            window?.decorView?.let { it.systemUiVisibility = it.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv() }
        }
    }

/*    fun setStatusWhite(){
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.statusBarColor=ContextCompat.getColor(this,android.R.color.white)
        setDarkStatusBar()
    }*/



    fun leaveWelcome(){
        checkingPersonalLock {
            ConstantClass.printForDebug("checkingPersonalLock end")
            runOnUiThread {
                ConstantClass.printForDebug("leaveWelcome lanuchMain")
                ViewUtils.showStatusBar(this@MainActivity)
                bindingMain.titleFrame.visibility=View.VISIBLE

                setStatusDeep()
                frags_page.wlecome.pageFragment?.apply {
                    ConstantClass.printForDebug("leaveWelcome welcomeFragement remove")
                    supportFragmentManager.beginTransaction().let {
                        ConstantClass.printForDebug("leaveWelcome welcomeFragement transaction")
                        it.setReorderingAllowed(true)
                        it.remove(this)
                        it.commitAllowingStateLoss()
                        frags_page.wlecome.pageFragment=null
                    }
                }
                bindingMain.appBarMain.mainBaseFrame.removeAllViews()
                changeMainPage(bindingMain.appBarMain.mainBaseFrame,frags_page.allList)
                ConstantClass.printForDebug("leaveWelcome isDisconvery=${mainViewModel.isDisconvery}")

            }

        }

    }

    fun  openDrawer()= bindingMain.drawerLayout.openDrawer(GravityCompat.START)

    fun  closeDrawer()= bindingMain.drawerLayout.closeDrawer(GravityCompat.START)

    fun isDisconvery()=mainViewModel.isDisconvery

    fun getCurrentEditDevice()=mainViewModel.currentEditDevice

    fun onShowProgress(msg: String?) {

        runOnUiThread {
            synchronized(ProgressFragment){
                ConstantClass.printForDebug("onShowProgress")
                if (isProgressShowing()) {
                    ConstantClass.printForDebug("onShowProgress dialog isShowing")
                    return@runOnUiThread
                }
                val frag = ProgressFragment.progress?:ProgressFragment.newInstance()
                if(!isFinishing)
                {
                    frag.showNow(supportFragmentManager, ProgressFragment.mTag)
                    frag.isCancelable = false
                    if (!msg.isNullOrBlank()) {
                        frag.setMsg(msg)
                    }else{
                        //frag.setMsg(getString(R.string.m_waitting))
                        frag.setMsg("")
                    }
                    ProgressFragment.progress=frag
                }

            }

        }
    }

    fun updateProgress(msg: String){
        runOnUiThread {
            synchronized(ProgressFragment){
                ConstantClass.printForDebug("updateProgress")
                if (!isProgressShowing()) {
                  //  ConstantClass.printForDebug("updateProgress dialog not Showing")
                    return@runOnUiThread
                }
              ProgressFragment.progress?.apply {
                  setMsg(msg)
              }

            }

        }
    }


    fun onHideProgress() {
        ConstantClass.printForDebug("onHideProgress")
        runOnUiThread {
           // (supportFragmentManager.findFragmentByTag(ProgressFragment.mTag) as? ProgressFragment)?.dismiss()
            synchronized(ProgressFragment){
                ProgressFragment.progress?.dismiss()
                ProgressFragment.progress=null
            }
        }
    }

    fun isProgressShowing():Boolean{
        //return  ((supportFragmentManager.findFragmentByTag(ProgressFragment.mTag) as? ProgressFragment)?.run { isAdded&&(dialog?.isShowing?:false) })?:false
        return ProgressFragment.progress!=null && ProgressFragment.progress!!.isAdded
    }



    override fun onClick(p0: View) {
        when(p0.id){
            R.id.main_settings->
            {
                changeMainPage(frags_page.settings )
                closeDrawer()
            }
            R.id.main_about->
            {
                changeMainPage(frags_page.about )
                closeDrawer()
            }
            R.id.main_logout->
            {
               // changeMainPage(frags_page.device_list )
            }

        }
    }

  //  private fun isWelecom()=getSupportFragmentManager().findFragmentByTag(WelcomeFragment.mTag)?.isAdded?:false
    fun isWelecom()=frags_page.wlecome.pageFragment!=null

    fun getSophosDevice()=mainViewModel.sophosDevices

    fun startSSDPScanTimer()=mainViewModel.startSSDPScanTimer()


    private fun showBiometricPromptForEncryption(block: (result: BiometricPrompt.AuthenticationResult?)  -> Unit) {
        val canAuthenticate = BiometricManager.from(applicationContext)
            .canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG)
        if (canAuthenticate == BiometricManager.BIOMETRIC_SUCCESS) {
           // cryptographyManager = CryptographyManager()

            var bioPrompt:BiometricPrompt?=null
            var p= SPUtils.getPincode(this)
            val promptInfo = BiometricPromptUtils.createPromptInfo(this,if(p==1) BiometricPromptUtils.bioAuthType.biometric_device else BiometricPromptUtils.bioAuthType.biometric )
            bioPrompt = BiometricPromptUtils.createBiometricPrompt(
                this,
                { errorCode, result ->

                    if (errorCode == BiometricPromptUtils.BiometricReject) {
                        finish()
                        return@createBiometricPrompt
                    } else if (errorCode == BiometricPromptUtils.BiometricTooMany || errorCode == BiometricPromptUtils.BiometricDisable) {
                        showLockTooManyAlready(errorCode)
                        return@createBiometricPrompt
                    }


                    block.invoke(result)
                },
            )

            bioPrompt.authenticate(promptInfo)

        }
    }

    private fun showPincodePromptForEncryption(back:BiometricPrompt.AuthenticationCallback) {
        val canAuthenticate = BiometricManager.from(applicationContext)
            .canAuthenticate(BiometricManager.Authenticators.DEVICE_CREDENTIAL)
        if (canAuthenticate == BiometricManager.BIOMETRIC_SUCCESS) {
            val executor = ContextCompat.getMainExecutor(this)


            val promptInfo = BiometricPromptUtils.createPromptInfo(this,BiometricPromptUtils.bioAuthType.device)
            ConstantClass.printForDebug( "showPincodePromptForEncryption  ")
            BiometricPrompt(this, executor, back).authenticate(promptInfo)
        }
    }


    override fun onStart() {
        super.onStart()
        when(MainViewModel.currentPage.value){
            frags_page.allList,frags_page.ap_edit->{
                startSSDPScanTimer()
            }
        }
    }

    override fun onStop() {
        mainViewModel.releaseSSDP()
        super.onStop()
    }

    override fun onDestroy() {
        ConstantClass.printForDebug("MainActivity onDestroy")
        mainPages.forEach {
            it.pageFragment=null

        }
        mainPages.clear()
        MainViewModel.main=null
        mainViewModel.releaseSSDP()
        mainViewModel.release()
        ProgressFragment.progress=null
        super.onDestroy()
    }

    /**
     * The logic is kept inside onResume instead of onCreate so that authorizing biometrics takes
     * immediate effect.
     */
    override fun onResume() {
        super.onResume()

        /*if (ciphertextWrapper != null) {
            if (SampleAppUser.fakeToken == null) {
                showBiometricPromptForDecryption()
            } else {
                // The user has already logged in, so proceed to the rest of the app
                // this is a todo for you, the developer
               Log.e("123","Login------------------------------------>")
            }
        }*/
    }

    fun getSystemData()=mainViewModel.systemData

     fun getSystem(device:BaseSophosDevice,block: (system:GetSystem?,responseCode:Int) -> Unit)=mainViewModel.getSystem(device,block)



    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menu_ap->{
                changeMainPage(frags_page.allList)
                closeDrawer()
            }
        }
       return false
    }


}