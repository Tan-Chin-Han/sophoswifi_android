package com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.acelink.library.utils.data.ConstantClass
import com.android.viewer.utils.TimerUtil
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.ap_edit.APEditFragment
import com.sophos.network.wireless.gui.ap_edit.APEditViewModel
import com.sophos.network.wireless.gui.widget.dialog.SophosChooseDialog
import com.sophos.network.wireless.gui.widget.dialog.SophosSmallChooseDialog
import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.psclass.SophosEditableStype
import com.sophos.network.wireless.psclass.connection.SophosNetwork
import com.sophos.network.wireless.psclass.gson.data.GetDayTime
import com.sophos.network.wireless.psclass.gson.data.GetSystem
import com.sophos.network.R
import com.sophos.network.databinding.FragApEditDayAndTimeBinding
import com.sophos.network.wireless.gui.APConfigFragment
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.abstractfrag.BaseSubEditFragment
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import java.util.*
enum class DayTimePageMode{
    Normal,Edit_NTP_Enable,Edit_NTP_Disable
}

class DayTimeFragment : BaseSubEditFragment(),View.OnClickListener {
    companion object{
        fun getInstance(): DayTimeFragment {
            return  DayTimeFragment()
        }
       val mTag = "DayTimeFragment"
    }


    val binding by viewBinding(FragApEditDayAndTimeBinding::inflate)

    var dayTimePageMode=DayTimePageMode.Normal

    private var dayLightSavingEable=false

    var ntpServerEnable=true

    private var currentMonth=1

    private var curentTimeZone=0;

    val yearList = (2005..2035).toList().toTypedArray()
    val monthList = arrayOf("January","February","March","April",
                        "May","June","July","August",
                          "September","October","November","December")
    val dayList = (1..31).toList().toTypedArray()
    val hourList = (0..23).toList().toTypedArray()
    val minsecList = (0..59).toList().toTypedArray()
    val zoneList= arrayOf(
        "(GMT-12:00) Eniwetok, Kwajalein, International Date Line West",
        "(GMT-11:00) Midway Island, Samoa",
        "(GMT-10:00) Hawaii",
        "(GMT-09:00) Alaska",
        "(GMT-08:00) Pacific Time (US & Canada)",
        "(GMT-07:00) Arizona",
        "(GMT-07:00) Chihuahua, La Paz, Mazatian",
        "(GMT-07:00) Mountain Time (US & Canada)",
        "(GMT-06:00) Central America",
        "(GMT-06:00) Central Time (US & Canada)",
        "(GMT-06:00) Guadalajara, Mexico City, Monterrey",
        "(GMT-06:00) Saskatchewan",
        "(GMT-05:00) Bogota, Lima, Quito",
        "(GMT-05:00) Eastern Time (US & Canada)",
        "(GMT-05:00) Indiana (East)",
        "(GMT-04:00) Atlantic Time (Canada)",
        "(GMT-04:00) Caracas, La Paz",
        "(GMT-04:00) Santiago",
        "(GMT-03:30) Newfoundland",
        "(GMT-03:00) Brasilia",
        "(GMT-03:00) Buenos Aires, Georgetown",
        "(GMT-03:00) Greenland",
        "(GMT-02:00) Mid-Atlantic",
        "(GMT-01:00) Azores",
        "(GMT-01:00) Cape Verde Is.",
        "(GMT) Casablanca, Monrovia",
        "(GMT) Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London",
        "(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
        "(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague",
        "(GMT+01:00) Brussels, Copenhagen, Madrid, Paris",
        "(GMT+01:00) Sarajevo, Sofija, Warsaw, Zagreb, Skopje, Vilnius",
        "(GMT+01:00) West Central Africa",
        "(GMT+02:00) Athens, Istanbul, Minsk",
        "(GMT+02:00) Bucharest",
        "(GMT+02:00) Cairo",
        "(GMT+02:00) Harare, Pretoria",
        "(GMT+02:00) Helsinki, Riga, Tallinn",
        "(GMT+02:00) Jerusalem",
        "(GMT+03:00) Baghdad",
        "(GMT+03:00) Kuwait, Riyadh",
        "(GMT+03:00) Moscow, St. Petersburg, Volgograd",
        "(GMT+03:00) Nairobi",
        "(GMT+03:30) Tehran",
        "(GMT+04:00) Abu Dhabi, Muscat",
        "(GMT+04:00) Baku, Tbilisi, Yerevan",
        "(GMT+04:30) Kabul",
        "(GMT+05:00) Ekaterinburg",
        "(GMT+05:00) Islamabad, Karachi, Tashkent",
        "(GMT+05:30) Calcutta, Chennai, Mumbai, New Delhi",
        "(GMT+05:45) Kathmandu",
        "(GMT+06:00) Almaty, Novosibirsk",
        "(GMT+06:00) Astana, Dhaka",
        "(GMT+06:00) Sri, Jayawardenepura",
        "(GMT+06:30) Rangoon",
        "(GMT+07:00) Bangkok, Hanoi, Jakarta",
        "(GMT+07:00) Krasnoyarsk",
        "(GMT+08:00) Beijing,  Hong Kong",
        "(GMT+08:00) Irkutsk, Ulaan Bataar",
        "(GMT+08:00) Kuala Lumpur, Singapore",
        "(GMT+08:00) Perth",
        "(GMT+08:00) Taipei, Taiwan",
        "(GMT+09:00) Osaka, Sapporo, Tokyo",
        "(GMT+09:00) Seoul",
        "(GMT+09:00) Yakutsk",
        "(GMT+09:30) Adelaide",
        "(GMT+09:30) Darwin",
        "(GMT+10:00) Brisbane",
        "(GMT+10:00) Canberra, Melbourne, Sydney",
        "(GMT+10:00) Guam, Port Moresby",
        "(GMT+10:00) Hobart",
        "(GMT+10:00) Vladivostok",
        "(GMT+11:00) Magadan, Solamon, New Caledonia",
        "(GMT+12:00) Auckland, Wllington",
        "(GMT+12:00) Fiji, Kamchatka, Marshall Is."
        )
    val serverList = arrayOf(
        arrayOf("User-Defined", ""),
        arrayOf("Global", "0.sophos.pool.ntp.org"),
        arrayOf("Africa", "0.africa.pool.ntp.org"),
        arrayOf("Asia", "0.asia.pool.ntp.org"),
        arrayOf("Europe", "0.europe.pool.ntp.org"),
        arrayOf("North America", "0.north-america.pool.ntp.org"),
        arrayOf("Oceania", "0.oceania.pool.ntp.org"),
        arrayOf("South America", "0.south-america.pool.ntp.org")
    )

    var dataOLD: GetDayTime.DayTimeData?= null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding.apEditServerRegion.setOnClickListener (this)
        ConstantClass.printForDebug("onCreateView ${javaClass.simpleName} ")

        binding.apEditYearTxt.setOnClickListener(this)
        binding.apEditMonthTxt.setOnClickListener(this)
        binding.apEditDayTxt.setOnClickListener(this)
        binding.apEditHoursTxt.setOnClickListener(this)
        binding.apEditMinuteTxt.setOnClickListener(this)
        binding.apEditSecondTxt.setOnClickListener(this)
        binding.apEditDayTimeAcquireTxt.setOnClickListener(this)
        binding.apEditTimeZoneLay.setOnClickListener(this)
        binding.apEditNtpServerNtpCheck.setOnClickListener(this)
        binding.apEditDaylightSavingText.setOnClickListener(this)
        updateDayTimePageMode()
        binding.apEditTimeZoneTxt.isSelected=true
        return binding.root
    }

    private fun syncPhoneTime(){
        val cal = Calendar.getInstance()
        cal.time = Date()
        var year=cal.get(Calendar.YEAR)
        binding.apEditYearTxt.text=year.toString()

        var month=cal.get(Calendar.MONTH)
        currentMonth=month+1
        binding.apEditMonthTxt.text=monthList.get(currentMonth-1)

        var day=cal.get(Calendar.DAY_OF_MONTH)
        binding.apEditDayTxt.text=day.toString()

        var hour=cal.get(Calendar.HOUR_OF_DAY)
        binding.apEditHoursTxt.text=hour.toString()

        var miniute=cal.get(Calendar.MINUTE)
        binding.apEditMinuteTxt.text=miniute.toString()

        var sec=cal.get(Calendar.SECOND)
        binding.apEditSecondTxt.text=sec.toString()
    }


    fun updateDayTimePageMode(){
        when(dayTimePageMode){
            DayTimePageMode.Normal->{

                ColorDrawable(ContextCompat.getColor(requireContext(),android.R.color.transparent)).apply {
                    binding.apEditYearFrame.background=this
                    binding.apEditMonthFrame.background=this
                    binding.apEditDayFrame.background=this
                    binding.apEditHoursFrame.background=this
                    binding.apEditMiniuteFrame.background=this
                    binding.apEditSecondFrame.background=this
                    binding.apEditTimeZoneFrame.background=this
                  //  binding.apEditUpdateIntervalFrame.background=this
                   binding.apEditServerName.background=this


                }
                SophosEditableStype.disableEditText(binding.apEditUpdateIntervalTxt)
                requireContext().getDrawable(R.drawable.background_secondary_diable).apply {
                    binding.apEditDayTimeAcquire.background=this
                }
                ContextCompat.getColor(requireContext(),R.color.sophos_secondary_disable_color).apply {
                    binding.apEditDayTimeAcquireTxt.setTextColor(this)
                }
                binding.apEditYearImg.visibility=View.INVISIBLE
                binding.apEditMonthImg.visibility=View.INVISIBLE
                binding.apEditDayImg.visibility=View.INVISIBLE
                binding.apEditHoursImg.visibility=View.INVISIBLE
                binding.apEditMinuteImg.visibility=View.INVISIBLE
                binding.apEditSecondImg.visibility=View.INVISIBLE

                binding.apEditServerRegionImg.visibility=View.INVISIBLE
                binding.apEditTimeZoneImg.visibility=View.INVISIBLE
                binding.apEditDaylightSavingImg.visibility=View.INVISIBLE


                ResourcesCompat.getFont(requireContext(), R.font.sophossans_regular).apply {

                }
                binding.apEditNtpServerNtpCheck.setImageResource(if(ntpServerEnable)R.mipmap.on_disable else R.mipmap.off_disable)
            }
            DayTimePageMode.Edit_NTP_Enable,DayTimePageMode.Edit_NTP_Disable->{
                if(dayTimePageMode==DayTimePageMode.Edit_NTP_Enable){
                    SophosEditableStype.enableEditText( binding.apEditServerName)
                    SophosEditableStype.enableEditText(binding.apEditUpdateIntervalTxt)
                    requireContext().getDrawable(R.drawable.background_secondary_diable).apply {
                        binding.apEditDayTimeAcquire.background=this
                    }
                    ContextCompat.getColor(requireContext(),R.color.sophos_secondary_disable_color).apply {
                        binding.apEditDayTimeAcquireTxt.setTextColor(this)
                    }
                    binding.apEditYearImg.visibility=View.INVISIBLE
                    binding.apEditMonthImg.visibility=View.INVISIBLE
                    binding.apEditDayImg.visibility=View.INVISIBLE
                    binding.apEditHoursImg.visibility=View.INVISIBLE
                    binding.apEditMinuteImg.visibility=View.INVISIBLE
                    binding.apEditSecondImg.visibility=View.INVISIBLE

                    binding.apEditServerRegionImg.visibility=View.VISIBLE
                    binding.apEditDaylightSavingImg.visibility=View.VISIBLE

                    requireContext().getDrawable(R.drawable.background_default_textbox_5dp).apply {
                        binding.apEditServerName.background=this
                    }
                }else{
                    SophosEditableStype.disableEditText( binding.apEditServerName)
                    SophosEditableStype.disableEditText(binding.apEditUpdateIntervalTxt)
                    requireContext().getDrawable(R.drawable.background_secondary_enable).apply {
                        binding.apEditDayTimeAcquire.background=this
                    }
                    ContextCompat.getColor(requireContext(),R.color.sophos_secondary_enable_color).apply {
                        binding.apEditDayTimeAcquireTxt.setTextColor(this)
                    }
                    binding.apEditYearImg.visibility=View.VISIBLE
                    binding.apEditYearImg.setImageResource(R.mipmap.drop_down)
                    binding.apEditMonthImg.visibility=View.VISIBLE
                    binding.apEditMonthImg.setImageResource(R.mipmap.drop_down)
                    binding.apEditDayImg.visibility=View.VISIBLE
                    binding.apEditDayImg.setImageResource(R.mipmap.drop_down)
                    binding.apEditHoursImg.visibility=View.VISIBLE
                    binding.apEditHoursImg.setImageResource(R.mipmap.drop_down)
                    binding.apEditMinuteImg.visibility=View.VISIBLE
                    binding.apEditMinuteImg.setImageResource(R.mipmap.drop_down)
                    binding.apEditSecondImg.visibility=View.VISIBLE
                    binding.apEditSecondImg.setImageResource(R.mipmap.drop_down)
                    binding.apEditServerRegionImg.visibility=View.INVISIBLE
                    binding.apEditDaylightSavingImg.visibility=View.INVISIBLE
                    ColorDrawable(ContextCompat.getColor(requireContext(),android.R.color.transparent)).apply{
                        binding.apEditServerName.background=this
                    }
                }
                binding.apEditTimeZoneImg.visibility=View.VISIBLE


                var dayLightStr=if(dayLightSavingEable) R.string.enable else  R.string.disable
                binding.apEditDaylightSavingText.setText(dayLightStr)

                binding.apEditNtpServerNtpCheck.setImageResource(if(ntpServerEnable)R.mipmap.on_enable else R.mipmap.off_enable)

            }

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }


    var serverRegionDialog:SophosChooseDialog<Array<String>>?=null
    private set
    var ZoneDialog:SophosChooseDialog<String>?=null
        private set
    var yearDialog:SophosSmallChooseDialog<Int>?=null
        private set
    var monthDialog:SophosSmallChooseDialog<String>?=null
        private set
    var dayDialog:SophosSmallChooseDialog<Int>?=null
        private set
    var hourDialog:SophosSmallChooseDialog<Int>?=null
        private set
    var minDialog:SophosSmallChooseDialog<Int>?=null
        private set
    var secDialog:SophosSmallChooseDialog<Int>?=null
        private set
    override fun onClick(v: View) {

        when(dayTimePageMode){
            DayTimePageMode.Edit_NTP_Enable,DayTimePageMode.Edit_NTP_Disable->{
                when(v.id){
                    R.id.ap_edit_ntp_server_ntp_check->{
                        ntpServerEnable=!ntpServerEnable
                        dayTimePageMode=if(ntpServerEnable)DayTimePageMode.Edit_NTP_Enable else DayTimePageMode.Edit_NTP_Disable
                        binding.apEditNtpServerNtpCheck.setImageResource(if(ntpServerEnable)R.mipmap.on_enable else R.mipmap.off_enable)
                        updateDayTimePageMode()
                    }
                    R.id.ap_edit_time_zone_lay->{
                        ZoneDialog=SophosChooseDialog<String>(requireActivity(),"",zoneList.toMutableList(),0,false,false,{dialog,pos->
                            curentTimeZone=pos
                            var zone=zoneList.get(pos)
                            ConstantClass.printForDebug("SophosChooseDialog zone ${zone}")
                            binding.apEditTimeZoneTxt.text=zone.toString()
                            dialog.dismiss()
                            (requireActivity() as MainActivity).blurMainFrame(false)
                        }).apply {
                            adjustHeight60Percent=true

                            setOnDismissListener {
                                binding.apEditTimeZoneImg.setImageResource(R.mipmap.drop_down)
                                (requireActivity() as MainActivity).blurMainFrame(false)
                            }
                            show()
                        }
                        (requireActivity() as MainActivity).blurMainFrame(true)
                        binding.apEditTimeZoneImg.setImageResource(R.mipmap.drop_up)

                    }

                }
                if(dayTimePageMode==DayTimePageMode.Edit_NTP_Disable){
                    when(v.id){
                        R.id.ap_edit_year_txt->{
                            yearDialog=SophosSmallChooseDialog<Int>(requireActivity(),"",yearList.toMutableList(),0,false,{ dialog, pos->
                                var year=yearList.get(pos)
                                ConstantClass.printForDebug("SophosChooseDialog year ${year}")
                                binding.apEditYearTxt.text=year.toString()
                                dialog.dismiss()
                                (requireActivity() as MainActivity).blurMainFrame(false)
                            }).apply {
                                isAdjustHeight=true

                                setOnDismissListener {
                                    (requireActivity() as MainActivity).blurMainFrame(false)
                                    binding.apEditYearImg.setImageResource( R.mipmap.drop_down);
                                }
                                show()
                            }
                            (requireActivity() as MainActivity).blurMainFrame(true)
                            binding.apEditYearImg.setImageResource( R.mipmap.drop_up);
                        }
                        R.id.ap_edit_month_txt->{
                            monthDialog=SophosSmallChooseDialog<String>(requireActivity(),"",monthList.toMutableList(),0,false,{dialog,pos->
                                currentMonth=pos+1
                                var month=monthList.get(currentMonth-1)
                                ConstantClass.printForDebug("SophosChooseDialog year ${month}")
                                binding.apEditMonthTxt.text=month.toString()
                                dialog.dismiss()
                                (requireActivity() as MainActivity).blurMainFrame(false)
                            }).apply {
                                isAdjustHeight=true
                                setOnDismissListener {
                                    (requireActivity() as MainActivity).blurMainFrame(false)
                                    binding.apEditMonthImg.setImageResource( R.mipmap.drop_down);
                                }
                                show()
                            }
                            (requireActivity() as MainActivity).blurMainFrame(true)
                            binding.apEditMonthImg.setImageResource(R.mipmap.drop_up);
                        }
                        R.id.ap_edit_day_txt->{
                            dayDialog=SophosSmallChooseDialog<Int>(requireActivity(),"",dayList.toMutableList(),0,false,{dialog,pos->
                                var month=dayList.get(pos)
                                ConstantClass.printForDebug("SophosChooseDialog month ${month}")
                                binding.apEditDayTxt.text=month.toString()
                                dialog.dismiss()
                                (requireActivity() as MainActivity).blurMainFrame(false)
                            }).apply {
                                isAdjustHeight=true

                                setOnDismissListener {
                                    (requireActivity() as MainActivity).blurMainFrame(false)
                                    binding.apEditDayImg.setImageResource( R.mipmap.drop_down);
                                }
                                show()
                            }
                            (requireActivity() as MainActivity).blurMainFrame(true)
                            binding.apEditDayImg.setImageResource( R.mipmap.drop_up);
                        }
                        R.id.ap_edit_hours_txt->{
                            hourDialog=SophosSmallChooseDialog<Int>(requireActivity(),"",hourList.toMutableList(),0,false,{dialog,pos->
                                var hour=hourList.get(pos)
                                ConstantClass.printForDebug("SophosChooseDialog hour ${hour}")
                                binding.apEditHoursTxt.text=hour.toString()
                                dialog.dismiss()
                                (requireActivity() as MainActivity).blurMainFrame(false)
                            }).apply {
                                isAdjustHeight=true

                                setOnDismissListener {
                                    (requireActivity() as MainActivity).blurMainFrame(false)
                                    binding.apEditHoursImg.setImageResource( R.mipmap.drop_down);
                                }
                                show()
                            }
                            (requireActivity() as MainActivity).blurMainFrame(true)
                            binding.apEditHoursImg.setImageResource( R.mipmap.drop_up);
                        }
                        R.id.ap_edit_minute_txt->{
                            minDialog=SophosSmallChooseDialog<Int>(requireActivity(),"",minsecList.toMutableList(),0,false,{dialog,pos->
                                var min=minsecList.get(pos)
                                ConstantClass.printForDebug("SophosChooseDialog min ${min}")
                                binding.apEditMinuteTxt.text=min.toString()
                                dialog.dismiss()
                                (requireActivity() as MainActivity).blurMainFrame(false)
                            }).apply {
                                isAdjustHeight=true

                                setOnDismissListener {
                                    (requireActivity() as MainActivity).blurMainFrame(false)
                                    binding.apEditMinuteImg.setImageResource(R.mipmap.drop_down)
                                }
                                show()
                            }
                            (requireActivity() as MainActivity).blurMainFrame(true)
                            binding.apEditMinuteImg.setImageResource(R.mipmap.drop_up)
                        }
                        R.id.ap_edit_second_txt->{
                            secDialog=SophosSmallChooseDialog<Int>(requireActivity(),"",minsecList.toMutableList(),0,false,{dialog,pos->
                                var sec=minsecList.get(pos)
                                ConstantClass.printForDebug("SophosChooseDialog sec ${sec}")
                                binding.apEditSecondTxt.text=sec.toString()
                                dialog.dismiss()
                                (requireActivity() as MainActivity).blurMainFrame(false)
                            }).apply {
                                isAdjustHeight=true

                                setOnDismissListener {
                                    (requireActivity() as MainActivity).blurMainFrame(false)
                                    binding.apEditSecondImg.setImageResource( R.mipmap.drop_down);
                                }
                                show()
                            }
                            (requireActivity() as MainActivity).blurMainFrame(true)
                            binding.apEditSecondImg.setImageResource( R.mipmap.drop_up);
                        }
                        R.id.ap_edit_day_time_acquire_txt->{
                            syncPhoneTime()
                        }
                    }
                }else if(dayTimePageMode==DayTimePageMode.Edit_NTP_Enable){
                    when(v.id){
                        R.id.ap_edit_server_region->{
                            serverRegionDialog=SophosChooseDialog<Array<String>>(requireActivity(),"",serverList.toMutableList(),0,false,false,{dialog,pos->
                                var server=serverList.get(pos)
                                ConstantClass.printForDebug("SophosChooseDialog choose ${Arrays.toString(server)}")
                                // binding.itemWireless24gBands.apEditAuthenticationType.setText(getString(auth))
                                binding.apEditServerRegion.text=server.get(0)
                                binding.apEditServerName.setText(server.get(1))
                                dialog.dismiss()
                                (requireActivity() as MainActivity).blurMainFrame(false)
                                if(pos==0){
                                    SophosEditableStype.enableEditText(binding.apEditServerName)

                                }else{
                                    SophosEditableStype.disableEditText(binding.apEditServerName)
                                    requireContext().getDrawable(R.drawable.background_default_textbox_5dp).apply {
                                        binding.apEditServerName.background=this
                                    }
                                }
                            }).apply {
                                setOnDismissListener {
                                    binding.apEditServerRegionImg.setImageResource(R.mipmap.drop_down)
                                    (requireActivity() as MainActivity).blurMainFrame(false)
                                }
                                show()
                            }
                            (requireActivity() as MainActivity).blurMainFrame(true)
                            binding.apEditServerRegionImg.setImageResource(R.mipmap.drop_up)

                        }
                        R.id.ap_edit_daylight_saving_text->{

                            dayLightSavingEable=!dayLightSavingEable
                            var dayLightStr=if(dayLightSavingEable) R.string.enable else  R.string.disable
                            binding.apEditDaylightSavingText.setText(dayLightStr)
                        }
                    }

                }
            }

            DayTimePageMode.Normal->{

            }
        }
    }

    // 0:fail 1:success 2:no Change
    suspend fun updateValue():Int{
        kotlin.runCatching {
        var data= GetDayTime.DayTimeData().apply {

                local_time_year =  binding.apEditYearTxt.text.toString().toInt()
                //local_time_month=binding.apEditMonthTxt.text.toString().toInt()
                local_time_month=currentMonth
                local_time_day=binding.apEditDayTxt.text.toString().toInt()
                local_time_hour=binding.apEditHoursTxt.text.toString().toInt()
                local_time_minute=binding.apEditMinuteTxt.text.toString().toInt()
                local_time_seconde=binding.apEditSecondTxt.text.toString().toInt()

                time_zone=curentTimeZone
                ntp_enabled=if(ntpServerEnable)1 else 0
                ntp_server_name=binding.apEditServerName.text.toString()

                ntp_update_interval= binding.apEditUpdateIntervalTxt.text.toString().toInt()


                auto_daylight_save=if(dayLightSavingEable)1 else 0

            }
                if(data.equalData(dataOLD)){
                    return 2
                }
                getMainActivity()?.onShowProgress(null)
                var daytime= SophosNetwork.retryAsynBoolean {
                    runBlocking {
                        SophosNetwork.setDayTime(getCurrentEditDevice()!!,data)
                    }
                }
                if(daytime.await()){
                    var reload= SophosNetwork.setReload(getCurrentEditDevice()!!)
                    if(reload!=null&&reload>0)   {
                        ConstantClass.printForDebug("setDayTime Reload =${reload} xxxxxxxxxxxxxxxxxxxx")
                        viewModel()!!.countReload(reload)
                        delay((reload*1000).toLong())
                        viewModel()!!.removeCountReload()
                    }
                }else return 0

                getDayTime()
                getMainActivity()?.onHideProgress()
                return if(daytime.await()) 1 else 0
        }//.onFailure { it.printStackTrace() }
        getMainActivity()?.onHideProgress()
        return 0
    }

    private fun updateDateTimeUI(dayTime:  GetDayTime){
        dayTime?.data?.apply {
            dataOLD=this
            binding.apEditYearTxt.setText(local_time_year.toString());
            currentMonth=local_time_month?:1
            binding.apEditMonthTxt.setText(monthList.get(currentMonth-1));
            binding.apEditDayTxt.setText(local_time_day.toString());
            binding.apEditHoursTxt.setText(local_time_hour.toString());
            binding.apEditMinuteTxt.setText(local_time_minute.toString());
            binding.apEditSecondTxt.setText(local_time_seconde.toString());

            binding.apEditServerName.setText(ntp_server_name)
            var hasServer=false
            serverList.forEach {
                if( it.get(1)==ntp_server_name){
                    binding.apEditServerRegion.text=it.get(0)
                    hasServer=true
                    return@forEach
                }
            }
            if(!hasServer){
                binding.apEditServerRegion.text=serverList.get(0).get(0)//user defined
            }
            curentTimeZone=time_zone?:0
            binding.apEditTimeZoneTxt.setText(zoneList.get(curentTimeZone));


            binding.apEditUpdateIntervalTxt.setText(ntp_update_interval.toString());
            if(ntp_enabled==0){
                ntpServerEnable=false
            }else{
                ntpServerEnable=true
            }
            binding.apEditNtpServerNtpCheck.setImageResource(if(ntpServerEnable)R.mipmap.on_disable else R.mipmap.off_disable)
            if(auto_daylight_save==0){
                dayLightSavingEable=false
            }else{
                dayLightSavingEable=true
            }
            var dayLightStr=if(dayLightSavingEable) R.string.enable else  R.string.disable
            binding.apEditDaylightSavingText.setText(dayLightStr)


        }

    }

    fun onSelect(){
        lanuchIO {
            getMainActivity()?.onShowProgress(null)
            getDayTime()
            getMainActivity()?.onHideProgress()
        }
    }

    private suspend fun getDayTime(){
            var daytime=  SophosNetwork.getDayTime(getCurrentEditDevice()!!)
            lanuchMain {
                daytime?.let { updateDateTimeUI(it) }
            }


    }
}