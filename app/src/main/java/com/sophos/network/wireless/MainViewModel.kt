package com.sophos.network.wireless

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Build
import android.provider.Settings.Secure
import androidx.annotation.RequiresApi
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.acelink.library.common.annotations.Definitions
import com.acelink.library.pagetool.CoroutineIO
import com.acelink.library.pagetool.CoroutineIO.contextMain
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.acelink.library.search.UPnPSophosDiscovery
import com.acelink.library.utils.data.ConstantClass
import com.android.viewer.utils.TimerUtil
import com.sophos.network.wireless.gui.widget.dialog.SophosMessageDialog
import com.sophos.network.wireless.gui.widget.dialog.SophosYesNoDialog
import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.psclass.gson.data.GetSystem
import com.sophos.network.R
import com.sophos.network.wireless.gui.APConfigFragment
import com.sophos.network.wireless.gui.ap_edit.APEditFragment
import com.sophos.network.wireless.gui.central.APCentralFragment
import com.sophos.network.wireless.gui.devices.devicelist.DeviceAllListFragment
import com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter.MyPageMode
import com.sophos.network.wireless.psclass.connection.SophosNetwork
import com.sophos.network.wireless.psclass.devcies.BaseSophosDevice
import com.sophos.network.wireless.psclass.sharepreference.device.SophosAccount
import kotlinx.coroutines.coroutineScope

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File
import java.io.FileNotFoundException
import java.util.*
import java.util.concurrent.TimeoutException
import kotlin.collections.ArrayList


class MainViewModel(application: Application) : AndroidViewModel(application) {
    var sophosDevices=java.util.ArrayList<BaseSophosDevice>()
    var currentEditDevice: BaseSophosDevice?=null

    private var timerSSDP =TimerUtil()
    companion object{
        val currentPage= MutableLiveData<frags_page>().apply { value=frags_page.wlecome }
        var main:MainActivity?=null
    }

    fun initDB(){
        try {
            var all=SophosAccount.getAllDevice()
            for ((key, data) in all){
                ConstantClass.printForDebug("initDB dev mac=${key} data=${data}")
                if (sophosDevices.find { it.mac?.lowercase()==key }==null){
                    var base= BaseSophosDevice()
                    base.mac=key
                    base.new=false
                    base.deviceName=data?.name
                    // base.location="192.168.1.4:10443"
                    base.operatorMode=data?.operatoer
                    base.timeNotFound=1
                    sophosDevices.add(base)
                    ConstantClass.printForDebug("initDB dev mac=${base.mac} deviceName=${base.deviceName}")
                }
            }
        }catch (e:Exception){
            e.printStackTrace()
        }


        //ConstantClass.printForDebug("initDB all=${Arrays.toString(all.toTypedArray())}")
        /*viewModelScope.launch (CoroutineIO.contextIO) {
            room = RoomDB(main!!.baseContext)
            var allRoom = room.db.sophosDevices().getAll()
            ConstantClass.printForDebug("DB data=${Arrays.toString(allRoom.toTypedArray())}")

        }*/
    }

    var isDisconvery=false

    fun discoveryDevices(context: Context, block:(devices: ArrayList<BaseSophosDevice>)->Unit){
        UPnPSophosDiscovery.lastTimeDiscovery =System.currentTimeMillis()
        viewModelScope.launch (CoroutineIO.contextIO)
        {
            isDisconvery=true
            ConstantClass.printForDebug("discoveryGateWay-------->")
            var devices= UPnPSophosDiscovery.getDevice(context =context);
            try
            {
                while(main?.isWelecom()?:false){//is welcome page,wait wlecom page end
                    // ConstantClass.printForDebug("discoveryGateWay isWelecom=${isWelecom()}")
                    Thread.sleep(10)
                }
                if(currentPage.value==frags_page.leaveWelcome){
                    viewModelScope.launch(contextMain){
                        main?.leaveWelcome()
                    }

                }
            }catch (e:Exception){e.printStackTrace()}
            ConstantClass.printForDebug("discoveryGateWay--------device size=${devices.size}")
           // main?.onHideProgress()
            isDisconvery=false
            UPnPSophosDiscovery.lastTimeDiscovery =System.currentTimeMillis()
            block.invoke(devices)

        }
    }


    private var welcomeTimer: TimerUtil?=null
    fun timerWelecomeEnd(main: MainActivity){
        ConstantClass.printForDebug("welcome","welcome timer start")
        if (welcomeTimer == null) {
            welcomeTimer = TimerUtil()
        }
        welcomeTimer!!.startTimer({
            ConstantClass.printForDebug("welcome","welcome timer invoke")

                //currentPage.postValue(frags_page.leaveWelcome)
               // main.changeMainPage(frags_page.dashboard)
                ConstantClass.printForDebug("welcome","welcome timer end")
                main.leaveWelcome()
                welcomeTimer?.removeMyTimer()
                welcomeTimer=null

        },2000,0)
    }




   /* fun isRooted(context: Context): Boolean {
        val isEmulator = isEmulator(context)
        val buildTags = Build.TAGS
        return if (!isEmulator && buildTags != null && buildTags.contains("test-keys")) {
            true
        } else {
            var file = File("/system/app/Superuser.apk")
            if (file.exists()) {
                true
            } else {
                file = File("/system/xbin/su")
                !isEmulator && file.exists()
            }
        }
    }

    private fun isEmulator(context: Context): Boolean {
        val androidId = Secure.getString(context.contentResolver, "android_id")
        return "sdk" == Build.PRODUCT || "google_sdk" == Build.PRODUCT || androidId == null
    }*/

    fun updateXendDevice(devices:ArrayList<BaseSophosDevice>)
    {
            var start=System.currentTimeMillis()
            sophosDevices.forEach {
                it.timeNotFound++
            }
            devices.forEach {
                var find=sophosDevices!!.find { sophos->
                    sophos.mac==it.mac
                }
                ConstantClass.printForDebug("updateXendDevice find mac=${find?.mac}")
                if(find==null){
                    it.timeNotFound=0

                    it.new=true
                    sophosDevices?.add(it)
                }else{
                    find.timeNotFound=0
                    find.copy(it)
                   // find.operatorMode=1
                }
                //check news amd if name has change and update
                it.mac?.let { it1 ->
                    var device=SophosAccount.getMyDevice(it1)
                    //ConstantClass.printForDebug("updateXendDevice getMyDevice mac=${find?.mac} device=${device}")
                    if(device!=null ){
                        it.new=false
                        if(!it.deviceName.isNullOrEmpty()&&it.deviceName!=device.name){
                            device?.apply {
                                name =it.deviceName!!
                                operatoer=it.operatorMode
                                SophosAccount.setMyDevice(it1,this)
                            }

                        }
                    }else{
                        it.new=true
                    }
                }
            }
        /*sophosDevices.forEach {
            it.operatorMode=1
        }*/
        //remove new device no found this time
        for(i in sophosDevices.size-1 downTo 0){
            var xend=sophosDevices.get(i)
            if(xend.timeNotFound!=0){
                if(xend.mac?.run { SophosAccount.isMyDevice(this) }.isNullOrEmpty()){
                    sophosDevices.removeAt(i)
                }
            }
        }


        ConstantClass.printForDebug("updateXendDevice time =${(System.currentTimeMillis()-start)/1000}")

    }

    fun startSSDPScanTimer()
    {
        timerSSDP.removeMyTimer()
        timerSSDP.startTimer({

            var current=System.currentTimeMillis()
           // ConstantClass.printForDebug("startSSDPScanTimer  ${((current- UPnPSophosDiscovery.lastTimeDiscovery)/1000).toInt()}")

            if(UPnPSophosDiscovery.lastTimeDiscovery>0
               // &&fragment.getViewPager2Adapter()?.deviceMyAllListFragment?.myPageMode!= MyPageMode.NoDevice
                &&current- UPnPSophosDiscovery.lastTimeDiscovery>UPnPSophosDiscovery.ssdpDiscoveryInterval)
            {
                UPnPSophosDiscovery.lastTimeDiscovery =System.currentTimeMillis()
                ssdpScan()
            }
        },0,1000)

    }

    @Synchronized
    private fun ssdpScan(){
        ConstantClass.printForDebug("ssdpScan")
        when( currentPage.value){
            frags_page.allList,frags_page.ap_edit,frags_page.ap_central->
            {
                discoveryDevices(main!!,{
                    updateXendDevice(it)
                    lanuchMain{
                        ConstantClass.printForDebug("ssdpScan get currentPage=${currentPage.value}")
                        currentPage.value!!.pageFragment?.apply {
                            when( currentPage.value)
                            {
                                frags_page.allList->(this as DeviceAllListFragment).notificationAllSlaveUpdate()
                                frags_page.ap_edit,frags_page.ap_central->{
                                    (this as APConfigFragment).updateDiscoveryEnd()
                                }
                            }
                        }
                    }
                })
            }

        }

    }

    fun releaseSSDP(){
        timerSSDP.removeMyTimer()
    }

    fun release(){
        main=null
        frags_page.wlecome.pageFragment=null
        frags_page.allList.pageFragment=null
        currentPage.apply { value=frags_page.wlecome }
    }

    var systemData: GetSystem?=null
    fun getSystem(device:BaseSophosDevice,block: (system:GetSystem?,responseCode:Int) -> Unit){
        lanuchIO {

            main!!.onShowProgress(null)
            var info=  SophosNetwork.retryAsyn {
                runBlocking {
                    SophosNetwork.getSystemInfo(device)
                }
            }?.await()
            if(info==null){
                block.invoke(info,0)
            }else if(info.errorCode==401){
                block.invoke(null,info.errorCode)
            }else
            {
                info?.let {
                    systemData=it
                }
                block.invoke(info,0)

            }

            main!!.onHideProgress()
        }
    }

}