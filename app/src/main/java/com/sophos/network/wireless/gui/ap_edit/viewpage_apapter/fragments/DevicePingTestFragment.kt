package com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments

import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.android.viewer.utils.TimerUtil
import com.sophos.network.wireless.gui.ap_edit.APEditFragment
import com.sophos.network.wireless.gui.ap_edit.APEditViewModel
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.abstractfrag.DeviceAbstractTestFragment
import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.psclass.connection.SophosNetwork
import com.sophos.network.R

class DevicePingTestFragment : DeviceAbstractTestFragment() {
    companion object{
        fun getInstance(): DevicePingTestFragment {
            return  DevicePingTestFragment()
        }
       val mTag = "DevicePingTestFragment"
    }
    override val titleRes: Int
        get() = R.string.ping_test

    override suspend fun exeTest(address:String): String? {
        return viewModel()?.sophosPing(address)
    }


    override fun supportIPv6()=true
}