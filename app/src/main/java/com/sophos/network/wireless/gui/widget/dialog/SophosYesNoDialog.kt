package com.sophos.network.wireless.gui.widget.dialog

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnShowListener
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.sophos.network.R
import com.sophos.network.databinding.SophosDialogYesNoBinding


class SophosYesNoDialog
    (
    private val con: Context,
   private val mTitle: String,
   private val mMSG: String,
    private val result:(dialog:Dialog,yes:Boolean)->Unit
) : Dialog(con, R.style.MyDialog), OnShowListener, View.OnClickListener {

    private val binding by viewBinding(SophosDialogYesNoBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // ConstantClass.printForDebug("OwnYesNoDialog " )
        window?.apply {
            attributes.windowAnimations = R.style.DialogAnimationScaletIn
            setBackgroundDrawableResource(android.R.color.transparent);
        }
        setCancelable(true)
        setCanceledOnTouchOutside(true)
        setContentView(binding.root)
        setOnShowListener(this)
    }

    override fun onShow(arg0: DialogInterface) {

        //mTitle?.apply {
        binding.yesnoTitle.text = mTitle
            /* }?:run{
                 findViewById<View>(R.id.yesno_title).visibility=View.GONE
             }*/

        binding.yesnoMsg.text = mMSG
        binding.cancel.setOnClickListener(this)
        binding.ok.setOnClickListener(this)
    }

    override fun onClick(v: View) {
       when(v.id){
           R.id.ok->{
               result.invoke(this,true)
               dismiss()
           }
           R.id.cancel->{
               result.invoke(this,false)
               dismiss()
           }
       }
    }
}