package com.sophos.network.wireless.gui.widget.dialog
import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.DialogInterface.OnShowListener
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView

import com.acelink.library.utils.data.ConstantClass
import androidx.recyclerview.widget.LinearLayoutManager

import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.android.viewer.utils.ViewUtils
import com.sophos.network.R
import com.sophos.network.wireless.gui.widget.dialog.adapter.ChooseAdapter
import com.sophos.network.databinding.SophosDialogSmallChooseBinding


class SophosSmallChooseDialog<T>
    (
    val myActivity: Activity,
    private val tiltle:String?,
    private val dataSet: MutableList<T>,
    private var pos: Int=0,
    val needIcon: Boolean=true,

    val result:(dialog:Dialog,choose:Int)->Unit

) : Dialog(myActivity, R.style.MyDialog), OnShowListener {

    private lateinit var chooseAdapter: ChooseAdapter<T>

    val bindingChoose by viewBinding(SophosDialogSmallChooseBinding::inflate)

    var isAdjustHeight:Boolean=false

    var adjustHeightPercent=60

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ConstantClass.printForDebug("XendChooseDialog " )
       // window?.attributes?.windowAnimations = R.style.DialogAnimationScaletIn
        setCancelable(true)
        setCanceledOnTouchOutside(true)
        setContentView(bindingChoose.root)
        setOnShowListener(this)

    }

    private fun setWindowHeight(percent: Int) {
        val params = window!!.attributes
        val h=(ViewUtils.getWindowHeight(myActivity) * percent / 100)
       // if( params.height>h)
            params.height = (ViewUtils.getWindowHeight(myActivity) * percent / 100)
        this.window!!.attributes = params
    }



    override fun onShow(arg0: DialogInterface) {
        //if(tiltle.isNullOrEmpty()) {
            bindingChoose.chooseTitleText.visibility=View.GONE
            /*}else{
                bindingChoose.chooseTitleText.visibility=View.VISIBLE
                bindingChoose.chooseTitleText.text=tiltle
            }*/

        val layoutManager = LinearLayoutManager(myActivity)
        var recyclerView= (findViewById<RecyclerView>(R.id.choose_dialog_recycle) )
        recyclerView.layoutManager=layoutManager

        chooseAdapter= ChooseAdapter(dataSet,pos,needIcon,false,false,{pos->
            result.invoke(this,pos)
        })

        recyclerView.adapter=chooseAdapter
        if(isAdjustHeight)setWindowHeight(adjustHeightPercent)

    }

}