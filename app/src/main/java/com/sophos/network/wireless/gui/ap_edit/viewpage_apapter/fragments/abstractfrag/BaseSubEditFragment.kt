package com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.abstractfrag

import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.text.method.TextKeyListener
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.APConfigFragment
import com.sophos.network.wireless.gui.ap_edit.APEditFragment
import com.sophos.network.wireless.gui.ap_edit.APEditViewModel
import com.sophos.network.wireless.pagetool.frags_page

open class BaseSubEditFragment : Fragment() {


    fun viewModel(): APEditViewModel?=getAPConfigFragment()?.viewModel

    fun getAPConfigFragment(): APConfigFragment?=((frags_page.ap_edit.pageFragment?:frags_page.ap_central.pageFragment) as? APConfigFragment)

    fun getMainActivity(): MainActivity?=activity as?MainActivity

    fun getCurrentEditDevice()=(activity as?MainActivity)?.getCurrentEditDevice()

    override fun onDestroyView() {
        super.onDestroyView()
    }



}