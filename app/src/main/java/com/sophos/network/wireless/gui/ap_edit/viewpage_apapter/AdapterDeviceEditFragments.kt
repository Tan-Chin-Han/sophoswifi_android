package com.sophos.network.wireless.gui.ap_edit.viewpage_apapter

import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.FragmentViewHolder
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.*


import java.lang.NullPointerException

class AdapterDeviceEditFragments(val fragment: Fragment) : FragmentStateAdapter(fragment)
{
    companion object{
        val edit_system_page=0
        val edit_lan_port_page=edit_system_page+1
        val edit_wireless_band_page=edit_lan_port_page+1
        val edit_clients_page=edit_wireless_band_page+1
        val edit_day_time_page=edit_clients_page+1
        val edit_ping_test_page=edit_day_time_page+1
        val edit_traceroute_test_page=edit_ping_test_page+1
    }


    val  deviceSystemFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {

        DeviceSystemFragment.getInstance()
    }
    val  deviceLanPortFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DeviceLanPortFragment.getInstance()
    }
    val  deviceWirelessBandsFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DeviceWirelessBandsFragment.getInstance()
    }

    val  deviceClientsFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DeviceClientsFragment.getInstance()
    }

    val  dayTimeFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DayTimeFragment.getInstance()
    }

    val  devicePingTestFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DevicePingTestFragment.getInstance()
    }

    val  deviceTracerouteTestFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DeviceTracerouteTestFragment.getInstance()
    }

    override fun createFragment(position: Int): Fragment
    {
        var f =when(position){
            edit_system_page->deviceSystemFragment
            edit_lan_port_page->deviceLanPortFragment
            edit_wireless_band_page->deviceWirelessBandsFragment
            edit_clients_page->deviceClientsFragment
            edit_day_time_page->dayTimeFragment
            edit_ping_test_page->devicePingTestFragment
            edit_traceroute_test_page->deviceTracerouteTestFragment
            else->throw NullPointerException()
        }
        return f

    }

    override fun onBindViewHolder(holder: FragmentViewHolder, position: Int, payloads: MutableList<Any>) {
        ConstantClass.printForDebug("AdapterDeviceEditFragments onBindViewHolder ${position} ")
        super.onBindViewHolder(holder, position, payloads)
    }

    override fun getItemCount(): Int
    {
        return edit_traceroute_test_page+1
    }



}