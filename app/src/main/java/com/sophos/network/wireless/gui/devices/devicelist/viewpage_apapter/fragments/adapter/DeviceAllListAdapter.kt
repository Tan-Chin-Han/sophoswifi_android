package com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter.fragments.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.acelink.library.pagetool.BaseBindingHolder
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.acelink.library.utils.EasyUtils.tryCatchBlock
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.pagetool.frags_page


import com.sophos.network.R
import com.sophos.network.databinding.ItemDeviceCardBinding
import com.sophos.network.wireless.psclass.devcies.BaseSophosDevice

open class DeviceAllListAdapter (private val frag: Fragment, private var allDevices: MutableList<BaseSophosDevice>?=null) :
    RecyclerView.Adapter<DeviceAllListAdapter.ViewHolder>() {

    internal var filterResult: MutableList<BaseSophosDevice>?=null

    init {
        filterResult=allDevices
    }



    private var filter:String?=null

   // @Synchronized
    fun updateTextFilter(filter:String){
       this.filter=filter
        if(filter==null||filter.length==0){
           /* frag.lanuchMain {

            }*/
            filterResult=allDevices
            notifyDataSetChanged()
        }else{
            var list=ArrayList<BaseSophosDevice>()!!
            Log.e("TextWatcher", "deviceAllListMyEditFilter afterTextChanged:\t$filter origanl list size=${filterResult!!.size}")
            for (sub in allDevices!!){
                var f1=sub.deviceName?.run {filter in this  }?:false
                var f2= sub.mac?.contains(filter,true)?:false
                if(f1)  Log.e("TextWatcher", "afterTextChanged:\t$filter f1 \t ${sub.deviceName}")
                if(f2)  Log.e("TextWatcher", "afterTextChanged:\t$filter f2 \t ${sub.mac}")
                if(f1||f2){
                    list.add(sub)
                }
            }

            filterResult=list
            notifyDataSetChanged()
            Log.e("TextWatcher", "deviceAllListMyEditFilter afterTextChanged:\t$filter size=${filterResult!!.size}")

            }
    }

    fun updateLists(  slaves: MutableList<BaseSophosDevice>?){
        slaves?.apply {
            allDevices=this

        }?:this.allDevices?.clear()

        updateTextFilter(filter?:"")
    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(viewGroup.viewBinding(ItemDeviceCardBinding::inflate).value)
    }

    override fun getItemCount() = filterResult!!.size

    override fun onBindViewHolder(viewHolder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

       // var value=filterResult!![position]
       /* value.apply {
            viewHolder.apply {
                binding.deviceListItemName.isSelected=true


                binding.deviceListItemName.setText(deviceName)

                binding.deviceListItemMac.setText(mac)
                binding.deviceListCardParent.setBackgroundResource(if (operatorMode==1)R.drawable.card_edge_central else R.drawable.card_edge)
                when(heath){
                    0->{

                        binding.deviceListItemHealthTxt.setText(R.string.signal_excellent)
                        binding.deviceListItemSignal.setBackgroundColor(ContextCompat.getColor(frag.requireContext(),R.color.sophos_signal_excellent))
                    }
                    1->{
                        binding.deviceListItemHealthTxt.setText(R.string.signal_good)
                        binding.deviceListItemSignal.setBackgroundColor(ContextCompat.getColor(frag.requireContext(),R.color.sophos_signal_good))
                    }
                    2->{
                        binding.deviceListItemHealthTxt.setText(R.string.signal_poor)
                        binding.deviceListItemSignal.setBackgroundColor(ContextCompat.getColor(frag.requireContext(),R.color.sophos_signal_poor))
                    }
                }

                binding.root.setOnClickListener {
                    if(operatorMode==1){
                        (frag.requireActivity() as MainActivity).changeMainPage(frags_page.ap_central)
                    }else{
                        (frag.requireActivity() as MainActivity).changeMainPage(frags_page.ap_edit)
                    }

                }

            }

        }*/
    }

    inner class ViewHolder(vb: ItemDeviceCardBinding) : BaseBindingHolder<ItemDeviceCardBinding>(vb) {
        var passwordError=false
    }



    override fun getItemViewType(position: Int): Int {
        return tryCatchBlock({
            return@tryCatchBlock allDevices?.get(position)?.mac?.hashCode()?:super.getItemViewType(position)?:0
        },super.getItemViewType(position))?:0

    }
}
