package com.sophos.network.wireless.gui.ap_edit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import com.acelink.library.gson.JsonHelper
import com.acelink.library.pagetool.CoroutineIO
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.acelink.library.search.UPnPSophosDiscovery
import com.acelink.library.utils.data.ConstantClass
import com.android.viewer.utils.TimerUtil
import com.sophos.network.R
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.DeviceLanPortFragment

import com.sophos.network.wireless.gui.central.APCentralFragment
import com.sophos.network.wireless.psclass.connection.SophosNetwork
import com.sophos.network.wireless.psclass.devcies.BaseSophosDevice
import com.sophos.network.wireless.psclass.enum.Authentication_Type
import com.sophos.network.wireless.psclass.enum.Radio
import com.sophos.network.wireless.psclass.gson.data.GetClients
import com.sophos.network.wireless.psclass.gson.data.GetLanPort
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class APEditViewModel : ViewModel() {
    lateinit var editFragment: APEditFragment
    lateinit var centralFragment: APCentralFragment

    fun fragment()=if(isEditInitial() )editFragment else centralFragment

    private fun isEditInitial()=this::editFragment.isInitialized

    private var timerSSDP = TimerUtil()

    private val reloadTimer=TimerUtil()

    fun onSelect(){

    }

    fun onDeSelect(){

    }

   suspend fun updateProductName(product:String):Int{
        editFragment.apply {

            getMainActivity()?.apply {
                if(getSystemData()?.data?.productName==product){
                    ConstantClass.printForDebug("productName no change")
                    return 2
                }
                onShowProgress(null)
                //kotlin.runCatching {
                var result = SophosNetwork.retryAsynBoolean {
                    runBlocking {
                        SophosNetwork.setSystemInfo(
                            getCurrentEditDevice()!!,
                            product
                        )
                    }
                }

                if (result.await()) {
                    var reload =
                        SophosNetwork.setReload(getCurrentEditDevice()!!)
                    ConstantClass.printForDebug("setSystemInfo Reload= ${reload} xxxxxxxxxxxxxxxxxxxx")
                    if (reload != null && reload > 0) {
                        viewModel.countReload(reload)
                        delay((reload * 1000).toLong())
                        viewModel.removeCountReload()
                    }

                }
                onHideProgress()
                if(result.await()){
                    getSystemData()?.data?.productName=product
                }
                return if (result.await()) 1 else 0
            }
            return 0
            //}.onFailure { it.printStackTrace() }
           // getMainActivity()?.onHideProgress()
            //return 0
        }
    }

    suspend fun getWirelessBandData(block:()->Unit){
        runBlocking { //please use IO thread
            fragment().getMainActivity()?.getCurrentEditDevice()?.let {base->
                SophosNetwork.getRadioBands(base)
            }
        }

    }

    fun getBands(block:()->Unit){
        lanuchIO {

            fragment().getMainActivity()?.getCurrentEditDevice()?.let {base->

                getWirelessBandData{}
                //get Security
                Radio.values().forEach {
                    it.valueBand?.data?.ssid_num_enable?.apply {
                        it.deferredSecuritys.clear()
                        it.valueSecuritys.clear()
                        for (i in 0.. this-1)
                        {
                            var defer=async {
                                SophosNetwork.getSecurity(base,it,i)
                            }
                            it.deferredSecuritys.put(i,defer)

                        }

                        for (i in 0.. this-1)
                        {
                            it.valueSecuritys.put(i, it.deferredSecuritys.get(i)?.await())
                        }
                        //retry fist time
                        for (i in 0.. this-1)
                        {
                            if (it.valueSecuritys.get(i)==null){
                                var defer=async {
                                    SophosNetwork.getSecurity(base,it,i)
                                }
                                ConstantClass.printForDebug("valueSecuritys retry 1 index${i}")
                                it.deferredSecuritys.put(i,defer)
                                it.valueSecuritys.put(i, it.deferredSecuritys.get(i)?.await())
                            }

                        }
                        //retry second time
                       /* for (i in 0.. this-1)
                        {
                            if (it.valueSecuritys.get(i)==null){
                                var defer=async {
                                    SophosNetwork.getSecurity(base,it,i)
                                }
                                ConstantClass.printForDebug("valueSecuritys retry 1 index${i}")
                                it.deferredSecuritys.put(i,defer)
                                it.valueSecuritys.put(i, it.deferredSecuritys.get(i)?.await())
                            }

                        }*/

                    }
                }
            }

            //arrange security data
            Radio.values().forEach{radio ->

                var count=radio.valueBand?.data?.clientList?.size?:0

                ConstantClass.printForDebug("${radio.bands()} radio_enable=${radio.valueBand?.data?.radio_enable} count=${count} ssid_num_enable=${radio.valueBand?.data?.ssid_num_enable?:"null"}")
                //  var sort=radio.valueBand?.data?.clientList?.sortBy { it.index }

                radio.valueBand?.data?.clientList?.apply {
                    if(radio == Radio.G6){
                        forEach {
                            it.auth= Radio.G6DefaultAuth
                        }
                    }
                    forEach { it->

                        it.index?.let {index->
                            var security=radio.valueSecuritys.get(index)
                            security?.let { security->
                                security.data?.let {securityData->
                                    it.pass=securityData.auth_detail?.psk_value
                                    it.auth= Authentication_Type.getAuth(securityData,radio)
                                }//?: kotlin.run { ConstantClass.printForDebug("${radio} index ${index} security data null") }
                            }//?: kotlin.run { ConstantClass.printForDebug("${radio} index ${index} security null") }
                        }//?: kotlin.run { ConstantClass.printForDebug("${radio} WirelessName ${it} index null") }
                    }
                }

            }
            block.invoke()
        }
    }

    fun getClients(block:()->Unit){
        lanuchIO {
            fragment().getMainActivity()?.apply {
                onShowProgress(null)
                /*  fragment().getMainActivity()?.getCurrentEditDevice()?.apply {
                Radio.values().forEach {
                    it.deferred=async {
                        SophosNetwork.getClients(this@apply,it)
                    }
                }
            }

            Radio.values().forEach{
                it.clientsValue=null
                it.clientsValue=it.deferred?.await()
            }*/
                //retry
                Radio.values().forEach{
                    //if(it.clientsValue==null){
                    it.clientsValue=null
                    it.deferred=SophosNetwork.retryAsyn<GetClients?> {
                        runBlocking {
                            SophosNetwork.getClients(getCurrentEditDevice()!!,it)
                        }
                    }
                    it.clientsValue=it.deferred?.await()
                    //  }
                }


                block.invoke()
                onHideProgress()
            }


        }
    }

    var lanPortData: GetLanPort?=null
    suspend fun getLanPort():Boolean
    {
        fragment().getMainActivity()?.apply {
            onShowProgress(null)
            var lanPort= SophosNetwork.retryAsyn {
                runBlocking {
                    SophosNetwork.getLanPorts(getCurrentEditDevice()!!)
                }
            }?.await()

            ConstantClass.printForDebug("getLanPort lanPort=${lanPort}")
            lanPort?.let {
                lanPortData=it
                return true
            }
        }

        return false

    }

    suspend fun setLanPorts(new: GetLanPort.LanPorData,lanportFragment: DeviceLanPortFragment):Int{
        if(!lanPortData?.data!!.equalData( new!!)){
            var file= JsonHelper.createJson(new)
            ConstantClass.printForDebug("set LANPORT 2"+ file)

            var result= SophosNetwork.retryAsynBoolean {
                runBlocking {
                    SophosNetwork.setLanPorts(lanportFragment.getMainActivity()?.getCurrentEditDevice()!!,file)
                    //  SophosNetwork.setLanPortsJSON(getMainActivity()?.getCurrentEditDevice()!!, JsonHelper.createJson(finalData?.ipv4!!))
                }

            }
            return if(result.await()) APEditFragment.updateValueResultSuccess else APEditFragment.updateValueResultFullFail
        }
        return APEditFragment.updateValueResultNoChange
    }


    fun timerRefoundCurrentDevice(currentDevice: BaseSophosDevice,startTime:Long,block: (result:Boolean) -> Unit){
        if(System.currentTimeMillis()-startTime> 120000){
            startTime+60000
        }
        editFragment.apply {
            timerSSDP.startTimer({

                ConstantClass.printForDebug("timerRefoundCurrentDevice  SSDP Search")


                editFragment.getMainViewModel()?.discoveryDevices(requireContext()) {

                    it.forEach { device ->
                        if (device.mac == currentDevice.mac) {
                            timerSSDP.removeMyTimer()
                            lifecycleScope.launch(CoroutineIO.contextMain) {
                                block.invoke(true)

                            }
                            return@forEach
                        }
                    }
                    ConstantClass.printForDebug("reboot ssdp time=${(System.currentTimeMillis() - startTime) / 1000}")
                    if (System.currentTimeMillis() - startTime > 120000) {
                        timerSSDP.removeMyTimer()
                        lifecycleScope.launch(CoroutineIO.contextMain) {
                            block.invoke(false)
                        }
                    }
                }
            },0,(UPnPSophosDiscovery.ssdpDiscoveryInterval -2000).toLong())
        }

    }

    suspend fun TraceRoute(address:String):String?{
        editFragment.apply {
            var result: String? = null
            getMainActivity()?.apply {
                onShowProgress(null)
                result = SophosNetwork.traceRoute(getCurrentEditDevice()!!, address)
                onHideProgress()

            }
            return result
        }
    }

    suspend fun sophosPing(address:String):String?{
        editFragment.apply {
            getMainActivity()!!.apply {
               onShowProgress(null)
                var result=  SophosNetwork.sophosPing(getCurrentEditDevice()!!,address)
                onHideProgress()
                return result
            }
            return null
        }
    }

    fun countReload(reload:Int){
        var timer=reload
        reloadTimer.startTimer({
            timer--
            if(timer>=0){
                lanuchMain {
                    editFragment!!.getMainActivity()?.apply {
                        updateProgress("${timer} ${getString(R.string.seconds_left)}")
                    }
                }
            }else{
                reloadTimer.removeMyTimer()
            }

        },0,1000)
    }

    fun countReboot(reload:Int,block: (timer:Int) -> Unit){
        var timer=reload
        reloadTimer.startTimer({
            timer--
            if(timer>=0){
                lanuchMain {
                    block.invoke(timer)
                }
            }else{
                reloadTimer.removeMyTimer()
            }

        },0,1000)
    }

    fun removeCountReload()= reloadTimer.removeMyTimer()

}