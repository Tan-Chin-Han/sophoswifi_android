package com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.abstractfrag

import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.text.method.TextKeyListener
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat

import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.acelink.library.utils.data.ConstantClass
import com.android.viewer.utils.TimerUtil
import com.android.viewer.utils.ViewUtils
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.conn.util.InetAddressUtils.isIPv4Address
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.conn.util.InetAddressUtils.isIPv6Address
import com.sophos.network.R
import com.sophos.network.databinding.FragApEditPingTestBinding
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.APConfigFragment
import com.sophos.network.wireless.gui.ap_edit.APEditFragment
import com.sophos.network.wireless.gui.ap_edit.APEditViewModel
import com.sophos.network.wireless.pagetool.frags_page

open abstract class DeviceAbstractTestFragment : BaseSubEditFragment() {

    //fun getMainActivity(): MainActivity?=getAPConfigFragment()?.getMainActivity()

    private val binding by viewBinding(FragApEditPingTestBinding::inflate)

    abstract val titleRes:Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug("onCreateView ${javaClass.simpleName} ")
        binding.apEditPingTestTitle.setText(titleRes)
        binding.apEditPingTestDestAddress.setOnFocusChangeListener { v, hasFocus ->
            if(hasFocus){
                requireContext().getDrawable(R.drawable.background_focus_textbox_5dp).apply {
                    binding.apEditPingTestDestLay.background=this
                }
            }else{
                requireContext().getDrawable(R.drawable.background_default_textbox_5dp).apply {
                    binding.apEditPingTestDestLay.background=this
                }
            }
        }

        binding.apEditPingTestDestAddress.apply {
            addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
                override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                  /*  if (!getText().toString().equals("")) {
                        if (getText().toString()
                                .toInt() > 255
                        ) setText("255") else if (getText().toString().toInt() < 0) {
                            setText("0")
                        }
                    }
                    setSelection(length())*/
                }

                override fun afterTextChanged(editable: Editable) {
                        val s=text.toString()
                        //if(!s.isNullOrEmpty()){
                           if(isIPv4Address(s)||(supportIPv6()&&isIPv6Address(s))){
                               binding.apEditPingTestExeFrame.setBackgroundResource(R.drawable.background_secondary_enable)
                               binding.apEditPingTestExe.setTextColor( ContextCompat.getColor(requireContext(),R.color.sophos_secondary_enable_color))
                               binding.apEditPingTestExe.setOnClickListener {
                                   lanuchIO {
                                      var res= exeTest(binding.apEditPingTestDestAddress.text.toString())
                                       ConstantClass.printForDebug("test res=${res}")
                                       lanuchMain {
                                           binding.apEditPingResultTxt.setText(res)
                                       }

                                   }


                               }
                               return
                           }
                        //}
                        binding.apEditPingTestExeFrame.setBackgroundResource(R.drawable.background_secondary_diable)
                        binding.apEditPingTestExe.setTextColor( ContextCompat.getColor(requireContext(),R.color.sophos_secondary_disable_color))
                    binding.apEditPingTestExe.setOnClickListener(null)
                }
            })
        }
        if(supportIPv6()){

            binding.apEditPingTestDestAddress.inputType=InputType.TYPE_CLASS_TEXT
           /* binding.apEditPingTestDestAddress.filters = arrayOf<InputFilter>(
                EasyInputFilter.Builder().setAcceptorType(CharacterType.CUSTOM)
                .setCustomAcceptedCharacters("0123456789.:abcdefABCDEF/")
                .build()
            )*/

        }else{
            binding.apEditPingTestDestAddress.inputType=InputType.TYPE_CLASS_NUMBER
            binding.apEditPingTestDestAddress.setKeyListener(DigitsKeyListener.getInstance("0123456789."));
        }
        return binding.root
    }


    override fun onDestroyView() {
        super.onDestroyView()
    }

    abstract fun supportIPv6():Boolean

    abstract suspend fun exeTest(address:String):String?


}