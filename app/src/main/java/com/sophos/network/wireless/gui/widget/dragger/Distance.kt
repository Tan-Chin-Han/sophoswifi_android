package com.sophos.network.wireless.gui.widget.dragger

internal class Distance(val startPoint: Int, val dragMin: Int, val dragMax: Int) {
    val min: Int
    val max: Int
    override fun toString(): String {
        return "Min : $min, Max : $max dragMin: $dragMin dragMax: $dragMax"
    }

    init {
        min = startPoint - dragMin
        max = startPoint + dragMax
    }
}