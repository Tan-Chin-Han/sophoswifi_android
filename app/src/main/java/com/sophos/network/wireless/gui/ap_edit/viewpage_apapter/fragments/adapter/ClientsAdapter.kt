package com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.adapter

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.annotation.UiThread
import androidx.recyclerview.widget.RecyclerView
import com.acelink.library.pagetool.BaseBindingHolder
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.acelink.library.utils.WiFiUtils

import com.sophos.network.wireless.gui.widget.dragger.ViewDragLayout
import com.sophos.network.wireless.psclass.gson.data.ClientBand
import com.sophos.network.R
import com.sophos.network.databinding.ItemClientBinding
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.DeviceClientsFragment
import com.sophos.network.wireless.psclass.connection.SophosNetwork
import kotlinx.coroutines.runBlocking


class ClientsAdapter (private val fragment: DeviceClientsFragment, private val dataSet: MutableList<ClientBand>) :
    RecyclerView.Adapter<ClientsAdapter.ViewHolder>() {

    var dragable=true

    //private lateinit var recyclerView: RecyclerView
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
      //  this.recyclerView=recyclerView
        super.onAttachedToRecyclerView(recyclerView)
    }

    inner class ViewHolder(val vb: ItemClientBinding) : BaseBindingHolder<ItemClientBinding>(vb) {
            fun bindItem(value:ClientBand,position:Int) {
                vb.apClientDeviceName.text = value.vendor.run {
                    if(this.isNullOrEmpty()||this.isBlank())fragment.getString(R.string.unknown) else this
                }
                vb.apClientDeviceBand.text = value.radio?.bands()
                vb.apClientDeviceIp.text = value.ip
                vb.apClientDeviceMac.text = value.mac
                vb.apClientTx.text = "Tx:${value.tx}"
                vb.apClientRx.text = "Rx:${value.rx}"
                vb.apClientSignal.text = "Singal(%): ${value.signal.toString()}"
                vb.apClientRssi.text = "RSSI(dbm): ${value.rssi.toString()}"
                vb.apClientConnectTime.text =
                    "Connected Time: ${value.connectedTime.toString()}"
                vb.apClientIdleTime.text = "idle Time: ${value.idle.toString()}"
                if (position == itemCount - 1) {
                    //(vb.root.getLayoutParams() as (ViewGroup.MarginLayoutParams)).bottomMargin = 0
                }
                var phone=WiFiUtils.getPhoneWifiIP(fragment.requireContext())


                val dragX = fragment.getResources().getDimension(R.dimen.space80dp).toInt()
                if(dragable){
                    ViewDragLayout.Builder(bindingHolder.clientHoverVdl)
                        .setLayoutType(ViewDragLayout.HOVER_LINEAR_HORIZONTAL)
                        .setSpecificDragDirectionFlag(
                            R.id.client_parent_view,
                            ViewDragLayout.LEFT or ViewDragLayout.RIGHT
                        )
                        .setDragX(dragX, 0)
                        .asChain() //.hookWith(R.id.gw_list_remove_view)
                        .create()
                    var recover = object : ViewDragLayout.onNameClickLisener {
                        override fun onClick() {
                            bindingHolder.clientHoverVdl.resetSpecificView(R.id.client_parent_view)
                        }
                    }
                    bindingHolder.clientHoverVdl.setOnNameClickLisener(recover)
                    if(phone==value.ip)
                    {
                        bindingHolder.clientRemoveView.alpha=0.3f
                        bindingHolder.clientRemoveView.setOnClickListener(null)
                    }else{
                        bindingHolder.clientRemoveView.alpha=1f
                        bindingHolder.clientRemoveView.setOnClickListener { v ->
                            fragment.apply {
                                lanuchIO {
                                    getMainActivity()?.apply {
                                        onShowProgress(null)
                                        var result=SophosNetwork.retryAsynBoolean {
                                            runBlocking {
                                                SophosNetwork.kicks(getCurrentEditDevice()!!,value.radio!!,value.ssid_index!!,value.mac!!)
                                            }
                                        }
                                        if(result.await()){
                                            lanuchMain {

                                                bindingHolder.clientHoverVdl.resetSpecificView(R.id.client_parent_view)
                                                dataSet.removeAt(position)
                                                notifyDataSetChanged()
                                            }
                                        }
                                        onHideProgress()
                                    }

                                }
                            }
                        }
                    }

                }

            }
    }

    override fun getItemViewType(position: Int): Int {
        return dataSet.get(position).mac.hashCode()
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(viewGroup.viewBinding(ItemClientBinding::inflate).value)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        viewHolder.itemView.setOnClickListener {

        }
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.bindItem(dataSet[position],position)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

  /*  @Synchronized
    @UiThread
    fun updateDataSet(sets: List<ClientBand>)
    {

        dataSet.clear()
        dataSet.addAll(sets)
        notifyDataSetChanged()
    }*/

}
