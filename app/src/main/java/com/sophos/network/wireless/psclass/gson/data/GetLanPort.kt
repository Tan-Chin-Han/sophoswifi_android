package com.sophos.network.wireless.psclass.gson.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class GetLanPort {
    companion object{
        val ipv4IPTypeStatic=0
        val ipv4IPTypeDHCP=1
        val ipv4IPTypeUserDefine=0
    }

    @Expose
    @SerializedName("error_code")
    val errorCode = -1

    @Expose
    @SerializedName("error_msg")
    val errorMsg:String?=null

    @Expose
    @SerializedName("data")
    val data:LanPorData ?= null


    override fun toString(): String {
        return "GetLanPort(errorCode=$errorCode, error_msg=$errorMsg, LanPorData=$data"
    }

    class LanPorData{
        @Expose
        @SerializedName("ipv4")
        var ipv4:IPvData ?= null

        @Expose
        @SerializedName("ipv6")
        var ipv6:IPv6Data ?= null

        @Expose
        @SerializedName("lanport")
        var lanport:Array<LanPortData> ?= null

        fun copy()=LanPorData().also {
            it.ipv4=ipv4?.copy()
            it.ipv6=ipv6
            it.lanport=lanport
        }

        fun equalData(new: LanPorData):Boolean
        {
            ipv4?.run {
                new?.ipv4?.also { //it is new
                    if(it.ipv4Type==ipv4IPTypeDHCP){
                        return it.ipv4Type==ipv4Type&&ipv4_default_gateway_type==ipv4_default_gateway_type
                                &&it.ipv4_default_gateway_type.run { if(this==ipv4IPTypeDHCP)  true else it.defaultGateway==defaultGateway}
                                &&it.ipv4_dns_primary_type==ipv4_dns_primary_type
                                &&it.ipv4_dns_primary_type.run { if(this==ipv4IPTypeDHCP)  true else it.ipv4DnsPrimary==ipv4DnsPrimary}
                                &&it.ipv4_dns_secondary_type==ipv4_dns_secondary_type
                                &&it.ipv4_dns_secondary_type.run { if(this==ipv4IPTypeDHCP)  true else it.ipv4DnsSecondary==ipv4DnsSecondary}
                    }
                    if(it.ipv4Type==ipv4IPTypeStatic){
                        return it.ipv4Type==ipv4Type&&it.ipv4Address==ipv4Address
                                &&it.subnetMask==subnetMask&&it.defaultGateway==defaultGateway
                                &&it.ipv4DnsPrimary==ipv4DnsPrimary &&it.ipv4DnsSecondary==ipv4DnsSecondary
                                &&it.dhcpEnabled==dhcpEnabled&&it.dhcpEnabled.run {
                                    if(this==1){
                                        it.dhcpStartAddr==dhcpStartAddr&& it.dhcpEndAddr==dhcpEndAddr
                                        && it.dhcpDomainName==dhcpDomainName&& it.dhcpLeaseTime==dhcpLeaseTime
                                        && it.dhcpDnsPrimary==dhcpDnsPrimary && it.dhcpDnsSecondary==dhcpDnsSecondary
                                    }else true
                                }
                    }
                }?:return true
            }?:return true

            return true
        }

        fun createLanPortJson(newPara: LanPorData):LanPorData{
            var data=copy()
            data.ipv4!!.run {
                newPara?.ipv4!!.also { //it is new
                    ipv4Type=it.ipv4Type
                    if(it.ipv4Type==ipv4IPTypeDHCP)
                    {
                        ipv4_default_gateway_type=it.ipv4_default_gateway_type
                        if(ipv4_default_gateway_type==ipv4IPTypeUserDefine){
                            defaultGateway  =it.defaultGateway
                        }
                        ipv4_dns_primary_type=it.ipv4_dns_primary_type
                         if(ipv4_dns_primary_type==ipv4IPTypeUserDefine){
                             ipv4DnsPrimary  =it.ipv4DnsPrimary
                         }
                        ipv4_dns_secondary_type=it.ipv4_dns_secondary_type
                        if(ipv4_dns_secondary_type==ipv4IPTypeUserDefine){
                            ipv4DnsSecondary  =it.ipv4DnsSecondary
                        }
                    }else if(it.ipv4Type==ipv4IPTypeStatic)
                    {
                        ipv4Address=it.ipv4Address
                          subnetMask=it.subnetMask
                          defaultGateway=it.defaultGateway
                         ipv4_default_gateway_type=0
                            ipv4_dns_primary_type=0
                            ipv4_dns_secondary_type=0
                          ipv4DnsPrimary=it.ipv4DnsPrimary
                           ipv4DnsSecondary=it.ipv4DnsSecondary
                          dhcpEnabled=it.dhcpEnabled
                            if(dhcpEnabled==1){
                                dhcpStartAddr=it.dhcpStartAddr
                                dhcpEndAddr=it.dhcpEndAddr
                                 dhcpDomainName=it.dhcpDomainName
                                dhcpLeaseTime=it.dhcpLeaseTime
                                 dhcpDnsPrimary=it.dhcpDnsPrimary
                                dhcpDnsSecondary=it.dhcpDnsSecondary
                            }
                    }
                }
            }
            return data!!
        }

        override fun toString(): String {
            return "LanPorData(ipv4=$ipv4, ipv6=$ipv6, lanport=${lanport?.contentToString()})"
        }


    }

    class IPvData{
        @Expose
        @SerializedName("ipv4_type")
        var ipv4Type:Int ?= null
        @Expose
        @SerializedName("ipv4_address")
        var ipv4Address:String ?= null
        @Expose
        @SerializedName("ipv4_subnet_mask")
        var subnetMask:String ?= null
        @Expose
        @SerializedName("ipv4_default_gateway_type")
        var ipv4_default_gateway_type:Int ?= null
        @Expose
        @SerializedName("ipv4_default_gateway")
        var defaultGateway:String ?= null
        @Expose
        @SerializedName("ipv4_dns_primary_type")
        var ipv4_dns_primary_type:Int ?= null
        @Expose
        @SerializedName("ipv4_dns_primary")
        var ipv4DnsPrimary:String ?= null
        @Expose
        @SerializedName("ipv4_dns_secondary_type")
        var ipv4_dns_secondary_type:Int ?= null
        @Expose
        @SerializedName("ipv4_dns_secondary")
        var ipv4DnsSecondary:String ?= null
        @Expose
        @SerializedName("dhcp_enabled")
        var dhcpEnabled:Int ?= null
        @Expose
        @SerializedName("dhcp_start_addr")
        var dhcpStartAddr:String ?= null
        @Expose
        @SerializedName("dhcp_end_addr")
        var dhcpEndAddr:String ?= null
        @Expose
        @SerializedName("dhcp_domain_name")
        var dhcpDomainName:String ?= null
        @Expose
        @SerializedName("dhcp_lease_time")
        var dhcpLeaseTime:Int ?= null
        @Expose
        @SerializedName("dhcp_dns_primary")
        var dhcpDnsPrimary:String ?= null
        @Expose
        @SerializedName("dhcp_dns_secondary")
        var dhcpDnsSecondary:String ?= null

        fun copy()=IPvData().also {
            it.ipv4Type=ipv4Type
            it.ipv4Address=ipv4Address
            it.subnetMask=subnetMask
            it.ipv4_default_gateway_type=ipv4_default_gateway_type
            it.defaultGateway=defaultGateway
            it.ipv4_dns_primary_type=ipv4_dns_primary_type
            it.ipv4DnsPrimary=ipv4DnsPrimary
            it.ipv4_dns_secondary_type=ipv4_dns_secondary_type
            it.ipv4DnsSecondary=ipv4DnsSecondary
            it.dhcpEnabled=dhcpEnabled
            it.dhcpStartAddr=dhcpStartAddr
            it.dhcpEndAddr=dhcpEndAddr
            it.dhcpDomainName=dhcpDomainName
            it.dhcpLeaseTime=dhcpLeaseTime
            it.dhcpDnsPrimary=dhcpDnsPrimary
            it.dhcpDnsSecondary=dhcpDnsSecondary
        }

        override fun toString(): String {
            return "IPvData(ipv4Type=$ipv4Type, ipv4Address=$ipv4Address, subnetMask=$subnetMask, ipv4_default_gateway_type=$ipv4_default_gateway_type, defaultGateway=$defaultGateway, ipv4_dns_primary_type=$ipv4_dns_primary_type, ipv4DnsPrimary=$ipv4DnsPrimary, ipv4_dns_secondary_type=$ipv4_dns_secondary_type, ipv4DnsSecondary=$ipv4DnsSecondary, dhcpEnabled=$dhcpEnabled, dhcpStartAddr=$dhcpStartAddr, dhcpEndAddr=$dhcpEndAddr, dhcpDomainName=$dhcpDomainName, dhcpLeaseTime=$dhcpLeaseTime, dhcpDnsPrimary=$dhcpDnsPrimary, dhcpDnsSecondary=$dhcpDnsSecondary)"
        }


    }
    class IPv6Data {
        @Expose
        @SerializedName("ipv6_enabled")
        val ipv6_enabled: Int? = null

        @Expose
        @SerializedName("ipv6_type")
        val ipv6_type: Int? = null

        @Expose
        @SerializedName("ipv6_address")
        val ipv6_address: String? = null

        @Expose
        @SerializedName("ipv6_predix_len")
        val ipv6_predix_len: Int? = null

        @Expose
        @SerializedName("ipv6_gateway")
        val ipv6_gateway: String? = null
        override fun toString(): String {
            return "IPv6Data(ipv6_enabled=$ipv6_enabled, ipv6_type=$ipv6_type, ipv6_address=$ipv6_address, ipv6_predix_len=$ipv6_predix_len, ipv6_gateway=$ipv6_gateway)"
        }


    }

    class LanPortData {
        @Expose
        @SerializedName("index")
        val index: Int? = null

        @Expose
        @SerializedName("duplex")
        val duplex: Int? = null

        @Expose
        @SerializedName("flow_control")
        val flow_control: Int? = null

        @Expose
        @SerializedName("az_enabled")
        val az_enabled: Int? = null
        override fun toString(): String {
            return "LanPortData(index=$index, duplex=$duplex, flow_control=$flow_control, az_enabled=$az_enabled)"
        }

    }
    /*
   ,
  "lanport": [
    {
      "index": 0,
      "duplex": 0,
      "flow_control": 0,
      "az_enabled": 0
    }

     */

}