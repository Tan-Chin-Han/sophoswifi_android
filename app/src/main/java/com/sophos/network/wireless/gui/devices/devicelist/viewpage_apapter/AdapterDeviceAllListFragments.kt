package com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter

import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.FragmentViewHolder
import com.sophos.network.wireless.gui.devices.devicelist.DeviceAllListFragment
import java.lang.NullPointerException

class AdapterDeviceAllListFragments(val fragment: DeviceAllListFragment) : FragmentStateAdapter(fragment)
{

    val  deviceMyAllListFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DeviceMyAllListFragment.getInstance()
    }
    val  deviceNewAllListFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DeviceNewAllListFragment.getInstance()
    }
    override fun createFragment(position: Int): Fragment
    {
        var f =when(position){
            0->deviceMyAllListFragment
            1->deviceNewAllListFragment
            else->throw NullPointerException()
        }

        return f

    }

    override fun onBindViewHolder(holder: FragmentViewHolder, position: Int, payloads: MutableList<Any>) {
        //ConstantClass.printForDebug("${javaClass.simpleName} onBindViewHolder ${position} ")
        super.onBindViewHolder(holder, position, payloads)
    }

    override fun getItemCount(): Int
    {
        return 2
    }


    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
      //  ConstantClass.printForDebug("onAttachedToRecyclerView ${javaClass.simpleName} ")
        recyclerView.apply {


            addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val firstVisiblePosition: Int =( recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()

                }

            })
        }
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
            recyclerView.clearOnScrollListeners()
        super.onDetachedFromRecyclerView(recyclerView)
    }
}