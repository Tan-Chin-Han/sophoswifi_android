package com.sophos.network.wireless.gui.widget.textview

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import android.util.TypedValue
import android.widget.TextView.BufferType
import com.sophos.network.R
import java.util.regex.Pattern

class SophosSans_Bold: AppCompatTextView {

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

   /* constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context!!, attrs, defStyleAttr
    ) {
    }*/

    private fun init(context: Context) {
        if (!isInEditMode) {
            // Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/sophossans_bold.otf");
            super.setTypeface(ResourcesCompat.getFont(context, R.font.sophossans_bold))
        }
    }

   /* private fun refitText(text: String?) {
        val width = width
        if (text == null || text.isEmpty() || width <= 0) {
            return
        }
        val strList = text.split("\n").toTypedArray()
        var maxLength = text.length
        var maxString = text
        if (strList.size > 1) {
            maxLength = Int.MIN_VALUE
            for (str in strList) {
                val length = str.length
                if (maxLength < length) {
                    maxLength = length
                    maxString = str
                }
            }
        }
        val availableTextViewWidth = width - paddingLeft - paddingRight
        val charsWidthArr = FloatArray(maxLength)
        val boundsRect = Rect()
        paint.getTextBounds(maxString, 0, maxLength, boundsRect)
        var textWidth = boundsRect.width()
        var textSize = textSize
        while (textWidth > availableTextViewWidth) {
            textSize -= 1f
            paint.textSize = textSize
            paint.getTextBounds(maxString, 0, maxLength, boundsRect)
            textWidth = boundsRect.width()
        }
        this.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
    }*/

    /*    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
        if(text != null)
            refitText(text.toString());
    }*/
   /* fun setReFitText(text: String?) {
        super.setText(text)
        if (text != null) refitText(text.toString())
    }

    fun setReFitText(text: String?, type: BufferType?) {
        super.setText(text, type)
        if (text != null) refitText(text.toString())
    }

    companion object {
        fun findStr(srcText: String?, keyword: String?): Int {
            var count = 0
            val p = Pattern.compile(keyword)
            val m = p.matcher(srcText)
            while (m.find()) {
                count++
            }
            return count
        }
    }*/
}