package com.sophos.network.wireless.gui

import android.os.Bundle
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.wireless.pagetool.ViewModelTool.viewModels
import com.sophos.network.wireless.gui.ap_edit.APEditViewModel

abstract class APConfigFragment : BaseFragment() {
    companion object{
        /*fun getInstance(): APConfigFragment {
            return  APConfigFragment()
        }
       var mTag = "APConfigFragment"
        */
    }

    val viewModel: APEditViewModel by viewModels()

    //fun fragment()=viewModel.fragment()

    var support6G=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ConstantClass.printForDebug("APConfigFragment onCreate")
    }

    abstract fun updateDiscoveryEnd()

}