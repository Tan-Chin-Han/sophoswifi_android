package com.sophos.network.wireless.gui.widget.dialog


import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnShowListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.sophos.network.R
import com.sophos.network.databinding.SophosDialogMsgBinding


class SophosMessageDialog
    (
   private val cont: Context,
   private val mTitle: String?,
   private val mMSG: String,
   private val result:(dialog:Dialog)->Unit
) : Dialog(cont, R.style.MyDialog), OnShowListener, View.OnClickListener{

    private val binding by viewBinding(SophosDialogMsgBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // ConstantClass.printForDebug("OwnYesNoDialog " )
        val factory = LayoutInflater.from(cont)
        window?.apply {
            attributes.windowAnimations = R.style.DialogAnimationScaletIn
            setBackgroundDrawableResource(android.R.color.transparent);
        }
        setCancelable(true)
        setCanceledOnTouchOutside(true)
        setContentView(binding.root)
        setOnShowListener(this)

    }

    override fun onShow(arg0: DialogInterface) {
        mTitle?.apply {
            binding.yesnoTitle.text = this
        }/*?:run{
            binding.yesnoTitle.visibility=View.GONE
        }*/

        binding.yesnoMsg.text = mMSG
        binding.ok.setOnClickListener(this)
    }

    override fun onClick(v: View) {
       when(v.id){
           R.id.ok->{
               result.invoke(this)
               dismiss()
           }
       }
    }


}