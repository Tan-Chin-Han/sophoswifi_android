package com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.android.viewer.utils.TimerUtil
import com.acelink.library.utils.data.ConstantClass

import androidx.recyclerview.widget.LinearLayoutManager
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.ap_edit.APEditFragment
import com.sophos.network.wireless.gui.ap_edit.APEditViewModel
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.adapter.ClientsAdapter

import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.psclass.gson.data.ClientBand
import com.sophos.network.databinding.FragApEditClientsBinding
import com.sophos.network.wireless.gui.APConfigFragment
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.abstractfrag.BaseSubEditFragment
import com.sophos.network.wireless.psclass.enum.Radio


class DeviceClientsFragment : BaseSubEditFragment() {
    companion object{
        fun getInstance(): DeviceClientsFragment {
            return  DeviceClientsFragment()
        }
       var mTag = "DeviceClientsFragment"
    }


    val binding by viewBinding(FragApEditClientsBinding::inflate)


    var dragable=true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug("onCreateView ${javaClass.simpleName} ")

        binding.apEditClientsRecycleview.layoutManager=object: LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        };
        /*binding.apEditClientsRecycleview.adapter= ClientsAdapter(this, MutableList<ClientBand>(0,{it->ClientBand()})).also {
            it.dragable=dragable
        }*/


        binding.apEditClientsReflesh.setOnClickListener {
            getClients()
        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }


    fun onSelect(){
        getClients()
    }


    fun  getClients(){
        ConstantClass.printForDebug("getClients")
        viewModel()!!.getClients {
            var clients= Radio.getAllClients()
            lanuchMain {

                    binding.apEditClientsRecycleview.adapter=ClientsAdapter(this@DeviceClientsFragment,clients.toMutableList()).also {
                        it.dragable=dragable
                    }
                //getClientsAdapter().updateDataSet(clients as List<ClientBand>)

            }
        }
    }


}