package com.sophos.network.wireless.psclass.gson.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.sophos.network.wireless.psclass.enum.Authentication_Type
import java.util.*

class GetSecurity {
    @Expose
    @SerializedName("error_code")
    val errorCode = -1

    @Expose
    @SerializedName("error_msg")
    val errorMsg:String?=null

    @Expose
    @SerializedName("data")
    val data:SecurityData ?= null


    class SecurityData{
        companion object{
            fun getDefaultSecurityData(index:Int):SecurityData=SecurityData().apply {
                broadcast_ssid=1
                ssid_index=index
                client_isolation=0
                ieee80211k=0
                ieee80211w=0
                load_balance=100
                auth_method=0
                additional_auth=0
                mac_radius_auth=0
                mac_radius_password=""
            }
        }
        @Expose
        @SerializedName("ssid_index")
        var ssid_index:Int ?= null
        @Expose
        @SerializedName("broadcast_ssid")
        var broadcast_ssid:Int ?= null
        @Expose
        @SerializedName("client_isolation")
        var client_isolation:Int ?= null
        @Expose
        @SerializedName("ieee80211k")
        var ieee80211k:Int ?= null
        @Expose
        @SerializedName("ieee80211w")
        var ieee80211w:Int ?= null
        @Expose
        @SerializedName("load_balance")
        var load_balance:Int ?= null
        @Expose
        @SerializedName("auth_method")
        var auth_method:Int ?= null
        @Expose
        @SerializedName("additional_auth")
        var additional_auth:Int ?= null
        @Expose
        @SerializedName("mac_radius_auth")
        var mac_radius_auth:Int ?= null
        @Expose
        @SerializedName("mac_radius_password")
        var mac_radius_password:String ?= null

        @Expose
        @SerializedName("auth_detail")
        var auth_detail:AuthDetail ?= null
        override fun toString(): String {
            return "SecurityData(ssid_index=$ssid_index, broadcast_ssid=$broadcast_ssid, client_isolation=$client_isolation, ieee80211k=$ieee80211k, ieee80211w=$ieee80211w, load_balance=$load_balance, auth_method=$auth_method, additional_auth=$additional_auth, mac_radius_auth=$mac_radius_auth, mac_radius_password=$mac_radius_password, auth_detail=$auth_detail)"
        }


    }



    class AuthDetail {
        companion object{
            fun setAuthDetailData(data:SecurityData,type: Authentication_Type):AuthDetail=AuthDetail().apply {
                ieee80211r=0
                wpa_rekey_time=60
                psk_type=0

                data.auth_detail=this
                Authentication_Type.setAuth(data,type)

            }
        }

        @Expose
        @SerializedName("ieee80211r")
        var ieee80211r:Int ?= null
        @Expose
        @SerializedName("wpa_type")
        var wpa_type:Int ?= null
        @Expose
        @SerializedName("encrypt_type")
        var encrypt_type:Int ?= null
        @Expose
        @SerializedName("wpa_rekey_time")
        var wpa_rekey_time:Int ?= null
        @Expose
        @SerializedName("psk_type")
        var psk_type:Int ?= null
        @Expose
        @SerializedName("psk_value")
        var psk_value:String ?= null
        override fun toString(): String {
            return "AuthDetail(ieee80211r=$ieee80211r, wpa_type=$wpa_type, encrypt_type=$encrypt_type, wpa_rekey_time=$wpa_rekey_time, psk_type=$psk_type, psk_value=$psk_value)"
        }


    }

    override fun toString(): String {
        return "GetSecurity(errorCode=$errorCode, errorMsg=$errorMsg, data=$data)"
    }

}