package com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.acelink.library.utils.EasyUtils.tryCatchBlock
import com.acelink.library.utils.data.ConstantClass
import com.android.viewer.utils.TimerUtil
import com.sophos.network.R
import com.sophos.network.databinding.FragApEditSystemBinding
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.APConfigFragment
import com.sophos.network.wireless.gui.ap_edit.APEditFragment
import com.sophos.network.wireless.gui.ap_edit.APEditViewModel
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.abstractfrag.BaseSubEditFragment
import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.psclass.SophosEditableStype
import com.sophos.network.wireless.psclass.gson.data.GetSystem
import java.text.SimpleDateFormat
import java.util.*

enum class SystemPageMode{
    Normal,Edit,Central
}

class DeviceSystemFragment : BaseSubEditFragment() {
    companion object{
        fun getInstance(): DeviceSystemFragment {
            return  DeviceSystemFragment()
        }
       val mTag = "DeviceSystemFragment"

    }

    private val binding by viewBinding(FragApEditSystemBinding::inflate)

    private val timer=TimerUtil()

    var systemPageMode=SystemPageMode.Normal

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug("onCreateView ${javaClass.simpleName} ")

        updateSystemPageMode()
       // onSelect()
        updatSystemUI(getMainActivity()?.getSystemData())
        binding.apEditProductNameTxt.isSelected=true
        binding.apEditIpv6AddressLocal.isSelected=true

        return binding.root
    }


    fun updateSystemPageMode(){
        when(systemPageMode){

            SystemPageMode.Normal,SystemPageMode.Central->{
                SophosEditableStype.disableEditText(binding.apEditProductName)
                binding.apEditProductName.visibility=View.GONE
                binding.apEditProductNameTxt.visibility=View.VISIBLE
                ContextCompat.getColor(requireContext(),android.R.color.black).apply {
                    binding.apEditFirmwareVersion.setTextColor(this)
                    binding.apEditMacAddress.setTextColor(this)
                    binding.apEditManagementVlanId.setTextColor(this)
                    binding.apEditIpAddress.setTextColor(this)
                    binding.apEditDefaultGateway.setTextColor(this)
                    binding.apEditDns.setTextColor(this)
                    binding.apEditDhcpServer.setTextColor(this)
                    binding.apEditUptime.setTextColor(this)
                    binding.apEditSystemTime.setTextColor(this)
                    binding.apEditModel.setTextColor(this)

                    binding.apEditIpv6Address.setTextColor(this)
                    binding.apEditIpv6AddressLocal.setTextColor(this)

                }

            }
            SystemPageMode.Edit->{

                SophosEditableStype.enableEditText(binding.apEditProductName)
                binding.apEditProductNameTxt.visibility=View.GONE
                binding.apEditProductName.visibility=View.VISIBLE
               /* ContextCompat.getColor(requireContext(),R.color.sophos_edit_disable_gray).apply {
                    binding.apEditModel.setTextColor(this)
                    binding.apEditFirmwareVersion.setTextColor(this)
                    binding.apEditMacAddress.setTextColor(this)
                    binding.apEditManagementVlanId.setTextColor(this)
                    binding.apEditIpAddress.setTextColor(this)
                    binding.apEditDefaultGateway.setTextColor(this)
                    binding.apEditDns.setTextColor(this)
                    binding.apEditDhcpServer.setTextColor(this)
                    binding.apEditUptime.setTextColor(this)
                    binding.apEditSystemTime.setTextColor(this)
                    binding.apEditIpv6Address.setTextColor(this)
                    binding.apEditIpv6AddressLocal.setTextColor(this)
                }*/


            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        timer.removeMyTimer()
    }

    private fun updatSystemUI(system: GetSystem?){
        system?.data?.apply {
            binding.apEditFirmwareVersion.setText(firmware_version)
            binding.apEditMacAddress.setText(mac_address)
            binding.apEditManagementVlanId.setText(management_vlan_id.toString())
            binding.apEditProductName.setText(productName)
            binding.apEditProductNameTxt.setText(productName)
            binding.apEditIpAddress.setText(ip_address)
            binding.apEditDefaultGateway.setText(default_gateway)
            binding.apEditDns.setText(dns_server)
            binding.apEditDhcpServer.setText(dhcp_server)
            binding.apEditUptime.setText(uptime)
           // binding.apEditSystemTime.setText(this)
            binding.apEditModel.setText(model_name)
            binding.apEditSystemTime.setText(system_time)
            binding.apEditIpv6Address.setText(ipv6_address)
            binding.apEditIpv6AddressLocal.setText(ipv6_link_local_address)
            updateTime(this)
        }?: ConstantClass.printForDebug("GetSystem null ")

    }

    private fun updateTime(data: GetSystem.SystemData){

        data?.apply {
            var start=System.currentTimeMillis()
            var upD=0
            var upH=0
            var upM=0
            var upS=0
            val ft = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
            var systtemTime:Long=0
            tryCatchBlock {
                uptime?.let { uptime->
                    val iday=uptime.indexOf(" day")?:-1
                    if(iday>=0)
                        upD=uptime.trim()?.substring(0,iday)?.toInt()?:0

                    val idays=uptime.indexOf(" days")?:-1
                    var startTimeIndex=if(idays>0)idays+6 else if(iday>=0) iday+5 else 0
                    var time=uptime.substring(startTimeIndex,uptime.length)
                    ConstantClass.printForDebug("time=${time}")
                    var times=time?.split(":")
                    if(times?.size==3){
                        upH=times[0].toInt()
                        upM=times[1].toInt()
                        upS=times[2].replace(" ","").toInt()
                    }
                }
                systtemTime=ft.parse(system_time).time

            }

            timer.startTimer({


                    var current=System.currentTimeMillis()
                    var interval=Math.floor(((current-start).toDouble()/1000))

                    lanuchMain {
                        if(systtemTime>0) {
                            Date().apply {
                                time=systtemTime+(current-start)
                                binding.apEditSystemTime.setText(ft.format(this))
                            }

                        }
                        if(upD>0||upH>0||upM>0||upS>0){
                            var res=(interval+upS)

                            var s=(res%60).toInt()
                            //ConstantClass.printForDebug("interval=${interval} upS=${upS} res=${res} s=${s}")
                            var m=upM+Math.floor(res/60)
                            var m2=(m%60).toInt()
                            var h=upH+Math.floor(res/(60*60))
                            var h2=(h%60).toInt()
                            var d=(upD+Math.floor(res/(60*60*24))).toInt()
                            var dayResult=if(d<=0)"" else "${d} ${if(d>1) "days" else "day"}"
                            var final="${dayResult} ${if(h2<10) "0${h2}" else h2.toString()}:${if(m2<10) "0${m2}" else m2.toString()}:${if(s<10) "0${s}" else s.toString()}"
                            binding.apEditUptime.setText(final)
                        }
                    }

            },1000,1000)
        }
    }

    // 0:fail 1:success 2:no Change
    suspend fun updateValue():Int
    {
        var product=binding.apEditProductName.text.toString()

        var result=viewModel()?.updateProductName(product)!!

        return result
    }

    fun onSelect(){
        getMainActivity()?.apply {

            getSystem (getCurrentEditDevice()!!,{info,responseCode->
                lanuchMain {
                    info?.let { updatSystemUI(it) }
                }
            })
        }

    }

    fun deSelect(){
        timer.removeMyTimer()
    }

}