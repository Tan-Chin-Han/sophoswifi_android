package com.sophos.network.wireless.gui.system

import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.android.viewer.utils.TimerUtil
import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.gui.BaseFragment

import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.BuildConfig

import com.sophos.network.databinding.FragAboutBinding


class AboutFragment : BaseFragment() {
    companion object{
        fun getInstance()=  AboutFragment()

       val mTag = "AboutFragment"
    }

    val binding by viewBinding(FragAboutBinding::inflate)

    private var timer =TimerUtil()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug("onCreateView ${javaClass.simpleName} ")

        binding.mainAboutVersion.setText(BuildConfig.VERSION_NAME)
        var lisener=object :View.OnClickListener{
            override fun onClick(v: View?) {
                ConstantClass.printForDebug("about Terms Click+++++++++++++++++++")
                val uri = Uri.parse("https://www.sophos.com/en-us/legal/sophos-end-user-terms-of-use")
                val intent1 = Intent(Intent.ACTION_VIEW, uri)
                intent1.addCategory(Intent.CATEGORY_BROWSABLE)
                getMainActivity()?.startActivity(intent1)
            }

        }
        binding.aboutTermsLay.setOnClickListener(lisener)
        binding.aboutTermsTxt.apply {
            setOnClickListener(lisener)
            paintFlags =(getPaintFlags() or   Paint.UNDERLINE_TEXT_FLAG);
        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }


    override fun onBackPress() {
        super.onBackPress()
       /* var currentIndex= mainPages.indexOf(frags_page.about)
        getMainActivity()?.changeMainPage(mainPages.get(currentIndex-1))*/
        getMainActivity()!!.openDrawer()
    }

    override fun onSelect() {
        ConstantClass.printForDebug("$mTag onSelect ")


        super.onSelect()
    }

    override fun onDeSelect() {
        timer.removeMyTimer()
        super.onDeSelect()
    }


}