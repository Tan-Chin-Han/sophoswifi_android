package com.sophos.network.wireless.psclass.gson.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class SimpleResult {
    @Expose
    @SerializedName("error_code")
    val errorCode = -1

    @Expose
    @SerializedName("error_msg")
    val errorMsg:String?=null

    @Expose
    @SerializedName("data")
    val data:ResultData ?= null


    class ResultData{
        @Expose
        @SerializedName("result")
        val result:String ?= null

    }

    override fun toString(): String {
        return "SimpleResult(errorCode=$errorCode, errorMsg=$errorMsg, data=$data)"
    }
}