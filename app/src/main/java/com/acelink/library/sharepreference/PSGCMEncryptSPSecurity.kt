package com.acelink.library.sharepreference

import android.app.Application
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.content.edit
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.acelink.library.utils.EasyUtils.tryCatchBlock
import com.sophos.network.BuildConfig
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.security.KeyStore
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.spec.InvalidKeySpecException
import java.security.spec.KeySpec
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.DESKeySpec
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec



object PSGCMEncryptSPSecurity {

    //private val keyStoreGCMAlias="sophosGCMAlias"

    //use later App above 23
    private val sharedPreferencesGCMEncrypt: SharedPreferences by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
            var context=getApplication()
            val spec = KeyGenParameterSpec.Builder(
                MasterKey.DEFAULT_MASTER_KEY_ALIAS,
                KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                .setKeySize(256)
                .build()
            val masterKey: MasterKey = MasterKey.Builder(context!!)//This key is stored using the Android keystore system.
                .setKeyGenParameterSpec(spec)
                .build()
           /* val keyGenerator = KeyGenerator.getInstance(
                KeyProperties.KEY_ALGORITHM_AES,
                ANDROID_KEYSTORE
            )
            keyGenerator.init(spec)
            keyGenerator.generateKey()*/

            var spEncrypted= EncryptedSharedPreferences.create(
                context,
                "SophosEncryptSP",
                masterKey,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            )

        spEncrypted!!

    }


    private fun getApplication(): Application? {
       return tryCatchBlock({
            val activityThreadClass = Class.forName("android.app.ActivityThread")
            val method = activityThreadClass.getMethod("currentApplication")
            method.isAccessible=true
            //  if (BuildConfig.DEBUG) Log.i("111", "getApplication method=$method")
            return@tryCatchBlock method.invoke(null) as Application
        },null)
    }


    fun getAllGCMEncrypt()=sharedPreferencesGCMEncrypt.all


    fun clearGCMEncryptKey(key: String){
        sharedPreferencesGCMEncrypt.edit {
            remove(key)
            apply()
        }
    }

   /* @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setGCMClearString(context: Context?, vararg key: String?) {
        for (ss in key) {
            sharedPreferencesGCMEncrypt.edit().remove(ss).apply()
        }
    }*/


    /**
     * Checks if [SharedPreferences] contains the specific key
     *
     * @param context application context
     * @param key specific key
     */
  /*  fun hasKey(context: Context?, key: String?): Boolean {
        return sharedPreferencesGCMEncrypt.contains(key)
    }*/

    /**
     * Clears [SharedPreferences]
     *
     * @param context application context
     */
    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun clears() {
        sharedPreferencesGCMEncrypt.edit().clear().apply()
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun getStringGCM( key: String, defaultValue: String): String {
        return sharedPreferencesGCMEncrypt.getString(key, defaultValue)!!
    }

    /**
     * Sets the String into [SharedPreferences]
     *
     * @param context application context
     * @param key specific key
     * @param value stored value
     */
    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setStringGCM( key: String, value: String) {

        sharedPreferencesGCMEncrypt.edit().putString(key,value).apply()
    }








}