package com.acelink.library.sharepreference

import android.app.Application
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.content.edit
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import androidx.security.crypto.MasterKeys.AES256_GCM_SPEC
import com.sophos.network.BuildConfig
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.security.KeyStore
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.spec.InvalidKeySpecException
import java.security.spec.KeySpec
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.DESKeySpec
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

/*
private data class DESCipher(val cipher: Cipher,val myDesKey: SecretKey)

private data class CBCCipher(val cipher: Cipher,val myDesKey: SecretKey,val iv:IvParameterSpec)

object PSSPSecurity {
    private val sharedPreferences: SharedPreferences by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        getApplication()!!.getSharedPreferences("EncryptSPUtils", MODE_PRIVATE);
    }

    private val keyStoreCBCAlias="sophosCBCCipher"


    //use later App above 23
    private val sharedPreferencesGCMEncrypt: SharedPreferences by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
            var context=getApplication()
            val spec = KeyGenParameterSpec.Builder(
                MasterKey.DEFAULT_MASTER_KEY_ALIAS,
                KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                .setKeySize(256)
                .build()
            val masterKey: MasterKey = MasterKey.Builder(context!!)//This key is stored using the Android keystore system.
                .setKeyGenParameterSpec(spec)
                .build()
           /* val keyGenerator = KeyGenerator.getInstance(
                KeyProperties.KEY_ALGORITHM_AES,
                ANDROID_KEYSTORE
            )
            keyGenerator.init(spec)
            keyGenerator.generateKey()*/

            var spEncrypted= EncryptedSharedPreferences.create(
                context,
                "SophosEncryptSP",
                masterKey,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            )

        spEncrypted!!

    }


    private fun getApplication(): Application? {
        return try {
            val activityThreadClass = Class.forName("android.app.ActivityThread")
            val method = activityThreadClass.getMethod("currentApplication")
            method.isAccessible=true
            if (BuildConfig.DEBUG) Log.i("111", "getApplication method=$method")
            return method.invoke(null) as Application
        } catch (e: Exception) {
            null
        }
    }

    fun getAll()=sharedPreferences.all

    fun getAllGCMEncrypt()=sharedPreferencesGCMEncrypt.all

    fun setString( key: String, name: String) {
        sharedPreferences.edit().putString(key, name).apply()
    }

    fun getString( key: String): String {
        return sharedPreferences.getString(key, "")!!
    }

    fun clearKey(key: String){
        sharedPreferences.edit {
            remove(key)
            apply()
        }
    }

    fun clearGCMEncryptKey(key: String){
        sharedPreferencesGCMEncrypt.edit {
            remove(key)
            apply()
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setClearString(context: Context?, vararg key: String?) {
        for (ss in key) {
            sharedPreferences.edit().remove(ss).apply()
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setClearSound( vararg id: String) {
        for (ss in id) {
            sharedPreferences.edit().remove(encodeMD5(ss)).apply()
        }
    }

    /**
     * Gets the boolean form [SharedPreferences]
     * with specific key
     *
     * @param context application context
     * @param key specific key
     * @param defaultValue default value
     */
    fun getBoolean(context: Context?, key: String?, defaultValue: Boolean): Boolean {
        return sharedPreferences.getBoolean(key, defaultValue)
    }

    /**
     * Sets the boolean into [SharedPreferences]
     *
     * @param context application context
     * @param key specific key
     * @param value stored value
     */
    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setBoolean(context: Context?, key: String?, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value).apply()
    }

    /**
     * Gets the Integer form [SharedPreferences]
     * with specific key
     *
     * @param context application context
     * @param key specific key
     * @param defaultValue default value
     */
    fun getInt( key: String?, defaultValue: Int): Int {
        return sharedPreferences.getInt(key, defaultValue)
    }

    /**
     * Sets the Integer into [SharedPreferences]
     *
     * @param context application context
     * @param key specific key
     * @param value stored value
     */
    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setInt(key: String, value: Int) {
        sharedPreferences.edit().putInt(key, value).apply()
    }

    /**
     * Gets the Float from [SharedPreferences]
     * with specific key
     *
     * @param context application context
     * @param key specific key
     * @param defaultValue default value
     */
    fun getFloat(context: Context?, key: String?, defaultValue: Float): Float {
        return sharedPreferences.getFloat(key, defaultValue)
    }

    /**
     * Sets the Float into [SharedPreferences]
     *
     * @param context application context
     * @param key specific key
     * @param value stored value
     */
    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setFloat(context: Context?, key: String?, value: Float) {
        sharedPreferences.edit().putFloat(key, value).apply()
    }

    /**
     * Gets the Long from [SharedPreferences]
     * with specific key
     *
     * @param context application context
     * @param key specific key
     * @param defaultValue default value
     */
    fun getLong(context: Context?, key: String?, defaultValue: Long): Long {
        return sharedPreferences.getLong(key, defaultValue)
    }

    /**
     * Sets the Long into [SharedPreferences]
     *
     * @param context application context
     * @param key specific key
     * @param value stored value
     */
    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setLong(context: Context?, key: String?, value: Long) {
        sharedPreferences.edit().putLong(key, value).apply()
    }

    /**
     * Checks if [SharedPreferences] contains the specific key
     *
     * @param context application context
     * @param key specific key
     */
    fun hasKey(context: Context?, key: String?): Boolean {
        return sharedPreferences.contains(key)
    }

    /**
     * Clears [SharedPreferences]
     *
     * @param context application context
     */
    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun clears(context: Context?) {
        sharedPreferences.edit().clear().apply()
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun getStringGCM( key: String, defaultValue: String): String {
        return decodeCBCStringToBean(sharedPreferencesGCMEncrypt.getString(key, defaultValue)!!)
    }

    /**
     * Sets the String into [SharedPreferences]
     *
     * @param context application context
     * @param key specific key
     * @param value stored value
     */
    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setStringGCM( key: String, value: String) {
        val encode = encodeCBCToString(value)
        sharedPreferencesGCMEncrypt.edit().putString(key, encode).apply()
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun getStringDecrypt( key: String, defaultValue: String): String {
        return decodeCBCStringToBean(sharedPreferences.getString(key, defaultValue)!!)
    }

    /**
     * Sets the String into [SharedPreferences]
     *
     * @param context application context
     * @param key specific key
     * @param value stored value
     */
    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    fun setStringEncrypt( key: String, value: String) {
        val encode = encodeCBCToString(value)
        sharedPreferences.edit().putString(key, encode).apply()
    }

    @Throws(NoSuchAlgorithmException::class, InvalidKeySpecException::class)
    private fun getAESKeyFromPassword(password: String, salt: String): SecretKey? {
        val factory =
            SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256")
        val spec: KeySpec = PBEKeySpec(password.toCharArray(), salt.toByteArray(), 65536, 256)
        return SecretKeySpec(
            factory.generateSecret(spec)
                .encoded, "AES"
        )
    }


    //can  use below API 23
    private val desCipher: DESCipher by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        var c: DESCipher?=null
        try {
            val dks = DESKeySpec("sophosDesCipher".toByteArray(charset("UTF-8")))
            val skf = SecretKeyFactory.getInstance("DES")
            var myDesKey = skf.generateSecret(dks)
            var  cipher = Cipher.getInstance("DES/ECB/PKCS5Padding")
            c=   DESCipher(cipher!!, myDesKey!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return@lazy c!!

    }

    private val cbcCipher: CBCCipher by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {



        var c: CBCCipher?=null
        try {

            var cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
            var secretKey = loadSecretKey()
            if (secretKey == null) {
                secretKey = generateSecretKey()
            }


            val iv = IvParameterSpec("0102030405060708".toByteArray())
           //  val ivData = ByteArray(16)
          //   SecureRandom().nextBytes(ivData)
           //   var iv=IvParameterSpec(ivData)
            c=   CBCCipher(cipher!!, secretKey!!,iv!!)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return@lazy  c!!
    }

    private fun generateSecretKey(): SecretKey? {

        var keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore")

            keyGenerator.init(
                KeyGenParameterSpec.Builder(
                    "keyStoreCBCAlias",
                    KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                )
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(false)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .setRandomizedEncryptionRequired(false)
                    .build()
            )

        return  keyGenerator.generateKey()!!
    }

    private fun loadSecretKey(): SecretKey? {
        return try {
            val keyStore = KeyStore.getInstance(ANDROID_KEYSTORE)
            keyStore.load(null)
            if (!keyStore.containsAlias(keyStoreCBCAlias)) {
                return null
            }
            val secretKeyEntry = keyStore.getEntry(keyStoreCBCAlias, null) as KeyStore.SecretKeyEntry

            // 在 KeyStore 中保存的秘钥是无法拿到它的具体值的，通过如下方式获取只会返回 null
            // byte[] keyData = secretKey.getEncoded();
            secretKeyEntry.secretKey
        } catch (ex: java.lang.Exception) {

            null
        }
    }

    private fun encodeMD5(jsonString: String): String? {
        return try {
            val md = MessageDigest.getInstance("MD5")
            // 計算md5函數
            md.update(jsonString.toByteArray())
            val messageDigest = md.digest()
            // Create Hex String
            val hexString = StringBuffer()
            for (i in messageDigest.indices) {
                hexString.append(
                    Integer.toHexString(
                        0xFF and messageDigest[i]
                            .toInt()
                    )
                )
            }
            hexString.toString()
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.FROYO)
    private fun encodeCBCToString(jsonString: String): String {

        return try {
            val text = jsonString.toByteArray(StandardCharsets.UTF_8)

            cbcCipher.cipher.init(Cipher.ENCRYPT_MODE, cbcCipher.myDesKey, cbcCipher.iv)
            val textEncrypted = cbcCipher.cipher.doFinal(text)
            String(Base64.encode(textEncrypted, Base64.DEFAULT), StandardCharsets.UTF_8)
        } catch (e: Exception) {
            e.printStackTrace()
            jsonString
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.FROYO)
    private fun decodeCBCStringToBean(param: String): String {
     //   ConstantClass.printForDebug("decodeCBCStringToBean param="+param);
        return try {
            val decodedBytes = Base64.decode(param, Base64.DEFAULT)

            cbcCipher.cipher.init(Cipher.DECRYPT_MODE, cbcCipher.myDesKey, cbcCipher.iv)
            val textDecrypted = cbcCipher.cipher.doFinal(decodedBytes)
            String(textDecrypted, StandardCharsets.UTF_8)
        } catch (e: Exception) {
            if(param!=null&&!param.isEmpty())e.printStackTrace()
            param
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.FROYO)
    private fun encodeDESToString(jsonString: String): String {

        return try {
            val text = jsonString.toByteArray(charset("UTF-8"))
            desCipher.cipher.init(Cipher.ENCRYPT_MODE, desCipher.myDesKey, cbcCipher.iv)
            val textEncrypted = desCipher.cipher.doFinal(text)
            String(Base64.encode(textEncrypted, Base64.DEFAULT), Charset.forName("UTF-8"))
        } catch (e: Exception) {
            e.printStackTrace()
            jsonString
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.FROYO)
    private fun decodeDESStringToBean(param: String): String {
        //ConstantClass.printForDebug("decodeStringToBean param="+param);
        return try {
            val decodedBytes = Base64.decode(param, Base64.DEFAULT)
            desCipher.cipher.init(Cipher.DECRYPT_MODE, desCipher.myDesKey)
            val textDecrypted = desCipher.cipher.doFinal(decodedBytes)
            String(textDecrypted, Charset.forName("UTF-8"))
        } catch (e: Exception) {
            if(param!=null&&!param.isEmpty())e.printStackTrace()
            param
        }
    }
}*/