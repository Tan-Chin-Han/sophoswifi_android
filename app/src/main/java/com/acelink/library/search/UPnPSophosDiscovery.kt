package com.acelink.library.search

import android.content.Context
import android.net.wifi.WifiManager
import com.acelink.library.utils.EasyUtils.tryCatchBlock
import com.sophos.network.wireless.psclass.devcies.BaseSophosDevice
import com.acelink.library.utils.data.ConstantClass
import com.android.viewer.utils.TimerUtil
import java.io.IOException
import java.net.*
import java.util.*
import kotlin.collections.ArrayList

class UPnPSophosDiscovery{

    companion object {
        val totalDiscoveryTime=5000
        var timeoutDiscovery=totalDiscoveryTime

        val ssdpDiscoveryInterval=(10000)//10sec frequency and discovery time
        val ssdpDiscoveryNotFountToUnknown=6
        var lastTimeDiscovery:Long=-1
        val timer=TimerUtil()
        fun getDevice(context: Context): ArrayList<BaseSophosDevice>{
            timeoutDiscovery=totalDiscoveryTime
            timer.startTimer({
                timeoutDiscovery-=50

                //ConstantClass.printForDebug("timeoutDiscovery ${timeoutDiscovery}")
                if(timeoutDiscovery<=0){
                    timer.removeMyTimer()

                }
            },0,50)

            val devices = getDiscovery(context)
            return devices
          /* return ExecutorsUtils.Single.exeSingleForResult(object :Callable<XendDevice?>{
                override fun call(): XendDevice? {

                }
            })*/
        }

       // private const val timeout = 2000
        private const val timeoutLong = 4000//orignal 3000 ,may not found
       private val mCfgMaster = "www.sophos.com:device:WLANAccessPointDevice:1"


       private fun getDiscovery(c: Context): ArrayList<BaseSophosDevice> {
            var devices = ArrayList<BaseSophosDevice>()
            if (c.getSystemService(Context.WIFI_SERVICE) as? WifiManager != null) {

                var socket: DatagramSocket? = null

                tryCatchBlock {
                    val group = InetAddress.getByName("239.255.255.250")
                    val port = 1900
                    val query = "M-SEARCH * HTTP/1.1\r\n"
                        .plus("HOST: 239.255.255.250:${port}\r\n")
                        .plus( "ST:  ${mCfgMaster}\r\n")
                       // .plus( "ST: upnp:meshdevice\r\n")

                        .plus( "MAN: \"ssdp:discover\"\r\n")
                        .plus( "MX:  3\r\n")
                        .plus( "\r\n")
                    val ms = MulticastSocket(port)
                    if (socket == null) {
                        socket = DatagramSocket()
                        socket!!.reuseAddress = true
                        socket!!.broadcast = true
                    }
                    val queryBytes = query.toByteArray()
                    ConstantClass.printForDebug("query length=${queryBytes.size} content=\n${query}" )
                    tryCatchBlock {
                        ms.joinGroup(group)
                        ms.broadcast = true
                        //ms.soTimeout = 100
                        ms.soTimeout = timeoutLong
                        ms.setLoopbackMode(false);
                        //for (i in 0..2) {
                            /*if(i==2){
                                ms.soTimeout = timeout

                            }*/

                            val packet = DatagramPacket(queryBytes, queryBytes.size, group, port)

                            tryCatchBlock {
                                ms.send(packet)
                            }
                            response(ms, devices)

                       // }
                       // response(ms, device)
                        ms.leaveGroup(group)

                    }
                    ms.close()


                }
                    socket?.close()


            }
            return devices
        }


        //----------------------------------------------------------------------------------------------

        private fun response(ms: MulticastSocket, result: ArrayList<BaseSophosDevice>) {

            val bsReceive = ByteArray(2048)
            while (true) {
                lastTimeDiscovery =System.currentTimeMillis()
                var res=tryCatchBlock<Int>({
                    Arrays.fill(bsReceive, 0.toByte())
                    val dpReceive = DatagramPacket(bsReceive, bsReceive.size)
                    ms.receive(dpReceive)

                    if (dpReceive.length > 160) {
                        val str = String(dpReceive.data, 0, dpReceive.length)
                        ConstantClass.printForDebug( "discovery=\r\n$str")

                        if (isVendor(str)) {
                            // ConstantClass.printForDebug( "discovery isVendor=$str")
                            parseData(str, result)
                        }

                    }
                  return@tryCatchBlock 1
               },0)
                if(res==0){
                    ConstantClass.printForDebug( "discovery end------------>")
                    break
                }
               /* try {



                } catch (e: SocketTimeoutException) {
                    break
                } catch (e: IOException) {
                    e.printStackTrace()
                    break
                }*/
            }
        }


        private fun isVendor(str: String): Boolean
        {
            var nMaster = 0
            var nST = 0
            var isCorrectDevice = 0

            val aList = str.split("\r\n")

            for (item in aList)
            {
                if (item.startsWith("USN:"))
                {
                    if (item.indexOf(mCfgMaster) > 0)
                    {
                        nMaster = 1
                    }
                }else if (item.startsWith("ST:"))
                {
                    if (item.indexOf(mCfgMaster) > 0)
                    {
                        nST = 1
                    }
                } else if (item.startsWith("Vendor"))
                {
                    if (item.indexOf("Sophos") >= 0)
                    {
                        isCorrectDevice = 1
                    }
                }
            }
            if (nMaster > 0 && isCorrectDevice > 0&&nST>0)
            {
                return true
            }
            return false
        }
        //----------------------------------------------------------------------------------------------

        private fun parseData(str: String, result: ArrayList<BaseSophosDevice>) {
                var xendDevice=BaseSophosDevice()
            if (str.isNotEmpty()) {
                val aList = str.split("\r\n")
                for (item in aList) {
                    when {
                        item.startsWith("Mac:") -> xendDevice.mac = item.substringAfter("Mac:").trim().trim('\"')
                        item.startsWith("OperatorMode:") -> tryCatchBlock {
                            xendDevice.operatorMode = item.substringAfter("OperatorMode:").trim().trim('\"').toInt()
                        }

                        item.startsWith("Model:") -> xendDevice.model = item.substringAfter("Model:").trim().trim('\"')
                        item.startsWith("FirmwareVersion:") -> xendDevice.firmwareVersion = item.substringAfter("FirmwareVersion:").trim().trim('\"')

                        item.startsWith("Identity:") -> tryCatchBlock {
                            xendDevice.Identity = item.substringAfter("Identity:").trim().trim('\"').toInt()
                        }
                        item.startsWith("Name:") -> xendDevice.deviceName = item.substringAfter("Name:").trim().trim('\"')
                        item.startsWith("CPUUtilization:") ->tryCatchBlock {
                            xendDevice.CPUUtilization = item.substringAfter("CPUUtilization:").trim().trim('\"').toInt()
                        }
                        item.startsWith("WiFiRadio:") ->tryCatchBlock {
                            xendDevice.WiFiRadio = item.substringAfter("WiFiRadio:").trim().trim('\"').toInt()
                        }
                        item.startsWith("SNR:") ->tryCatchBlock {
                            xendDevice.SNR = item.substringAfter("SNR:").trim().trim('\"').toInt()
                        }
                        item.startsWith("MemoryUtilization:") ->tryCatchBlock {
                            xendDevice.MemoryUtilization = item.substringAfter("MemoryUtilization:").trim().trim('\"').toInt()
                        }
                        item.startsWith("LOCATION:") -> xendDevice.location = item.substringAfter("LOCATION:").trim().trim('\"')
                        item.startsWith("ConnectedUsers:") ->tryCatchBlock {
                            xendDevice.ConnectedUsers = item.substringAfter("ConnectedUsers:").trim().trim('\"').toInt()
                        }
                    }
                }
                ConstantClass.printForDebug( "xendDevice=\r\n$xendDevice")
                if(xendDevice.isComplete())
                {
                   var find=result.find { it.mac==xendDevice.mac }

                    if(find==null){
                        xendDevice.calculateHealth()
                       result.add(xendDevice)
                   }else{
                        find?.calculateHealth()
                        find.copy(xendDevice)
                   }
                /* result.copy(base = BaseXendDevice)
                        if(it.equals("Master",true)){
                            result.copy(base = BaseXendDevice)
                        }else if(it.equals("Slave",true)){
                            if(!result.remoteSearchSets.any { it-> it.mac?.equals(BaseXendDevice.mac)?:false}){
                                RemoteDevice().apply {
                                    copy(base = BaseXendDevice)
                                    result.remoteSearchSets.add(this)
                                }
                            }

                        }*/
                }else{
                  //  ConstantClass.printForDebug("${javaClass.simpleName} BaseXendDevice info not enough=${xendDevice.toString()}")
                }
            }
        }
    }

}