package com.acelink.library.utils.data

import android.util.Log
import com.sophos.network.BuildConfig


object ConstantClass {
    private const val Debugging = true
    private const val TAG = "Xend"


    fun printForDebug(msg: String?) {
        printForDebug(TAG,msg)
    }

    fun printForDebug(t: String,msg: String?) {
        if (msg == null || msg.isEmpty()) {
            if (Debugging && BuildConfig.DEBUG) Log.e(t,msg?:"null")
        } else {
            if (Debugging && BuildConfig.DEBUG) {
                val len = msg.length
                val count = len / 1000
                for (i in 0..count) {
                    Log.e(
                        TAG,
                        msg.substring(i * 1000, if (i >= count - 2) msg.length else (i + 1) * 1000)
                    )
                    if (i >= count - 2) {
                        break
                    }
                }
            }
        }
    }

}