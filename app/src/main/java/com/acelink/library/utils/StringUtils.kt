package com.acelink.library.utils

import android.os.Bundle
import com.acelink.library.utils.EasyUtils.tryCatchBlock
import com.acelink.library.utils.data.ConstantClass
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


object StringUtils {

      /*  fun bundle2string(bundle: Bundle?): String {
            if (bundle == null) {
                return "null"
            }
            var string = "Bundle{"
            for (key in bundle.keySet()) {
                string += " " + key + " => " + bundle[key] + ";"
            }
            string += " }Bundle"
            return string
        }

        fun validateMAC(mac: String?): Boolean {
            if(mac==null)return false
            val pattern = Pattern.compile("^([0-9A-Fa-f]{2}[\\.:-]){5}([0-9A-Fa-f]{2})$")
            val matcher = pattern.matcher(mac)
            val normalizedPattern = Pattern.compile("^[0-9a-fA-F]{12}$")
            val normalizedMatcher = normalizedPattern.matcher(mac)

            if (!matcher.matches() && !normalizedMatcher.matches()) {
                return false
            }

            return true
        }

        fun generateUUID()= UUID.randomUUID().toString().replace("-","").uppercase()

        var pTwo = Pattern.compile("[^x00-xff]")
        */
        fun hasTwoBytes(input: String): Boolean {
         return tryCatchBlock({

              for(i in 0.. input.length-1){
                  // var asic=Character.codePointAt(input,i)
                  if(input.get(i).code < 0 || input.get(i).code>255){
                      ConstantClass.printForDebug("hasTwoBytes c= ${input.get(i)}")
                      return@tryCatchBlock true
                  }

              }
             return@tryCatchBlock false
          },false)!!

        }

}