package com.acelink.library.utils

import android.net.InetAddresses
import android.os.Build
import android.util.Patterns
import androidx.fragment.app.Fragment
import com.acelink.library.utils.JudgeIpAddress
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.R
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.DeviceLanPortFragment

import java.util.regex.Pattern
import kotlin.jvm.JvmStatic
import kotlin.experimental.inv

object JudgeIpAddress {
       //功能：判斷標準IPv6地址的正則表示式
  /*  private val IPV6_STD_REGEX = Pattern.compile(
        "^(?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}$"
    )

    //功能：判斷一般情況壓縮的IPv6正則表示式
    private val IPV6_COMPRESS_REGEX = Pattern.compile(
        "^((?:[0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4})*)?)::((?:([0-9A-Fa-f]{1,4}:)*[0-9A-Fa-f]{1,4})?)$"
    )

    /*由於IPv6壓縮規則是必須要大於等於2個全0塊才能壓縮
             不合法壓縮 ： fe80:0000:8030:49ec:1fc6:57fa:ab52:fe69
    ->           fe80::8030:49ec:1fc6:57fa:ab52:fe69
            該不合法壓縮地址直接壓縮了處於第二個塊的單獨的一個全0塊，
            上述不合法地址不能通過一般情況的壓縮正則表示式IPV6_COMPRESS_REGEX判斷出其不合法
            所以定義瞭如下專用於判斷邊界特殊壓縮的正則表示式
    (邊界特殊壓縮：開頭或末尾為兩個全0塊，該壓縮由於處於邊界，且只壓縮了2個全0塊，不會導致':'數量變少)*/
    //功能：抽取特殊的邊界壓縮情況
    private val IPV6_COMPRESS_REGEX_BORDER = Pattern.compile(
        "^(::(?:[0-9A-Fa-f]{1,4})(?::[0-9A-Fa-f]{1,4}){5})|((?:[0-9A-Fa-f]{1,4})(?::[0-9A-Fa-f]{1,4}){5}::)$"
    )



    //Judge IPv6 Address is right
    fun isIPv6Address(input: String): Boolean {
        var NUM = 0
        for (i in 0 until input.length) {
            if (input[i] == ':') NUM++
        }
        if (NUM > 7) return false
        if (IPV6_STD_REGEX.matcher(input).matches()) {
            return true
        }
        return if (NUM == 7) {
            IPV6_COMPRESS_REGEX_BORDER.matcher(input).matches()
        } else {
            IPV6_COMPRESS_REGEX.matcher(input).matches()
        }
    }*/

    //Judge IPv4 Address is right
    fun isIPv4Address(input: String?): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            (input?:"").run {
                (":" in this).not()&&InetAddresses.isNumericAddress(this)
            }

        }else{
            Patterns.IP_ADDRESS.matcher(input).matches()
        }
    }

    var maskAllowNum=arrayOf(0,128,192,224,240,248,252,254,255)
    fun CheckMaskandIPAddress(fragment: DeviceLanPortFragment, mask: String, ip: String, dhcpEnabled: Boolean, start: String, end: String): Boolean {
        kotlin.runCatching {
            fragment.run {
                var maskIPs=mask.split(".")
                var lanIPs=ip.split(".")
                if(lanIPs.size!=4){
                    showMessageDialog("${getString(R.string.ip_address)} ${getString(R.string.ip_wrong)}")
                    return false
                }
                if(maskIPs.size!=4){
                    showMessageDialog("${getString(R.string.invalid_submask)}")
                    return false
                }
                var maskIP0=maskIPs[0].toInt()
                if(maskIP0!=255){
                    showMessageDialog("${getString(R.string.invalid_submask)}")
                    return false
                }
                var maskIP1=maskIPs[1].toInt()
                var maskIP2=maskIPs[2].toInt()
                var maskIP3=maskIPs[3].toInt()
                if(!maskAllowNum.any { it==maskIP1 }){
                    showMessageDialog("${getString(R.string.invalid_submask)}")
                    return false
                }
                if(!maskAllowNum.any { it==maskIP2 }){
                    showMessageDialog("${getString(R.string.invalid_submask)}")
                    return false
                }
                if(!maskAllowNum.any { it==maskIP3 }){
                    showMessageDialog("${getString(R.string.invalid_submask)}")
                    return false
                }

                var lanIP0=lanIPs[0].toInt()
                var lanIP1=lanIPs[1].toInt()
                var lanIP2=lanIPs[2].toInt()
                var lanIP3=lanIPs[3].toInt()
                var funStartCheck:(start:String)->List<String>?={start->
                    var starts=start.split(".")
                    if(starts.size!=4){
                        showMessageDialog("${getString(R.string.starting_ip)} ${getString(R.string.ip_wrong)}")
                         null
                    }else starts
                }
                var funEndCheck:(end:String)->List<String>?={end->
                    var ends=end.split(".")
                    if(ends.size!=4){
                        showMessageDialog("${getString(R.string.ending_ip)} ${getString(R.string.ip_wrong)}")
                        null
                    }else ends
                }

                // class A
                if(maskIP1!=255)
                {
                   if(lanIP0<1||lanIP0>126)
                   {
                       showMessageDialog("${getString(R.string.ip_address)} ${getString(R.string.ip_wrong)}")
                       return false
                   }
                    //subnet-broadcast
                    if(dhcpEnabled)
                    {
                        val starts=funStartCheck.invoke(start)
                        val ends=funEndCheck.invoke(end)
                        if(starts==null||ends==null)
                        {
                            return false
                        }
                        val starts1=starts.get(1).toInt()
                        val ends1=ends.get(1).toInt()
                        val notMask= (maskIP1 and 0xFF).toByte().inv()
                        var broadcast=(maskIP1 and lanIP1)+notMask
                        if(maskIP1==0)broadcast=255
                        val subnet=maskIP1 and lanIP1
                        ConstantClass.printForDebug("Class A subnet=${subnet} broadcast=${broadcast}")
                        var same0=(starts.get(2).toInt()==0&&starts.get(3).toInt()==0)
                        var same255=(starts.get(2).toInt()==255&&starts.get(3).toInt()==255)
                        var end0=(ends.get(2).toInt()==0&&ends.get(3).toInt()==0)
                        var end255=(ends.get(2).toInt()==255&&ends.get(3).toInt()==255)
                         if(starts1<subnet||starts1>broadcast||((starts1==subnet&&same0)||(starts1==broadcast&&same255))){
                            showMessageDialog("${getString(R.string.starting_ip)} ${getString(R.string.ip_wrong)}")
                            return false
                        }else if(ends1<subnet||ends1>broadcast||((ends1==subnet&&end0)||(ends1==broadcast&&end255))){
                            showMessageDialog("${getString(R.string.ending_ip)} ${getString(R.string.ip_wrong)}")
                            return false
                        }
                        val totalIP=lanIP1*255*255+lanIP2*255+lanIP3
                        val totalStart=starts.get(1).toInt()*255*255+starts.get(2).toInt()*255+starts.get(3).toInt()
                        val totalEnd=ends.get(1).toInt()*255*255+ends.get(2).toInt()*255+ends.get(3).toInt()
                        if(totalIP==totalStart||lanIP0!=starts.get(0).toInt()){
                            showMessageDialog("${getString(R.string.starting_ip)} ${getString(R.string.ip_wrong)}")
                            return false
                        }
                        if(totalIP==totalEnd||(totalIP>totalStart&&totalIP<totalEnd)||(totalEnd<totalStart)||lanIP0!=ends.get(0).toInt())
                        {
                            showMessageDialog("${getString(R.string.ending_ip)} ${getString(R.string.ip_wrong)}")
                            return false
                        }
                    }

                    return true
                }
                // class B
                if(maskIP2!=255)
                {
                    if(lanIP0<1||lanIP0>191)
                    {
                        showMessageDialog("${getString(R.string.ip_address)} ${getString(R.string.ip_wrong)}")
                        return false
                    }
                    if(dhcpEnabled)
                    {
                        var starts=funStartCheck.invoke(start)
                        var ends=funEndCheck.invoke(end)
                        if(starts==null||ends==null)
                        {
                            return false
                        }
                        val starts2=starts.get(2).toInt()
                        val ends2=ends.get(2).toInt()
                        val notMask= (maskIP2 and 0xFF).toByte().inv()
                        var broadcast=(maskIP2 and lanIP2)+notMask
                        if(maskIP2==0)broadcast=255
                        val subnet=maskIP2 and lanIP2
                        ConstantClass.printForDebug("Class B subnet=${subnet} broadcast=${broadcast}")
                        var same0=(starts.get(3).toInt()==0)
                        var same255=(starts.get(3).toInt()==255)
                        var end0=(ends.get(3).toInt()==0)
                        var end255=(ends.get(3).toInt()==255)
                         if(starts2<subnet||starts2>broadcast||((starts2==subnet&&same0)||(starts2==broadcast)&&same255)){
                            showMessageDialog("${getString(R.string.starting_ip)} ${getString(R.string.ip_wrong)}")
                            return false
                        }else if(ends2<subnet||ends2>broadcast||((ends2==subnet&&end0)||(ends2==broadcast)&&end255)){
                            showMessageDialog("${getString(R.string.ending_ip)} ${getString(R.string.ip_wrong)}")
                            return false
                        }
                        val totalIP=lanIP2*255+lanIP3
                        val totalStart=starts.get(2).toInt()*255+starts.get(3).toInt()
                        val totalEnd=ends.get(2).toInt()*255+ends.get(3).toInt()
                        if(totalIP==totalStart){
                            showMessageDialog("${getString(R.string.starting_ip)} ${getString(R.string.ip_wrong)}")
                            return false
                        }
                        if(totalIP==totalEnd||(totalIP>totalStart&&totalIP<totalEnd)||(totalEnd<totalStart))
                        {
                            showMessageDialog("${getString(R.string.ending_ip)} ${getString(R.string.ip_wrong)}")
                            return false
                        }
                    }
                    return true
                }
               // class C
                if(lanIP0<1||lanIP0>223){
                    showMessageDialog("${getString(R.string.ip_address)} ${getString(R.string.ip_wrong)}")
                    return false
                }
                if(dhcpEnabled){
                    var starts=funStartCheck.invoke(start)
                    var ends=funEndCheck.invoke(end)
                    if(starts==null||ends==null)
                    {
                        return false
                    }
                    val starts3=starts?.get(3)?.toInt()?:-1
                    val ends3=ends?.get(3)?.toInt()?:-1
                    var notMask= (maskIP3 and 0xFF).toByte().inv()
                    var broadcast=(maskIP3 and lanIP3)+notMask
                    if(maskIP3==0)broadcast=255
                    var subnet=maskIP3 and lanIP3
                    ConstantClass.printForDebug("Class C maskIP3=${maskIP3} lanIP3=${lanIP3} subnet=${subnet} broadcast=${broadcast}")
                     if(starts3<=subnet||starts3>=broadcast){
                        showMessageDialog("${getString(R.string.starting_ip)} ${getString(R.string.ip_wrong)}")
                        return false
                    }else if(ends3<=subnet||ends3>=broadcast){
                        showMessageDialog("${getString(R.string.ending_ip)} ${getString(R.string.ip_wrong)}")
                        return false
                    }
                    val totalIP=lanIP3
                    val totalStart=starts.get(3).toInt()
                    val totalEnd=ends.get(3).toInt()
                    if(totalIP==totalStart||starts.get(0)?.toInt()!=lanIP0||starts.get(1).toInt()!=lanIP1||starts.get(2).toInt()!=lanIP2)
                    {
                        showMessageDialog("${getString(R.string.starting_ip)} ${getString(R.string.ip_wrong)}")
                        return false
                    }
                    if(totalIP==totalEnd||(totalIP>totalStart&&totalIP<totalEnd)||(totalEnd<totalStart)
                        ||ends.get(0).toInt()!=lanIP0||ends.get(1).toInt()!=lanIP1||ends.get(2).toInt()!=lanIP2
                    )
                    {
                        showMessageDialog("${getString(R.string.ending_ip)} ${getString(R.string.ip_wrong)}")
                        return false
                    }
                }

                return true

            }
        }.onFailure {
            it.printStackTrace()

        }
        return false
    }




}