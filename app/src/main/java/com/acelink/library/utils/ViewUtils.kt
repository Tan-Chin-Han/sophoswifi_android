package com.android.viewer.utils

import android.animation.TypeEvaluator
import android.animation.ValueAnimator
import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Rect
import android.os.Build
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.IdRes
import android.view.WindowInsets

import android.view.WindowMetrics
import androidx.annotation.RequiresApi
import androidx.core.view.WindowInsetsCompat


class ViewUtils private constructor() {

    companion object {

        fun getWindowHeight(activity: Activity): Int
        {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val metrics = activity.windowManager.currentWindowMetrics
                val insets = metrics.windowInsets.getInsets(WindowInsets.Type.systemBars())
                metrics.bounds.height() - insets.bottom - insets.top
            } else //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                val view = activity.window.decorView
                val insets = WindowInsetsCompat.toWindowInsetsCompat(view.rootWindowInsets, view).getInsets(WindowInsetsCompat.Type.systemBars())
                activity.resources.displayMetrics.heightPixels - insets.bottom - insets.top
            }/*else {
                getMetrics(activity.windowManager).heightPixels
            }*/
        }

     /*   fun getWindowWidth(activity: Activity): Int
        {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val metrics = activity.windowManager.currentWindowMetrics
                val insets = metrics.windowInsets.getInsets(WindowInsets.Type.systemBars())
                metrics.bounds.width() - insets.left - insets.right
            } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                val view = activity.window.decorView
                val insets = WindowInsetsCompat.toWindowInsetsCompat(view.rootWindowInsets, view).getInsets(WindowInsetsCompat.Type.systemBars())
                activity.resources.displayMetrics.widthPixels - insets.left - insets.right
            }else {
                getMetrics(activity.windowManager).widthPixels
            }
        }*/


      /*  fun getRelativeTop(view: View): Int {
            return if (view.parent === view.rootView) {
                view.top
            } else view.top + getRelativeTop(
                view.parent as View
            )
        }*/

        /**
         * Returns screen Metrics
         */
      /*  fun getMetrics(windowManager: WindowManager): DisplayMetrics {
            val displaymetrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(displaymetrics)
            return displaymetrics
        }*/
/*
        /**
         * Returns the screen density
         *
         *
         * 120dpi = 0.75
         * 160dpi = 1 (default)
         * 240dpi = 1.5
         * 320dpi = 2
         * 400dpi = 2.5
         *
         * @param context application context
         * @return Device density
         */
        fun getDensity(context: Context): Float {
            val metrics = context.resources.displayMetrics
            return metrics.density
        }

        /**
         * Returns the screen ScaledDensity
         *
         * @param context application context
         * @return Device scaled density
         */
        fun getScaledDensity(context: Context): Float {
            val metrics = context.resources.displayMetrics
            return metrics.scaledDensity
        }

               /**
         * Coverts px to dp.
         *
         * @param px pixel
         * @param context application context
         * @return dp
         */
        fun convertPixelToDp(px: Float, context: Context): Int {
            return (px / getDensity(context) + 0.5f).toInt()
        }

        /**
         * Coverts dp to px.
         *
         * @param dp dp
         * @param context application context
         * @return pixel
         */
        fun convertDpToPixel(dp: Float, context: Context): Int {
            return (dp * getDensity(context) + 0.5f).toInt()
        }

        /**
         * Converts px to sp.
         *
         * @param px pixel
         * @param context application context
         * @return sp
         */
        fun convertPixelToSp(px: Float, context: Context): Int {
            return (px / getScaledDensity(context) + 0.5f).toInt()
        }

        /**
         * Converts sp to px.
         *
         * @param dp sp
         * @param context application context
         * @return dp
         */
        fun convertSpToPixel(dp: Float, context: Context): Int {
            return (dp * getScaledDensity(context) + 0.5).toInt()
        }

        /**
         * Returns the status bar height
         *
         * @param activity activity context
         * @return status bar height in pixel
         */
        fun getStatusBarHeight(activity: Activity): Int {
            val rectangle = Rect()
            val window = activity.window
            window.decorView.getWindowVisibleDisplayFrame(rectangle)
            return rectangle.top
        }*/


        fun hideStatusBar(activity: Activity) {
            activity.run {
                runOnUiThread {
                    val decorView = window.decorView
                    window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
                    // Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
                    actionBar?.hide()
                }
            }

        }

        fun showStatusBar(activity: Activity) {
            activity.run {
                runOnUiThread {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        window.setDecorFitsSystemWindows(false)
                        window.insetsController?.show(WindowInsets.Type.statusBars())
                    } else {
                        // Show status bar
                        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
                        window.decorView.systemUiVisibility =
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    }
                    actionBar?.show()
                }
            }

        }
    }


}