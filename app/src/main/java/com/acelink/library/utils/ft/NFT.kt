package com.acelink.library.utils.ft

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.net.wifi.ScanResult
import android.net.wifi.SupplicantState
import android.net.wifi.WifiManager
import android.os.Build
import android.widget.Toast

import com.acelink.library.utils.data.ConstantClass.printForDebug
import java.util.*


object NFT {
   /* fun getConnectedDevWiFiSSID(context: Context): String? {
        printForDebug("getConnectedDevWiFiSSID")
        val wm = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        return getConnectedDevWiFiSSID(context, wm)
    }

    fun getConnectedDevWiFiSSID(context: Context, wm: WifiManager?): String? {
        var wm = wm
        if (wm == null) {
            wm = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        }
        if (wm != null && wm.isWifiEnabled) {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    ?: return null
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                for (network in connectivityManager.allNetworks) {
                    val nc = connectivityManager.getNetworkCapabilities(network) ?: continue
                    if (nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        if (isWifiConnected(wm)) {
                            val info = wm.connectionInfo
                            if (info != null) {
                                val ssid = info.ssid
                                printForDebug("21+ check dev ssid = $ssid")
                                if (info.ssid.indexOf(APmodeSSID) == 0 || info.ssid.indexOf("\"" + APmodeSSID) == 0) {
                                    return info.ssid
                                }
                            }
                        } else {
                            printForDebug("21+ check dev isWifiConnected =false ")
                        }
                    }
                }
            } else { // api below 21
                val mWifi = connectivityManager.activeNetworkInfo
                if (mWifi != null) {
                    if (mWifi.type == ConnectivityManager.TYPE_WIFI) {
                        if (mWifi.isConnected) {
                            val info = wm.connectionInfo
                            if (info != null) {
                                val ssid = info.ssid
                                // Log.e("yoyo", "check dev ssid = " + ssid);
//                if (isAirBoxHotSpot(ssid)) {
////                  return ssid;
////                }
                                if (info.ssid.indexOf(APmodeSSID) == 0 || info.ssid.indexOf("\"" + APmodeSSID) == 0) {
                                    return info.ssid
                                }
                            }
                        }
                    }
                }
            }
        }
        return null
    }

    /**
     * Check whether WiFi is connected
     * @return true if Wifi is connected
     */
    // @RequiresPermission(value = Manifest.permission.ACCESS_WIFI_STATE)
    fun isWifiConnected(wifiManager: WifiManager?): Boolean {
        if (wifiManager == null || !wifiManager.isWifiEnabled) return false
        val wifiInfo = wifiManager.connectionInfo
        printForDebug("  isWifiConnected wifiInfo.getNetworkId() =" + if (wifiInfo == null) "null" else wifiInfo.networkId.toString() + ",getSupplicantState=" + wifiInfo.supplicantState)
        return if (wifiInfo == null || wifiInfo.networkId == -1) false else wifiInfo.supplicantState == SupplicantState.ASSOCIATED || wifiInfo.supplicantState == SupplicantState.COMPLETED
    }

    fun getConnectedDevWiFiSSID(networkInfo: NetworkInfo?, wm: WifiManager?): String? {
        if (wm != null && wm.isWifiEnabled) {
            if (networkInfo == null) {
                return null
            }
            val info = wm.connectionInfo
            if (info != null) {
                val ssid = info.ssid
                //Log.e("yoyo", "21+check dev ssid = " + ssid);
//        if (isAirBoxHotSpot(ssid)) {
//          return ssid;
//        }
                if (info.ssid.toLowerCase().indexOf("setup") > 5) {
                    if (info.ssid.indexOf("EdiPlug") == 0 || info.ssid.indexOf("\"EdiPlug") == 0 /*|| (info.getSSID().indexOf("EdiView") == 0) || (info.getSSID()
                .indexOf("\"EdiView") == 0)*/) {
                        return info.ssid
                    }
                }
            }
        }
        return null
    }

    fun connectedWiFiStatus(context: Context, ssid: String): Boolean {
        val wm = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        if (wm != null && wm.isWifiEnabled) {
            val mgrConn =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                for (network in mgrConn.allNetworks) {
                    val nc = mgrConn.getNetworkCapabilities(network) ?: continue
                    if (nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        if (isWifiConnected(wm)) {
                            val info = wm.connectionInfo
                            if (info != null) {
                                if (info.ssid == ssid || info.ssid == "\"" + ssid + "\"") {
                                    return true
                                }
                            }
                        }
                    }
                }
            } else {
                val netInfo = mgrConn.activeNetworkInfo
                if (netInfo != null && netInfo.type == ConnectivityManager.TYPE_WIFI && isWifiConnected(
                        wm
                    )
                ) {
                    val info = wm.connectionInfo
                    if (info != null) {
                        if (info.ssid == ssid || info.ssid == "\"" + ssid + "\"") {
                            return true
                        }
                    }
                }
            }
        }
        return false
    }

    /**
     * Gets wifi hot-spot filtered by Edimax production
     */
    fun getDevWiFi(context: Context): List<ScanResult> {
        val list: MutableList<ScanResult> = ArrayList()
        val wm = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        if (wm != null && wm.isWifiEnabled) {
            val scanResults = wm.scanResults
            for (count in scanResults.indices) {
                val wifi = scanResults[count]
                if (wifi.SSID == null) {
                    continue
                }
                /**
                 * EdiPlug.Setup    (SmartPlug)
                 * EdiView.Setup    (IPCam)
                 * edimaxView.setup (Router and Extender)
                 */
                if (wifi.SSID.toLowerCase().indexOf("setup") > 5) {
                    if (wifi.SSID.indexOf("EdiPlug") == 0 || wifi.SSID.indexOf("\"EdiPlug") == 0 /*|| (wifi.SSID.indexOf("EdiView") == 0)
              || (wifi.SSID.indexOf("\"EdiView") == 0)*/) {
                        if (wifi.capabilities.toLowerCase()
                                .contains("wpa") || wifi.capabilities.toLowerCase()
                                .contains("wep")
                        ) {
                            // Skip the hot-spot that has authentication
                            continue
                        }
                        list.add(wifi)
                    }
                }
            }
        }
        return list
    }



    fun getNetworkType(context: Context): Int {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo ?: return -1
        return netInfo.type
    }



    fun showEnableWifiMessage(context: Context?) {
        Toast.makeText(context, "Please enable your Wi-Fi.", Toast.LENGTH_LONG).show()
    }

    fun getWifiManager(context: Context): WifiManager {
        return  context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
    }


fun View.showSnackMessage(
    message: String?,
    anchorView: View? = null,
    backgroundColor: Int,
    textColor: Int,
    length: Int = Snackbar.LENGTH_SHORT
) {
    message?.let {
        try {
            val snack = Snackbar.make(this, it, length)
            snack.setBackgroundTint(ContextCompat.getColor(context, backgroundColor))
            snack.setTextColor(ContextCompat.getColor(context, textColor))
            snack.anchorView = anchorView
            snack.show()
        } catch (ex: Exception) {
            ex.showLog()
        }
    }
}



    fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            val net = cm.activeNetwork
            val nc = cm.getNetworkCapabilities(net)
            nc != null && nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) && nc.hasCapability(
                NetworkCapabilities.NET_CAPABILITY_VALIDATED
            )
        } else {
            val netInfo = cm.activeNetworkInfo
            netInfo != null && netInfo.isConnected
        }
    }

fun Context.isNetworkAvailable(): Boolean {
    val manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        val capabilities = manager.getNetworkCapabilities(manager.activeNetwork)
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                return true
            }
        }
    } else {
        try {
            val activeNetworkInfo = manager.activeNetworkInfo
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                return true
            }
        } catch (e: Exception) {
            e.showLog()
        }
    }
    return false
}

    */
}