package com.acelink.library.pagetool

import android.app.Activity
import android.app.Dialog
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import kotlinx.coroutines.*


object BaseBindingTool {

    inline fun <T : ViewBinding> Activity.viewBinding(
        crossinline bindingInflater: (LayoutInflater) -> T
    ) = lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        bindingInflater.invoke(layoutInflater)
    }

    inline fun <T : ViewBinding> Fragment.viewBinding(
        crossinline bindingInflater: (LayoutInflater) -> T
    ) = lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        bindingInflater.invoke(layoutInflater)
    }

    inline fun <T : ViewBinding> Fragment.viewBinding(
        crossinline bindingInflater: (LayoutInflater) -> T,inflater: LayoutInflater
    ) = lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        bindingInflater.invoke(layoutInflater?:inflater)
    }

    inline fun <T : ViewBinding> ViewGroup.viewBinding(
        crossinline block: (inflater: LayoutInflater, container: ViewGroup, attach: Boolean) -> T
    ) = lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        block.invoke(LayoutInflater.from(context),this,false)
    }

    inline fun <T : ViewBinding> Dialog.viewBinding(
        crossinline bindingInflater: (LayoutInflater) -> T
    ) = lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        bindingInflater.invoke(layoutInflater)
    }

}

open class BaseBindingHolder<VB : ViewBinding>(val bindingHolder: VB) : RecyclerView.ViewHolder(bindingHolder.root)


object CoroutineIO{

    // SupervisorJob
    //val supervisorScope = CoroutineScope(SupervisorJob())

    var contextIO= CoroutineExceptionHandler { coroutineContext, e -> e.printStackTrace() } + Dispatchers.IO
    private set

    var contextMain=CoroutineExceptionHandler { coroutineContext, e -> e.printStackTrace() } + Dispatchers.Main
        private set

    fun Fragment.lanuchIO( b: suspend CoroutineScope.() -> Unit): Job {
        return lifecycleScope.launch(contextIO,block =b )
    }

    fun AppCompatActivity.lanuchIO(b: suspend CoroutineScope.() -> Unit): Job {
        return lifecycleScope.launch(contextIO,block =b )
    }

    fun Fragment.lanuchMain( b: suspend CoroutineScope.() -> Unit): Job {
        return lifecycleScope.launch(contextMain,block =b )
    }

    fun AppCompatActivity.lanuchMain(b: suspend CoroutineScope.() -> Unit): Job {
        return lifecycleScope.launch(contextMain,block =b )
    }

    fun ViewModel.lanuchIO(b: suspend CoroutineScope.() -> Unit): Job {
        return viewModelScope.launch(contextIO,block =b )
    }

    fun ViewModel.lanuchMain(b: suspend CoroutineScope.() -> Unit): Job {
        return viewModelScope.launch(contextMain,block =b )
    }
}
