package com.sophos.network.wireless.pagetool


import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.lifecycleScope
import com.sophos.network.R
import com.sophos.network.wireless.pagetool.ViewPage2Pages.Companion.mainPages
import com.sophos.network.wireless.pagetool.ViewPage2Pages.Companion.welcomePages
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.wireless.MainViewModel.Companion.currentPage
import kotlinx.coroutines.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.coroutines.coroutineContext

val retainMinSize=1
private var useRetainSize=true

class ViewPage2Pages{
    companion object{
        var  welcomePages=ArrayList<frags_page>().apply {
            if(size==0)
            add(frags_page.wlecome)
        }
        var  mainPages=ArrayList<frags_page>().apply {
            if(size==0)
              add(frags_page.allList)
        }
    }
}

@Synchronized
fun AppCompatActivity.changeWelecomsPage( viewGroup: FrameLayout,page:frags_page){
    if( mainPages.indexOf(currentPage.value)>0){
        removePages(mainPages)
    }
    changeCurrentPage(welcomePages,viewGroup,page)
}

@Synchronized
fun AppCompatActivity.changeMainPage( viewGroup: FrameLayout,page:frags_page){
    if( welcomePages.indexOf(currentPage.value)>0){
        removePages(welcomePages)
    }
    changeCurrentPage(mainPages,viewGroup,page)
}


@Synchronized
private fun AppCompatActivity.changeCurrentPage(pages:ArrayList<frags_page>, viewGroup: FrameLayout,page:frags_page){
    var resultIndex= pages.indexOf(page)
   ConstantClass.printForDebug("changeItem ---------> ${page} resultIndex=${resultIndex}  currentItem=${currentPage.value} mainPages size=${mainPages.size}")
    if(resultIndex<0)
    {
        pages.add(page)
        resultIndex= pages.size-1
    }
    var currentIndex= pages.indexOf(currentPage.value)

    val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
    transaction.apply {
        if(currentPage.value!=frags_page.leaveWelcome&&currentPage.value!=frags_page.wlecome)
        {
            if(resultIndex>currentIndex){
                setCustomAnimations(R.anim.slide_right_in, R.anim.slide_left_out);
            }else{
                setCustomAnimations(R.anim.slide_left_in, R.anim.slide_right_out);
            }
            //setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        }
        if (page.pageFragment==null) {
            add(viewGroup.id, page.getInstance(),page.getTag())
        } else {
            show(page.pageFragment!!)
        }
        if(currentPage.value!=page){
            currentPage.value?.pageFragment?.let {
                // ConstantClass.printForDebug("currentPage hide")
                hide(it)
            }
        }

        setReorderingAllowed(true)
        addToBackStack(null)
        commitAllowingStateLoss()
        currentPage.value?.pageFragment?.onDeSelect()
        currentPage.postValue(page)
    }

            if(useRetainSize){
                if(resultIndex<retainMinSize){
                    synchronized(mainPages){
                        if(mainPages.size>retainMinSize){

                            var sub=mainPages.subList(retainMinSize,mainPages.size)
                            ConstantClass.printForDebug("removePages sub=${ Arrays.toString(sub.toTypedArray())}")
                            removePages(sub)
                            for (i in mainPages.size-1 downTo retainMinSize+1){
                                mainPages.removeAt(i)
                            }


                        }
                    }


                }
            }



        GlobalScope.launch (Dispatchers.IO){
            delay(130)
            pages.get(resultIndex).pageFragment?.apply {
                lifecycleScope.launch(Dispatchers.Main){
                    onSelect()
                    setStatusBar()
                }

            }
        }
}

private fun AppCompatActivity.removePages(pages:MutableList<frags_page>){

        pages.forEachIndexed { index, fragsPage ->
            supportFragmentManager.beginTransaction().apply  {
                ConstantClass.printForDebug("removePages fragsPage=${ fragsPage}")
                fragsPage.pageFragment?.apply {
                    remove(this)
                    addToBackStack(null)
                    commitAllowingStateLoss()
                }
                fragsPage.pageFragment=null
              }
        }


}



