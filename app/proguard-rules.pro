# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile


-optimizationpasses 5 # Specifies the number of optimization passes to be performed.
-dontusemixedcaseclassnames # Specifies not to generate mixed-case class names while obfuscating.
-dontskipnonpubliclibraryclasses # Specifies to skip non-public classes while reading library jars, to speed up processing and reduce memory usage of ProGuard.
-dontpreverify # Specifies not to preverify the processed class files.
-verbose # Specifies to write out some more information during processing.

# rename source file as "Proguard"
#-renamesourcefileattribute Proguard
#-renamesourcefileattribute SourceFile
# Kee the source file, and line number
-keepattributes SourceFile, LineNumberTable


# typical library
-dontwarn com.google.**
-dontwarn com.nhaarman.listviewanimations.**
-dontwarn oauth.signpost.signature.**

# Exceptions, InnerClasses, Signature, Deprecated
-keepattributes Exceptions, InnerClasses, Signature, Deprecated, *Annotation*

-dontobfuscate
# android framework class
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service

-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService



-keep class org.jetbrains.kotlin.** { *; }
-keep class org.jetbrains.annotations.** { *; }
-keepclassmembers class ** {
  @org.jetbrains.annotations.ReadOnly public *;
}
# Exception
-keep public class * extends java.lang.Exception



# custom view
-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

# custom fragment
-keep public class * extends androidx.fragment.app.Fragment {
    *;
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context);
}

# custom view using in layout.xml
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

# static CREATOR, Android Parcel對象用的，在運行時的Instrospection，避免Proguard把它去掉
-keepclassmembers class * implements android.os.Parcelable {
    static android.os.Parcelable$Creator CREATOR;
}

# R類
-keepclassmembers class **.R$* {
    public static <fields>;
}

# native
-keepclasseswithmembernames,includedescriptorclasses class * {
    native <methods>;
}

# android support libaray
-dontwarn android.**
-keep class android.** { *; }
-dontwarn androidx.**
-keep class androidx.** { *; }
-keep class androidx.annotation.** { *; }#avoid usage.txt error
-keep class java.** {*;}
-keep class kotlin.** {*;}

-keep class javax.** { *; }
-dontwarn javax.**

# OkHttp
-keep class okio.** { *; }
-dontwarn okio.**
# Glide
-keep class com.bumptech.** { *; }
-dontwarn com.bumptech.**
-keep class com.google.** { *; }
-dontwarn com.google.**
-keep class com.android.** { *; }
-dontwarn com.android.**

# JS
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

# retrofit
-keepclassmembers,allowshrinking,allowobfuscation interface * {
    @retrofit2.http.* <methods>;
}
# Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn org.codehaus.mojo.animal_sniffer.*
# Ignore annotation used for build tooling.
# okio
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
# Ignore JSR 305 annotations for embedding nullability information.
# okHttp
-dontwarn javax.annotation.**
# Guarded by a NoClassDefFoundError try/catch and only used when on the classpath.
-dontwarn kotlin.Unit
# Top-level functions that can only be used by Kotlin.
-dontwarn retrofit2.-KotlinExtensions
# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase
# OkHttp platform used only on JVM and when Conscrypt dependency is available.
-dontwarn okhttp3.internal.platform.ConscryptPlatform
# spongycast
-keep class org.spongycastle.** { *; }
-dontwarn org.spongycastle.**



 #Prevent R8 from leaving Data object members always null
-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
  @com.google.gson.annotations.Expose <fields>;
}


